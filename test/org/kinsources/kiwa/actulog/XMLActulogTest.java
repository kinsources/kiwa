/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actulog;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.devinsy.util.xml.XMLBadFormatException;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLActulogTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLActulogTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActulog01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actulog source = new Actulog();

		//
		String xml = XMLActulog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actulog target = XMLActulog.readActulog(xml);

		//
		Assert.assertEquals(source.lastId(), target.lastId());
		Assert.assertEquals(source.wires().size(), target.wires().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActulog02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actulog source = new Actulog();

		source.createWire("wire1", Locale.FRENCH);
		source.createWire("wire2", Locale.ENGLISH);
		source.createWire("wire3", Locale.FRENCH);

		//
		String xml = XMLActulog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actulog target = XMLActulog.readActulog(xml);

		//
		Assert.assertEquals(source.lastId(), target.lastId());
		Assert.assertEquals(source.wires().size(), target.wires().size());

		Assert.assertEquals(source.wires().getByIndex(0).getId(), target.wires().getByIndex(0).getId());
		Assert.assertEquals(source.wires().getByIndex(0).getAuthor(), target.wires().getByIndex(0).getAuthor());
		Assert.assertEquals(source.wires().getByIndex(0).getLeadParagraph(), target.wires().getByIndex(0).getLeadParagraph());
		Assert.assertEquals(source.wires().getByIndex(0).getTitle(), target.wires().getByIndex(0).getTitle());
		Assert.assertEquals(source.wires().getByIndex(0).getCreationDate().toString(), target.wires().getByIndex(0).getCreationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(0).getEditionDate().toString(), target.wires().getByIndex(0).getEditionDate().toString());
		Assert.assertEquals(source.wires().getByIndex(0).getLocale().getLanguage(), target.wires().getByIndex(0).getLocale().getLanguage());
		Assert.assertEquals(source.wires().getByIndex(0).getPublicationDate(), target.wires().getByIndex(0).getPublicationDate());
		Assert.assertEquals(source.wires().getByIndex(0).status(), target.wires().getByIndex(0).status());

		Assert.assertEquals(source.wires().getByIndex(1).getId(), target.wires().getByIndex(1).getId());
		Assert.assertEquals(source.wires().getByIndex(1).getAuthor(), target.wires().getByIndex(1).getAuthor());
		Assert.assertEquals(source.wires().getByIndex(1).getLeadParagraph(), target.wires().getByIndex(1).getLeadParagraph());
		Assert.assertEquals(source.wires().getByIndex(1).getTitle(), target.wires().getByIndex(1).getTitle());
		Assert.assertEquals(source.wires().getByIndex(1).getCreationDate().toString(), target.wires().getByIndex(1).getCreationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(1).getEditionDate().toString(), target.wires().getByIndex(1).getEditionDate().toString());
		Assert.assertEquals(source.wires().getByIndex(1).getLocale().getLanguage(), target.wires().getByIndex(1).getLocale().getLanguage());
		Assert.assertEquals(source.wires().getByIndex(1).getPublicationDate(), target.wires().getByIndex(1).getPublicationDate());
		Assert.assertEquals(source.wires().getByIndex(1).status(), target.wires().getByIndex(1).status());

		Assert.assertEquals(source.wires().getByIndex(2).getId(), target.wires().getByIndex(2).getId());
		Assert.assertEquals(source.wires().getByIndex(2).getAuthor(), target.wires().getByIndex(2).getAuthor());
		Assert.assertEquals(source.wires().getByIndex(2).getLeadParagraph(), target.wires().getByIndex(2).getLeadParagraph());
		Assert.assertEquals(source.wires().getByIndex(2).getTitle(), target.wires().getByIndex(2).getTitle());
		Assert.assertEquals(source.wires().getByIndex(2).getCreationDate().toString(), target.wires().getByIndex(2).getCreationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(2).getEditionDate().toString(), target.wires().getByIndex(2).getEditionDate().toString());
		Assert.assertEquals(source.wires().getByIndex(2).getLocale().getLanguage(), target.wires().getByIndex(2).getLocale().getLanguage());
		Assert.assertEquals(source.wires().getByIndex(2).getPublicationDate(), target.wires().getByIndex(2).getPublicationDate());
		Assert.assertEquals(source.wires().getByIndex(2).status(), target.wires().getByIndex(2).status());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActXtulog03() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actulog source = new Actulog();

		Wire wire = source.createWire("wire1", Locale.FRENCH);
		wire.setPublicationDate(DateTime.now().plusDays(4));
		wire.setAuthor("testor");
		wire.setLeadParagraph("A lead paragraph text.");
		wire.setBody("A text.<br/>aaaaaa<b>aaaaa</b>aaaaa");
		wire.publish();

		wire = source.createWire("wire2", Locale.ENGLISH);
		wire.setPublicationDate(DateTime.now().minusDays(2));
		wire.setLeadParagraph("A lead paragraph text qf .");
		wire.setBody("A text.<br/>aaaaaa<b>aaaaa</b>aaaaaqs qdf");
		wire.unpublish();

		wire = source.createWire("wire3", Locale.FRENCH);
		wire.setPublicationDate(DateTime.now().plusDays(45));
		wire.setAuthor("testor 3");
		wire.setLeadParagraph("A lead paragraph text. qd fqsf");
		wire.setBody("A text.<br/>aaaaaa<b>aaaaa</b>aaaaa qds fqds fqsfdsq");
		wire.publish();

		//
		String xml = XMLActulog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actulog target = XMLActulog.readActulog(xml);

		//
		Assert.assertEquals(source.lastId(), target.lastId());
		Assert.assertEquals(source.wires().size(), target.wires().size());

		Assert.assertEquals(source.wires().getByIndex(0).getId(), target.wires().getByIndex(0).getId());
		Assert.assertEquals(source.wires().getByIndex(0).getAuthor(), target.wires().getByIndex(0).getAuthor());
		Assert.assertEquals(source.wires().getByIndex(0).getLeadParagraph(), target.wires().getByIndex(0).getLeadParagraph());
		Assert.assertEquals(source.wires().getByIndex(0).getTitle(), target.wires().getByIndex(0).getTitle());
		Assert.assertEquals(source.wires().getByIndex(0).getCreationDate().toString(), target.wires().getByIndex(0).getCreationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(0).getEditionDate().toString(), target.wires().getByIndex(0).getEditionDate().toString());
		Assert.assertEquals(source.wires().getByIndex(0).getLocale().getLanguage(), target.wires().getByIndex(0).getLocale().getLanguage());
		Assert.assertEquals(source.wires().getByIndex(0).getPublicationDate().toString(), target.wires().getByIndex(0).getPublicationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(0).status(), target.wires().getByIndex(0).status());

		Assert.assertEquals(source.wires().getByIndex(1).getId(), target.wires().getByIndex(1).getId());
		Assert.assertEquals(source.wires().getByIndex(1).getAuthor(), target.wires().getByIndex(1).getAuthor());
		Assert.assertEquals(source.wires().getByIndex(1).getLeadParagraph(), target.wires().getByIndex(1).getLeadParagraph());
		Assert.assertEquals(source.wires().getByIndex(1).getTitle(), target.wires().getByIndex(1).getTitle());
		Assert.assertEquals(source.wires().getByIndex(1).getCreationDate().toString(), target.wires().getByIndex(1).getCreationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(1).getEditionDate().toString(), target.wires().getByIndex(1).getEditionDate().toString());
		Assert.assertEquals(source.wires().getByIndex(1).getLocale().getLanguage(), target.wires().getByIndex(1).getLocale().getLanguage());
		Assert.assertNull(source.wires().getByIndex(1).getPublicationDate());
		Assert.assertNull(target.wires().getByIndex(1).getPublicationDate());
		Assert.assertEquals(source.wires().getByIndex(1).status(), target.wires().getByIndex(1).status());

		Assert.assertEquals(source.wires().getByIndex(2).getId(), target.wires().getByIndex(2).getId());
		Assert.assertEquals(source.wires().getByIndex(2).getAuthor(), target.wires().getByIndex(2).getAuthor());
		Assert.assertEquals(source.wires().getByIndex(2).getLeadParagraph(), target.wires().getByIndex(2).getLeadParagraph());
		Assert.assertEquals(source.wires().getByIndex(2).getTitle(), target.wires().getByIndex(2).getTitle());
		Assert.assertEquals(source.wires().getByIndex(2).getCreationDate().toString(), target.wires().getByIndex(2).getCreationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(2).getEditionDate().toString(), target.wires().getByIndex(2).getEditionDate().toString());
		Assert.assertEquals(source.wires().getByIndex(2).getLocale().getLanguage(), target.wires().getByIndex(2).getLocale().getLanguage());
		Assert.assertEquals(source.wires().getByIndex(2).getPublicationDate().toString(), target.wires().getByIndex(2).getPublicationDate().toString());
		Assert.assertEquals(source.wires().getByIndex(2).status(), target.wires().getByIndex(2).status());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testPublish01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actulog source = new Actulog();

		Wire wire = source.createWire("wire1", Locale.FRENCH);
		wire.setPublicationDate(DateTime.now().plusDays(4));
		wire.publish();
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testWire01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Wire source = new Wire(12, "wire1", Locale.FRENCH);
		source.setPublicationDate(DateTime.now().plusDays(4));
		source.setAuthor("testor");
		source.setLeadParagraph("A lead paragraph text.");
		source.setBody("A text.<br/>aaaaaa<b>aaaaa</b>aaaaa");
		source.publish();

		//
		String xml = XMLActulog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Wire target = XMLActulog.readWire(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getAuthor(), target.getAuthor());
		Assert.assertEquals(source.getLeadParagraph(), target.getLeadParagraph());
		Assert.assertEquals(source.getTitle(), target.getTitle());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());
		Assert.assertEquals(source.getLocale().getLanguage(), target.getLocale().getLanguage());
		Assert.assertEquals(source.getPublicationDate().toString(), target.getPublicationDate().toString());
		Assert.assertEquals(source.status(), target.status());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testWire02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Wire source = new Wire(12, "Première news", Locale.FRENCH);
		source.setPublicationDate(DateTime.now().plusDays(4));
		source.setAuthor("testor");
		source.setLeadParagraph("A lead paragraph text with accent éééé.");
		source.setBody("A text with accents àààààà.<br/>aaaaaa<b>aaaaa</b>aaaaa");
		source.publish();

		//
		String xml = XMLActulog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Wire target = XMLActulog.readWire(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getAuthor(), target.getAuthor());
		Assert.assertEquals(source.getLeadParagraph(), target.getLeadParagraph());
		Assert.assertEquals(source.getTitle(), target.getTitle());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());
		Assert.assertEquals(source.getLocale().getLanguage(), target.getLocale().getLanguage());
		Assert.assertEquals(source.getPublicationDate().toString(), target.getPublicationDate().toString());
		Assert.assertEquals(source.status(), target.status());

		//
		logger.debug("===== test done.");
	}
}
