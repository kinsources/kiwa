/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLWriter;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLAgoraTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLAgoraTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAgora01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Agora source = new Agora();

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLAgora.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Agora target = XMLAgora.readAgora(xmlSource);

		//
		Assert.assertEquals(0, target.lastForumId());
		Assert.assertEquals(0, target.lastMessageId());
		Assert.assertEquals(0, target.lastTopicId());
		Assert.assertEquals(0, target.forums().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAgora02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Agora source = new Agora();
		source.createForum("Alpha forum", "A test forum.");
		source.createForum("Bravo forum", "Another test forum.");

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLAgora.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Agora target = XMLAgora.readAgora(xmlSource);

		//
		Assert.assertEquals(2, target.forums().size());

		//
		Forum sourceForum = source.forums().getByIndex(0);
		Forum targetForum = target.forums().getByIndex(0);
		Assert.assertEquals(sourceForum.getId(), targetForum.getId());
		Assert.assertEquals(sourceForum.getTitle(), targetForum.getTitle());
		Assert.assertEquals(sourceForum.getSubtitle(), targetForum.getSubtitle());
		Assert.assertEquals(sourceForum.getCreationDate().toString(), targetForum.getCreationDate().toString());
		Assert.assertEquals(sourceForum.getEditionDate().toString(), targetForum.getEditionDate().toString());

		//
		sourceForum = source.forums().getByIndex(1);
		targetForum = target.forums().getByIndex(1);
		Assert.assertEquals(sourceForum.getId(), targetForum.getId());
		Assert.assertEquals(sourceForum.getTitle(), targetForum.getTitle());
		Assert.assertEquals(sourceForum.getSubtitle(), targetForum.getSubtitle());
		Assert.assertEquals(sourceForum.getCreationDate().toString(), targetForum.getCreationDate().toString());
		Assert.assertEquals(sourceForum.getEditionDate().toString(), targetForum.getEditionDate().toString());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAgora03() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Agora source = new Agora();
		Forum alpha = source.createForum("Alpha forum", "A test forum.");
		source.createForum("Bravo forum", "Another test forum.");

		Topic topic = source.createTopic(alpha, "First topic");
		source.createMessage(topic, 10, "Mike", "This is a text.");
		topic = source.createTopic(alpha, "Second topic");
		source.createMessage(topic, 11, "Mike", "This is a text.");

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLAgora.write(out, source);
		String xml = xmlTarget.toString();

		System.out.println(xml);
		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Agora target = XMLAgora.readAgora(xmlSource);

		//
		Assert.assertEquals(2, target.forums().size());
		Assert.assertEquals(2, target.forums().getByIndex(0).topics().size());
		Assert.assertEquals(0, target.forums().getByIndex(1).topics().size());

		//
		Topic sourceTopic = source.forums().getByIndex(0).topics().getByIndex(0);
		Topic targetTopic = target.forums().getByIndex(0).topics().getByIndex(0);
		Assert.assertEquals(sourceTopic.getId(), targetTopic.getId());
		Assert.assertEquals(sourceTopic.getTitle(), targetTopic.getTitle());
		Assert.assertEquals(sourceTopic.getStatus(), targetTopic.getStatus());
		Assert.assertEquals(sourceTopic.getForum().getId(), targetTopic.getForum().getId());
		Assert.assertEquals(sourceTopic.getCreationDate().toString(), targetTopic.getCreationDate().toString());

		//
		sourceTopic = source.forums().getByIndex(0).topics().getByIndex(1);
		targetTopic = target.forums().getByIndex(0).topics().getByIndex(1);
		Assert.assertEquals(sourceTopic.getId(), targetTopic.getId());
		Assert.assertEquals(sourceTopic.getTitle(), targetTopic.getTitle());
		Assert.assertEquals(sourceTopic.getStatus(), targetTopic.getStatus());
		Assert.assertEquals(sourceTopic.getForum().getId(), targetTopic.getForum().getId());
		Assert.assertEquals(sourceTopic.getCreationDate().toString(), targetTopic.getCreationDate().toString());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testAgora04() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Agora source = new Agora();
		Forum alpha = source.createForum("Alpha forum", "A test forum.");
		source.createForum("Bravo forum", "Another test forum.");

		Topic tango = source.createTopic(alpha, "First topic");
		source.createTopic(alpha, "Second topic");

		source.createMessage(tango, 17, "ego", "It is a wonderful day.");
		source.createMessage(tango, 18, "alter", "It is a wonderful world.");

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLAgora.write(out, source);
		String xml = xmlTarget.toString();

		System.out.println(xml);
		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Agora target = XMLAgora.readAgora(xmlSource);

		//
		Assert.assertEquals(2, target.forums().size());
		Assert.assertEquals(2, target.forums().getByIndex(0).topics().size());
		Assert.assertEquals(2, target.forums().getByIndex(0).topics().getByIndex(0).messages().size());

		//
		Message sourceMessage = source.forums().getByIndex(0).topics().getByIndex(0).messages().getByIndex(0);
		Message targetMessage = target.forums().getByIndex(0).topics().getByIndex(0).messages().getByIndex(0);
		Assert.assertEquals(sourceMessage.getId(), targetMessage.getId());
		Assert.assertEquals(sourceMessage.getAuthorId(), targetMessage.getAuthorId());
		Assert.assertEquals(sourceMessage.getAuthorName(), targetMessage.getAuthorName());
		Assert.assertEquals(sourceMessage.getText(), targetMessage.getText());
		Assert.assertEquals(sourceMessage.getStatus(), targetMessage.getStatus());
		Assert.assertEquals(sourceMessage.getTopic().getId(), targetMessage.getTopic().getId());
		Assert.assertEquals(sourceMessage.getCreationDate().toString(), targetMessage.getCreationDate().toString());
		Assert.assertEquals(sourceMessage.getEditionDate().toString(), targetMessage.getEditionDate().toString());

		//
		sourceMessage = source.forums().getByIndex(0).topics().getByIndex(0).messages().getByIndex(1);
		targetMessage = target.forums().getByIndex(0).topics().getByIndex(0).messages().getByIndex(1);
		Assert.assertEquals(sourceMessage.getId(), targetMessage.getId());
		Assert.assertEquals(sourceMessage.getAuthorId(), targetMessage.getAuthorId());
		Assert.assertEquals(sourceMessage.getAuthorName(), targetMessage.getAuthorName());
		Assert.assertEquals(sourceMessage.getText(), targetMessage.getText());
		Assert.assertEquals(sourceMessage.getStatus(), targetMessage.getStatus());
		Assert.assertEquals(sourceMessage.getTopic().getId(), targetMessage.getTopic().getId());
		Assert.assertEquals(sourceMessage.getCreationDate().toString(), targetMessage.getCreationDate().toString());
		Assert.assertEquals(sourceMessage.getEditionDate().toString(), targetMessage.getEditionDate().toString());

		//
		logger.debug("===== test done.");
	}
}
