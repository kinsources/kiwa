/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.kinsources.kiwa.actalog.EventLog.Category;
import org.kinsources.kiwa.actalog.EventLogComparator.Criteria;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class EventLogs implements Iterable<EventLog>
{
	private ArrayList<EventLog> eventLogs;

	/**
	 * 
	 */
	public EventLogs()
	{
		this.eventLogs = new ArrayList<EventLog>();
	}

	/**
	 * This constructor does a shallow copy of parameter.
	 */
	public EventLogs(final EventLogs source)
	{
		//
		this.eventLogs = new ArrayList<EventLog>(source.size());

		//
		for (EventLog eventLog : source)
		{
			this.eventLogs.add(eventLog);
		}
	}

	/**
	 * 
	 */
	public EventLogs(final int initialCapacity)
	{
		this.eventLogs = new ArrayList<EventLog>(initialCapacity);
	}

	/**
	 * 
	 * @param eventLog
	 */
	public void add(final EventLog eventLog)
	{
		//
		if (eventLog == null)
		{
			throw new IllegalArgumentException("eventLog is null.");
		}
		else if (eventLog.getCategory() == null)
		{
			throw new IllegalArgumentException("category is null.");
		}
		else
		{
			this.eventLogs.add(eventLog);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.eventLogs.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public EventLogs copy()
	{
		EventLogs result;

		//
		result = new EventLogs(this.eventLogs.size());

		//
		for (EventLog eventLog : this.eventLogs)
		{
			result.add(eventLog);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param category
	 * @return
	 */
	public int countByCategory(final Category category)
	{
		int result;

		if (category == null)
		{
			//
			result = this.eventLogs.size();
		}
		else
		{
			result = 0;

			for (EventLog eventLog : this.eventLogs)
			{
				if (eventLog.getCategory() == category)
				{
					result += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public EventLogs first(final int targetCount)
	{
		EventLogs result;

		//
		result = new EventLogs(targetCount);

		//
		boolean ended = false;
		Iterator<EventLog> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			//
			if ((count >= targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param category
	 * @return
	 */
	public EventLogs getByCategory(final Category category)
	{
		EventLogs result;

		if (category == null)
		{
			//
			result = new EventLogs(this);
		}
		else
		{
			//
			result = new EventLogs();

			for (EventLog eventLog : this.eventLogs)
			{
				if (eventLog.getCategory() == category)
				{
					result.add(eventLog);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public EventLog getByIndex(final int index)
	{
		EventLog result;

		result = this.eventLogs.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.eventLogs.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<EventLog> iterator()
	{
		Iterator<EventLog> result;

		result = this.eventLogs.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param lastCount
	 * @return
	 */
	public EventLogs lastest(final int targetCount)
	{
		EventLogs result;

		//
		result = copy().sortByDate().reverse().first(targetCount);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (EventLog eventLog : this.eventLogs)
		{
			if (eventLog.getId() > result)
			{
				result = eventLog.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public EventLogs reverse()
	{
		EventLogs result;

		//
		Collections.reverse(this.eventLogs);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.eventLogs.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public EventLogs sortByDate()
	{
		EventLogs result;

		//
		Collections.sort(this.eventLogs, new EventLogComparator(Criteria.DATETIME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public EventLogs sortById()
	{
		EventLogs result;

		//
		Collections.sort(this.eventLogs, new EventLogComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}
}
