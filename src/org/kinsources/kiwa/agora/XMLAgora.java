/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * Copyright 2017      Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Agora File reader and writer.
 * 
 * @author TIP
 */
public class XMLAgora
{
	public enum Mode
	{
		SHALLOW,
		DEEP
	}

	private static final Logger logger = LoggerFactory.getLogger(XMLAgora.class);;

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Agora load(final File file) throws Exception
	{
		Agora result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readAgora(in);
		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Agora readAgora(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Agora result;

		//
		in.readStartTag("agora");

		//
		{
			//
			long lastForumId = Long.parseLong(in.readContentTag("last_forum_id").getContent());

			//
			long lastTopicId = Long.parseLong(in.readContentTag("last_topic_id").getContent());

			//
			long lastMessageId = Long.parseLong(in.readContentTag("last_message_id").getContent());

			//
			result = new Agora(lastForumId, lastTopicId, lastMessageId);

			//
			readForums(result.forums(), in);
		}

		//
		in.readEndTag("agora");

		//
		result.resetLastIds();
		result.rebuildIndexes();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Forum readForum(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Forum result;

		//
		in.readStartTag("forum");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			String title = StringEscapeUtils.unescapeXml(in.readContentTag("title").getContent());
			String subtitle = StringEscapeUtils.unescapeXml(in.readNullableContentTag("subtitle").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());

			//
			result = new Forum(id, title, subtitle, creationDate, editionDate);

			//
			if (in.hasNextStartTag("topics"))
			{
				readTopics(result.topics(), in, result);
			}
		}

		//
		in.readEndTag("forum");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static void readForums(final Forums target, final String source) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		readForums(target, in);
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readForums(final Forums target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("forums");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("forum"))
			{
				//
				Forum forum = readForum(in);

				//
				target.add(forum);
			}

			in.readEndTag("forums");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Message readMessage(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Message result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readMessage(in, null);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Message readMessage(final XMLReader in, final Topic topic) throws XMLStreamException, XMLBadFormatException
	{
		Message result;

		//
		in.readStartTag("message");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			long topicId = Long.parseLong(in.readContentTag("topic_id").getContent());
			long authorId = Long.parseLong(in.readContentTag("author_id").getContent());
			String author = StringEscapeUtils.unescapeXml(in.readContentTag("author").getContent());
			Message.Status status = Message.Status.valueOf(in.readContentTag("status").getContent());
			String text = StringEscapeUtils.unescapeXml(in.readContentTag("text").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());

			//
			result = new Message(id, topic, authorId, author, status, creationDate, editionDate, text);

			//
			if (result.getTopic() == null)
			{
				result.setTopic(new Topic(topicId, null, null));
			}
		}

		//
		in.readEndTag("message");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readMessages(final Messages target, final XMLReader in, final Topic topic) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("messages");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("message"))
			{
				//
				Message message = readMessage(in, topic);

				//
				target.add(message);
			}

			//
			in.readEndTag("messages");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Topic readTopic(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Topic result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readTopic(in, null);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Topic readTopic(final XMLReader in, final Forum forum) throws XMLStreamException, XMLBadFormatException
	{
		Topic result;

		//
		in.readStartTag("topic");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			String title = StringEscapeUtils.unescapeXml(in.readContentTag("title").getContent());
			long forumId = Long.parseLong(in.readContentTag("forum_id").getContent());
			Topic.Status status = Topic.Status.valueOf(in.readContentTag("status").getContent());
			boolean sticky = Boolean.parseBoolean(in.readContentTag("sticky").getContent());

			// TODO delete with patch.
			if (in.hasNextStartTag("chronological"))
			{
				in.readContentTag("chronological").getContent();
			}

			//
			result = new Topic(id, forum, title, status, sticky);

			if (result.getForum() == null)
			{
				result.setForum(new Forum(forumId, "foo", "foo"));
			}

			//
			if (in.hasNextStartTag("messages"))
			{
				readMessages(result.messages(), in, result);
			}
		}

		//
		in.readEndTag("topic");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readTopics(final Topics target, final XMLReader in, final Forum forum) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("topics");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("topic"))
			{
				//
				Topic topic = readTopic(in, forum);

				//
				target.add(topic);
			}

			//
			in.readEndTag("topics");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Agora source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				logger.warn(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Forums source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLAgora.write(xmlWriter, source, Mode.SHALLOW);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Message source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLAgora.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Topic source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLAgora.write(xmlWriter, source, Mode.SHALLOW);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Agora source)
	{

		//
		out.writeStartTag("agora");

		//
		out.writeTag("last_forum_id", source.lastForumId());

		//
		out.writeTag("last_topic_id", source.lastTopicId());

		//
		out.writeTag("last_message_id", source.lastMessageId());

		//
		write(out, source.forums(), Mode.DEEP);

		//
		out.writeEndTag("agora");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Forum source, final Mode mode)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("forum");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("title", StringEscapeUtils.escapeXml(source.getTitle()));
				out.writeTag("subtitle", StringEscapeUtils.escapeXml(source.getSubtitle()));
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());

				//
				if (mode == Mode.DEEP)
				{
					write(out, source.topics(), mode);
				}
			}

			//
			out.writeEndTag("forum");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Forums source, final Mode mode)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("forums");
		}
		else
		{
			//
			out.writeStartTag("forums", "size", String.valueOf(source.size()));

			//
			{
				for (Forum forum : source)
				{
					write(out, forum, mode);
				}
			}

			//
			out.writeEndTag("forums");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Message source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("message");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("topic_id", source.getTopic().getId());
				out.writeTag("author_id", source.getAuthorId());
				out.writeTag("author", StringEscapeUtils.escapeXml(source.getAuthorName()));
				out.writeTag("status", source.getStatus().toString());
				out.writeTag("text", StringEscapeUtils.escapeXml(source.getText()));
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
			}

			//
			out.writeEndTag("message");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Messages source, final Mode mode)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("messages");
		}
		else
		{
			//
			out.writeStartTag("messages", "size", String.valueOf(source.size()));

			//
			{
				for (Message message : source)
				{
					write(out, message);
				}
			}

			//
			out.writeEndTag("messages");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Topic source, final Mode mode)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("topic");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("title", StringEscapeUtils.escapeXml(source.getTitle()));
				out.writeTag("forum_id", source.getForum().getId());
				out.writeTag("status", source.getStatus().toString());
				out.writeTag("sticky", source.isSticky());

				//
				if (mode == Mode.DEEP)
				{
					write(out, source.messages(), mode);
				}
			}

			//
			out.writeEndTag("topic");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Topics source, final Mode mode)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("topics");
		}
		else
		{
			//
			out.writeStartTag("topics", "size", String.valueOf(source.size()));

			//
			{
				for (Topic topic : source)
				{
					write(out, topic, mode);
				}
			}

			//
			out.writeEndTag("topics");
		}
	}
}
