/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * The <code>IdTopicIndex</code> class represents an Topic index.
 * 
 * @author christian.momon@devinsy.fr
 */
public class TopicIdIndex implements Iterable<Topic>
{
	private HashMap<Long, Topic> index;

	/**
	 * 
	 */
	public TopicIdIndex()
	{
		super();

		this.index = new HashMap<Long, Topic>();
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.index.clear();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Topic getById(final long key)
	{
		Topic result;

		result = this.index.get(key);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.index.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Topic> iterator()
	{
		Iterator<Topic> result;

		result = this.index.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Long> keys()
	{
		Set<Long> result;

		result = this.index.keySet();

		//
		return result;
	}

	/**
	 * 
	 */
	synchronized public Topic put(final Topic source)
	{
		Topic result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.index.put(source.getId(), source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	synchronized public void put(final Topics source)
	{
		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			for (Topic topic : source)
			{
				put(topic);
			}
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void rebuild(final Topics source)
	{

		//
		this.index.clear();

		//
		for (Topic topic : source)
		{
			put(topic);
		}
	}

	/**
	 * 
	 * @param wire
	 */
	public void remove(final Topic topic)
	{
		this.index.remove(topic.getId());
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.index.size();

		//
		return result;
	}
}
