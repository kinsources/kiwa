/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.kinsources.kiwa.agora.TopicComparator.Criteria;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Topics implements Iterable<Topic>
{
	private ArrayList<Topic> topics;

	/**
	 * 
	 */
	public Topics()
	{

		this.topics = new ArrayList<Topic>();
	}

	/**
	 * 
	 */
	public Topics(final int initialCapacity)
	{

		this.topics = new ArrayList<Topic>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final Topic topic)
	{
		//
		if (topic == null)
		{
			//
			throw new IllegalArgumentException("Topic is null.");

		}
		else if (topic.getTitle() == null)
		{
			//
			throw new IllegalArgumentException("Title is null.");

		}
		else
		{
			//
			this.topics.add(topic);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{

		this.topics.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Topics copy()
	{
		Topics result;

		//
		result = new Topics(this.topics.size());

		//
		for (Topic Topic : this.topics)
		{
			result.add(Topic);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Topics first(final int targetCount)
	{
		Topics result;

		//
		result = new Topics(targetCount);

		//
		boolean ended = false;
		Iterator<Topic> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Topic getByIndex(final int index)
	{
		Topic result;

		result = this.topics.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.topics.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Topic> iterator()
	{
		Iterator<Topic> result;

		result = this.topics.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Topic topic)
	{
		this.topics.remove(topic);
	}

	/**
	 * 
	 * @return
	 */
	public Topics reverse()
	{
		Topics result;

		//
		Collections.reverse(this.topics);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.topics.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Topics sortById()
	{
		Topics result;

		//
		Collections.sort(this.topics, new TopicComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Topics sortByLatestMessage()
	{
		Topics result;

		//
		Collections.sort(this.topics, new TopicComparator(Criteria.LAST_MESSAGE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Topics sortBySticky()
	{
		Topics result;

		//
		Collections.sort(this.topics, new TopicComparator(Criteria.STICKY));

		//
		result = this;

		//
		return result;
	}
}
