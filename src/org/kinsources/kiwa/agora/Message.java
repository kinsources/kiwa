/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Message
{
	public enum Status
	{
		ACTIVATED
		// SUSPENDED, STU?
	}

	private long id;
	private Topic topic;
	private long authorId;
	private String authorName;
	private Status status;
	private DateTime creationDate;
	private DateTime editionDate;
	private String text;

	/**
	 * 
	 * @param id
	 * @param topic
	 * @param authorId
	 * @param authorName
	 * @param status
	 * @param creationDate
	 * @param editionDate
	 * @param text
	 */
	public Message(final long id, final Topic topic, final long authorId, final String authorName, final Status status, final DateTime creationDate, final DateTime editionDate, final String text)
	{
		this.id = id;
		this.topic = topic;
		this.authorId = authorId;
		this.authorName = authorName;
		this.status = status;
		this.creationDate = creationDate;
		this.editionDate = editionDate;
		this.text = text;
	}

	/**
	 * 
	 * @param id
	 * @param topic
	 * @param authorId
	 * @param authorName
	 */
	public Message(final long id, final Topic topic, final long authorId, final String authorName, final String text)
	{
		this.id = id;
		this.topic = topic;
		this.authorId = authorId;
		this.authorName = authorName;
		this.status = Status.ACTIVATED;
		this.creationDate = DateTime.now();
		this.editionDate = this.creationDate;
		this.text = text;
	}

	public long getAuthorId()
	{
		return this.authorId;
	}

	public String getAuthorName()
	{
		return this.authorName;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public long getId()
	{
		return this.id;
	}

	/**
	 * 
	 * @return
	 */
	public Status getStatus()
	{
		return this.status;
	}

	public String getText()
	{
		return this.text;
	}

	public Topic getTopic()
	{
		return this.topic;
	}

	public void setAuthorId(final long authorId)
	{
		this.authorId = authorId;
	}

	public void setAuthorName(final String name)
	{
		this.authorName = name;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	public void setText(final String text)
	{
		this.text = text;
	}

	public void setTopic(final Topic topic)
	{
		this.topic = topic;
	}

	public String title()
	{
		String result;

		if (this.topic == null)
		{
			result = null;
		}
		else
		{
			result = this.topic.getTitle();
		}

		//
		return result;
	}
}
