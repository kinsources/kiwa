/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Contributor
{
	private Account account;
	private Datasets personalDatasets;
	private Collections collections;

	/**
	 * 
	 */
	public Contributor(final Account source)
	{
		this.account = source;
		this.personalDatasets = new Datasets();
		this.collections = new Collections();
	}

	public Account account()
	{
		return this.account;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations collaborationsFrom()
	{
		Collaborations result;

		result = new Collaborations();

		for (Dataset dataset : this.personalDatasets.findWithCollaborator())
		{
			for (Collaborator collaborator : dataset.collaborators())
			{
				Collaboration collaboration = new Collaboration(this, dataset, collaborator);
				result.add(collaboration);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collections collections()
	{
		return this.collections;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfAttachmentFiles()
	{
		long result;

		result = this.personalDatasets.countOfAttachmentFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfCollections()
	{
		long result;

		result = this.collections.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetEntries()
	{
		long result;

		result = 0;
		for (Collection collection : this.collections)
		{
			result += collection.countOfDatasets();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetFiles()
	{
		long result;

		result = this.personalDatasets.countOfDatasetFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasets()
	{
		long result;

		result = this.personalDatasets.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFiles()
	{
		long result;

		result = this.personalDatasets.countOfFiles();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfGenealogies()
	{
		long result;

		result = this.personalDatasets.find(Dataset.Type.GENEALOGY).size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfPublishedDatasetFiles()
	{
		long result;

		result = this.personalDatasets.countOfDatasetFiles(Dataset.Status.VALIDATED);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfPublishedDatasets()
	{
		long result;

		result = this.personalDatasets.count(Dataset.Status.VALIDATED);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfTerminologies()
	{
		long result;

		result = this.personalDatasets.find(Dataset.Type.TERMINOLOGY).size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collections findCollectionsWith(final Dataset criteria)
	{
		return this.collections.findCollectionsWith(criteria);
	}

	public Collections getCollections()
	{
		return this.collections;
	}

	public String getEmail()
	{
		return this.account.getEmail();
	}

	public String getFirstNames()
	{
		return this.account.getFirstNames();
	}

	public String getFullName()
	{
		return this.account.getFullName();
	}

	public long getId()
	{
		return this.account.getId();
	}

	public DateTime getLastConnection()
	{
		return this.account.getLastConnection();
	}

	public String getLastName()
	{
		return this.account.getLastName();
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastPersonalDatasetId()
	{
		long result;

		result = this.personalDatasets.lastId();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets personalDatasets()
	{
		return this.personalDatasets;
	}

	public void setAccount(final Account account)
	{
		this.account = account;
	}

	public void setCollections(final Collections collections)
	{
		this.collections = collections;
	}
}
