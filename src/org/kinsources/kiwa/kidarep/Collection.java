/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Collection
{
	public enum Scope
	{
		PUBLIC,
		PRIVATE
	}

	private long id;
	private String name;
	private String description;
	private Contributor owner;
	private Scope scope;
	private DateTime creationDate;
	private DateTime editionDate;
	private Datasets datasets;

	/**
	 * 
	 */
	public Collection(final long id, final Contributor owner, final String name, final String description, final Scope scope)
	{
		this.id = id;
		this.owner = owner;
		this.name = name;
		this.scope = scope;
		this.description = description;
		this.creationDate = DateTime.now();
		this.editionDate = this.creationDate;
		this.datasets = new Datasets();
	}

	/**
	 * 
	 */
	public Collection(final long id, final Contributor owner, final String name, final String description, final Scope scope, final DateTime creationDate, final DateTime editionDate)
	{
		this.id = id;
		this.name = name;
		this.scope = scope;
		this.description = description;
		this.owner = owner;
		this.creationDate = creationDate;
		this.editionDate = editionDate;
		this.datasets = new Datasets();
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public boolean contains(final Dataset criteria)
	{
		boolean result;

		result = this.datasets.contains(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasets()
	{
		return this.datasets.size();
	}

	/**
	 * 
	 * @return
	 */
	public Datasets datasets()
	{
		return this.datasets;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public String getDescription()
	{
		return this.description;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public long getId()
	{
		return this.id;
	}

	public String getName()
	{
		return this.name;
	}

	public Contributor getOwner()
	{
		return this.owner;
	}

	public Scope getScope()
	{
		return this.scope;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public void setOwner(final Contributor owner)
	{
		this.owner = owner;
	}

	public void setScope(final Scope scope)
	{
		this.scope = scope;
	}
}
