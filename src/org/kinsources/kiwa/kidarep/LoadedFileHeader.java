/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class LoadedFileHeader
{
	public static final long NO_ID = 0;

	private long id;
	private String name;
	private long length;
	private String digest;
	private DateTime loadingDate;

	/**
	 * 
	 * @param name
	 * @param data
	 */
	public LoadedFileHeader(final LoadedFileHeader source)
	{
		this.id = source.id();
		this.name = source.name();
		this.length = source.length();
		this.digest = source.digest();
		this.loadingDate = source.loadingDate();
	}

	/**
	 * 
	 * @param name
	 * @param data
	 */
	public LoadedFileHeader(final long id, final LoadedFileHeader source)
	{
		this.id = id;
		this.name = source.name();
		this.length = source.length();
		this.digest = source.digest();
		this.loadingDate = source.loadingDate();
	}

	/**
	 * 
	 * @param name
	 * @param data
	 */
	public LoadedFileHeader(final long id, final String name, final byte[] data)
	{
		this.id = id;
		this.name = name;
		if (data == null)
		{
			this.length = 0;
		}
		else
		{
			this.length = data.length;
		}
		this.digest = digest(data);
		this.loadingDate = DateTime.now();
	}

	/**
	 * 
	 */
	public LoadedFileHeader(final long id, final String name, final long length, final String digest, final DateTime loadingDate)
	{
		this.id = id;
		this.name = name;
		this.length = length;
		this.digest = digest;
		this.loadingDate = loadingDate;
	}

	/**
	 * 
	 * @param name
	 * @param data
	 */
	public LoadedFileHeader(final String name, final byte[] data)
	{
		this.id = NO_ID;
		this.name = name;
		if (data == null)
		{
			this.length = 0;
		}
		else
		{
			this.length = data.length;
		}
		this.digest = digest(data);
		this.loadingDate = DateTime.now();
	}

	/**
	 * 
	 * @return
	 */
	public boolean checkDigest(final byte[] source)
	{
		boolean result;

		result = StringUtils.equals(digest(source), this.digest);

		//
		return result;
	}

	public String digest()
	{
		return this.digest;
	}

	public long id()
	{
		return this.id;
	}

	public long length()
	{
		return this.length;
	}

	public DateTime loadingDate()
	{
		return this.loadingDate;
	}

	public String name()
	{
		return this.name;
	}

	/**
	 * 
	 * @param newName
	 */
	public void rename(final String newName)
	{
		if (StringUtils.isNotBlank(newName))
		{
			this.name = newName;
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String digest(final byte[] source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = DigestUtils.md5Hex(source);
		}

		//
		return result;
	}
}
