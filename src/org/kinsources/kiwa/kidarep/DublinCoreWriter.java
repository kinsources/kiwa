/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import fr.devinsy.util.StringListWriter;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.util.xml.XMLWriter;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class DublinCoreWriter
{
	/**
	 * 
	 * @param dataset
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toString(final Dataset dataset, final String websiteUrl, final String permanentLink) throws UnsupportedEncodingException
	{
		String result;

		//
		StringListWriter xml = new StringListWriter();

		new PrintWriter(xml).println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML+RDFa 1.0//EN\" \"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd\">");
		XMLWriter out = new XMLWriter(xml);

		//
		out.writeStartTag("html", "xmlns", "http://www.w3.org/1999/xhtml", "xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#", "xmlns:rdfs", "http://www.w3.org/2000/01/rdf-schema#",
				"xmlns:xsd", "http://www.w3.org/2001/XMLSchema#", "xmlns:dc", "http://purl.org/dc/elements/1.1/", "xmlns:dcterms", "http://purl.org/dc/terms/");
		{
			//
			out.writeEmptyTag("meta", "http-equiv", "Content-type", "content", "text/html; charset=utf-8");

			out.writeEmptyTag("link", "rel", "dc.identifier", "href", permanentLink);
			out.writeEmptyTag("meta", "property", "dc:title", "content", "Kinsources Kinship Corpus : " + dataset.getId() + "," + XMLTools.escapeXmlBlank(dataset.getName()));
			out.writeEmptyTag("meta", "property", "dc:creator", "content", XMLTools.escapeXmlBlank(dataset.getContributor().getFullName()));
			if (dataset.getPublicationDate() == null)
			{
				out.writeEmptyTag("meta", "property", "dcterms:created", "content", "");
			}
			else
			{
				out.writeEmptyTag("meta", "property", "dcterms:created", "content", dataset.getPublicationDate().toString("yyyy-MM-dd"));
			}
			out.writeEmptyTag("meta", "property", "dcterms:abstract", "content", XMLTools.escapeXmlBlank(dataset.getShortDescription()), "xml:lang", "en");
			out.writeEmptyTag("meta", "property", "dc:subject", "content", "kinship");
			out.writeEmptyTag("meta", "property", "dc:subject", "content", "dataset");
			out.writeEmptyTag("meta", "property", "dc:subject", "content", XMLTools.escapeXmlBlank(dataset.getCountry()));
			out.writeEmptyTag("meta", "property", "dc:subject", "content", XMLTools.escapeXmlBlank(dataset.getRegion()));
			out.writeEmptyTag("meta", "property", "dc:subject", "content", XMLTools.escapeXmlBlank(dataset.getContinent()));
			out.writeEmptyTag("meta", "property", "dc:subject", "content", XMLTools.escapeXmlBlank(dataset.getLocation()));
			out.writeEmptyTag("meta", "property", "dc:subject", "content", XMLTools.escapeXmlBlank(dataset.getEthnicOrCulturalGroup()));
			out.writeEmptyTag("meta", "property", "dc:subject", "content", XMLTools.escapeXmlBlank(dataset.getPeriod()));
			out.writeEmptyTag("meta", "property", "dc:type", "content", "kinship dataset");
			out.writeEmptyTag("meta", "property", "dc:format", "content", "text/html");
			out.writeEmptyTag("meta", "property", "dc:relation", "content", "https://kinsources.net/");
			out.writeEmptyTag("meta", "property", "dcterms:bibliographicCitation", "content", XMLTools.escapeXmlBlank(dataset.getCitation()));
		}
		out.writeEndTag("html");

		//
		result = xml.toString();

		System.out.println("xml=[" + xml + "]");

		//
		return result;
	}
}
