/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.kidarep.CollaboratorComparator.Criteria;

/**
 * The <code>Collaborators</code> class represents a dataset collaborator.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Collaborators implements Iterable<Collaborator>
{
	private List<Collaborator> collaborators;

	/**
	 * 
	 */
	public Collaborators()
	{
		super();

		this.collaborators = new ArrayList<Collaborator>();
	}

	/**
	 * 
	 */
	public Collaborators(final int initialCapacity)
	{
		this.collaborators = new ArrayList<Collaborator>(initialCapacity);
	}

	/**
	 * 
	 */
	public Collaborator add(final Collaborator source)
	{
		Collaborator result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.collaborators.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final Collaborators source)
	{
		if (source != null)
		{
			for (Collaborator collaborator : source)
			{
				if (!this.collaborators.contains(collaborator))
				{
					add(collaborator);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.collaborators.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Collaborators copy()
	{
		Collaborators result;

		//
		result = new Collaborators(this.collaborators.size());

		//
		for (Collaborator account : this.collaborators)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Collaborator getByAccountId(final long id)
	{
		Collaborator result;

		boolean ended = false;
		int index = 0;
		result = null;
		while (!ended)
		{
			//
			if (index < this.size())
			{
				//
				Collaborator collaborator = this.getByIndex(index);

				//
				if (collaborator.getId() == id)
				{
					ended = true;
					result = collaborator;
				}
				else
				{
					index += 1;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Collaborator getByAccountId(final String id)
	{
		Collaborator result;

		//
		if (NumberUtils.isDigits(id))
		{
			result = getByAccountId(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Collaborator getByIndex(final int index)
	{
		Collaborator result;

		result = this.collaborators.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.collaborators.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Collaborator> iterator()
	{
		Iterator<Collaborator> result;

		result = this.collaborators.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Collaborator account)
	{
		this.collaborators.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public Collaborators reverse()
	{
		Collaborators result;

		//
		java.util.Collections.reverse(this.collaborators);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.collaborators.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborators sortByFullName()
	{
		Collaborators result;

		//
		java.util.Collections.sort(this.collaborators, new CollaboratorComparator(Criteria.COLLABORATOR_FULLNAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborators sortById()
	{
		Collaborators result;

		//
		java.util.Collections.sort(this.collaborators, new CollaboratorComparator(Criteria.COLLABORATOR_ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborators sortByLastName()
	{
		Collaborators result;

		//
		java.util.Collections.sort(this.collaborators, new CollaboratorComparator(Criteria.COLLABORATOR_LASTNAME));

		//
		result = this;

		//
		return result;
	}
}
