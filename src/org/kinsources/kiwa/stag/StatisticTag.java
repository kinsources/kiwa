/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.stag;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public enum StatisticTag
{
	// Genealogy statistics.
	NUM_OF_INDIVIDUALS(Scope.GENEALOGY, Type.LONG),
	NUM_OF_MEN(Scope.GENEALOGY, Type.LONG),
	NUM_OF_WOMEN(Scope.GENEALOGY, Type.LONG),
	NUM_OF_UNKNOWN(Scope.GENEALOGY, Type.LONG),

	RATE_OF_MEN(Scope.GENEALOGY, Type.DOUBLE),
	RATE_OF_WOMEN(Scope.GENEALOGY, Type.DOUBLE),
	RATE_OF_UNKNOWN(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_UNIONS(Scope.GENEALOGY, Type.LONG),
	NUM_OF_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	DENSITY_OF_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
	NUM_OF_NON_SINGLE_MEN(Scope.GENEALOGY, Type.LONG),
	NUM_OF_NON_SINGLE_WOMEN(Scope.GENEALOGY, Type.LONG),

	NUM_OF_PARENT_CHILD_TIES(Scope.GENEALOGY, Type.LONG),
	DENSITY_OF_FILIATION(Scope.GENEALOGY, Type.DOUBLE),
	NUM_OF_FERTILE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_FERTILE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
	NUM_OF_COWIFE_RELATIONS(Scope.GENEALOGY, Type.LONG),
	NUM_OF_COHUSBAND_RELATIONS(Scope.GENEALOGY, Type.LONG),

	NUM_OF_RELATIONS(Scope.GENEALOGY, Type.LONG),
	NUM_OF_RELATION_MODELS(Scope.GENEALOGY, Type.LONG),

	NUM_OF_COMPONENTS(Scope.GENEALOGY, Type.LONG),
	MAX_OF_COMPONENTS(Scope.GENEALOGY, Type.LONG),

	MEAN_OF_COMPONENTS_SHARE_AGNATIC(Scope.GENEALOGY, Type.DOUBLE),
	MAX_OF_COMPONENTS_SHARE_AGNATIC(Scope.GENEALOGY, Type.DOUBLE),
	MEAN_OF_COMPONENTS_SHARE_AGNATIC_WO_SINGLETON(Scope.GENEALOGY, Type.DOUBLE),

	MEAN_OF_COMPONENTS_SHARE_UTERINE(Scope.GENEALOGY, Type.DOUBLE),
	MAX_OF_COMPONENTS_SHARE_UTERINE(Scope.GENEALOGY, Type.DOUBLE),
	MEAN_OF_COMPONENTS_SHARE_UTERINE_WO_SINGLETON(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_ELEMENTARY_CYCLES(Scope.GENEALOGY, Type.LONG),

	DEPTH(Scope.GENEALOGY, Type.LONG),
	DEPTH_MEAN(Scope.GENEALOGY, Type.DOUBLE),

	MEAN_SPOUSE_OF_MEN(Scope.GENEALOGY, Type.DOUBLE),
	MEAN_SPOUSE_OF_WOMEN(Scope.GENEALOGY, Type.DOUBLE),

	MEAN_AGNATIC_FRATRY_SIZE(Scope.GENEALOGY, Type.DOUBLE),
	MEAN_UTERINE_FRATRY_SIZE(Scope.GENEALOGY, Type.DOUBLE),
	MEAN_CHILDREN_PER_FERTILE_COUPLE(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_LEVIRATE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_LEVIRATE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_SORORATE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_SORORATE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_NIECE_NEPHEW_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_NIECE_NEPHEW_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_DOUBLE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_DOUBLE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	NUM_OF_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
	RATE_OF_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),

	BASIC_GSTATISTICS_ERROR(Scope.GENEALOGY, Type.LONG),

	// --------------------------------------------------------------
	// Terminology controls.
	GENERAL_CHECK(Scope.TERMINOLOGY, Type.LONG),

	// Terminology statistics.
	NUM_OF_TERMS(Scope.TERMINOLOGY, Type.LONG),
	SELF_NAME(Scope.TERMINOLOGY, Type.STRING),
	NUM_OF_FEMALES(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_FEMALES_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_FEMALES_SPEAKER(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_FEMALES_SPEAKER_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_MALES(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_MALES_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_MALES_SPEAKER(Scope.TERMINOLOGY, Type.LONG),
	NUM_OF_MALES_SPEAKER_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
	GENERATION_PATTERN_MALE(Scope.TERMINOLOGY, Type.STRING),
	GENERATION_PATTERN_FEMALE(Scope.TERMINOLOGY, Type.STRING),
	GENERATION_PATTERN_TOTAL(Scope.TERMINOLOGY, Type.STRING),
	NUM_OF_AUTORECIPROCAL_TERMS(Scope.TERMINOLOGY, Type.LONG),
	RECURSIVE_TERMS(Scope.TERMINOLOGY, Type.STRING),
	ASCENDANT_CLASSIFICATION(Scope.TERMINOLOGY, Type.STRING),
	COUSIN_CLASSIFICATION(Scope.TERMINOLOGY, Type.STRING),
	KIN_TERM_NETWORK_FEMALE_COMPONENTS(Scope.TERMINOLOGY, Type.LONG),
	KIN_TERM_NETWORK_FEMALE_CONCENTRATION(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_MALE_COMPONENTS(Scope.TERMINOLOGY, Type.LONG),
	KIN_TERM_NETWORK_MALE_CONCENTRATION(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT(Scope.TERMINOLOGY, Type.DOUBLE),
	KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE(Scope.TERMINOLOGY, Type.DOUBLE),

	BASIC_TSTATISTICS_ERROR(Scope.TERMINOLOGY, Type.LONG),

	// --------------------------------------------------------------
	GEOGRAPHY_AVAILABLE(Scope.GEOGRAPHY, Type.BOOLEAN),

	// --------------------------------------------------------------
	COMPUTING_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_BEGIN_DATETIME(Scope.SYSTEM, Type.STRING),
	COMPUTING_END_DATETIME(Scope.SYSTEM, Type.STRING),
	GRAPHICS_COUNT(Scope.SYSTEM, Type.LONG),

	COMPUTING_CONTROLS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_BASICS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GEOGRAPHY_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_ATTRIBUTES_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_CIRCUITS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_COMPONENTS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_COMPLETENESS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_SIBSETDISTRIBUTION_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_CONSANGUINEPAIRS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_BIASWEIGHTS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_BIASNETWEIGHTS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_GENDERS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_PEDG2_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_PEDG3_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_UNIONSTATUS_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_FIRSTCOUSINMARRIAGES_TIME(Scope.SYSTEM, Type.LONG),
	COMPUTING_GRAPHIC_ANCESTORCHAINS_TIME(Scope.SYSTEM, Type.LONG);

	// --------------------------------------------------------------

	public enum Scope
	{
		GENEALOGY,
		TERMINOLOGY,
		GEOGRAPHY,
		SYSTEM
	}

	public enum Type
	{
		DOUBLE,
		LONG,
		STRING,
		BOOLEAN
	}

	private Scope scope;
	private Type type;

	/**
	 * 
	 * @param scope
	 * @param type
	 */
	private StatisticTag(final Scope scope, final Type type)
	{
		this.scope = scope;
		this.type = type;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public boolean isBasic()
	{
		boolean result;

		switch (this)
		{
			case NUM_OF_INDIVIDUALS:
			case NUM_OF_UNIONS:
			case NUM_OF_RELATIONS:
			case DEPTH:
				result = true;
			break;

			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isGenealogy()
	{
		boolean result;

		if (this.scope == Scope.GENEALOGY)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public boolean isNotBasic()
	{
		boolean result;

		result = !isBasic();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isTerminology()
	{
		boolean result;

		if (this.scope == Scope.TERMINOLOGY)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Scope scope()
	{
		return this.scope;
	}

	/**
	 * 
	 * @return
	 */
	public Type type()
	{
		return this.type;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean isBasic(final String name)
	{
		boolean result;

		StatisticTag statistic = EnumUtils.getEnum(StatisticTag.class, StringUtils.upperCase(name));

		if (statistic == null)
		{
			result = false;
		}
		else
		{
			result = statistic.isBasic();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean isNotBasic(final String name)
	{
		boolean result;

		result = !isBasic(name);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static StatisticTags tags()
	{
		StatisticTags result;

		result = new StatisticTags();

		for (StatisticTag tag : values())
		{
			result.add(tag);
		}

		//
		return result;
	}
}
