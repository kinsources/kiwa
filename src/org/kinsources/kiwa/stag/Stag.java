/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.stag;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;

import nl.mpi.kinnate.kindata.EntityData;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.AccountManager;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Kidarep;
import org.kinsources.kiwa.kidarep.LoadedFile;
import org.kinsources.kiwa.kidarep.LoadedFileHeader;
import org.kinsources.kiwa.kidarep.LoadedFiles;
import org.kinsources.kiwa.kidarep.Statistics;
import org.kinsources.kiwa.utils.Chronometer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.census.workers.CircuitType;
import org.tip.puck.census.workers.RestrictionType;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.kinoath.Puck2KinOath;
import org.tip.puck.kinoath.io.KinOathFile;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.net.relations.roles.RoleRelationWorker;
import org.tip.puck.net.relations.workers.RelationModelReporter;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.IndividualValuator.EndogenousLabel;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.PartitionType;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChartMaker;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.BIASCounts;
import org.tip.puck.statistics.FiliationCounts;
import org.tip.puck.statistics.GenderedDouble;
import org.tip.puck.statistics.GenderedInt;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.IntWithMax;
import org.tip.puck.util.MathUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Stag
{
	private static final Logger logger = LoggerFactory.getLogger(Stag.class);

	public static final int GENEALOGY_GRAPHIC_WIDTH = 400;
	public static final int GENEALOGY_GRAPHIC_HEIGHT = 240;

	public static final String CROSS_FIRST_COUSIN_MARRIAGE_PATTERN = "XF(X)HX";
	public static final String LEVIRATE_MARRIAGE_PATTERN = "X(X)X.F";
	public static final String SORORATE_MARRIAGE_PATTERN = "X(X)X.H";
	public static final String DOUBLE_OR_EXCHANGE_MARRIAGE_PATTERN = "X(X)X.X(X)X";
	public static final String NIECE_NEPHEW_MARRIAGE_PATTERN = "X(X)XX";
	public static final String PARALLEL_FIRST_COUSIN_MARRIAGE_PATTERN = "XH(X)HX XF(X)FX";
	public static final String FIRST_COUSIN_MARRIAGE_PATTERN = "XX(X)XX";
	public static final String DOUBLE_MARRIAGE_PATTERN = "H(X)H.F(X)F";
	public static final String EXCHANGE_MARRIAGE_PATTERN = "H(X)F.H(X)F";

	private AccountManager accountManager;
	private Kidarep kidarep;
	private long activatedAccountCount;
	private long individualCount;
	private long publishedDatasetCount;
	private long publishedDatasetFileCount;
	private long unionCount;
	private long relationCount;

	private StringList organizationNames;
	private StringList atlasCodeNames;
	private StringList authorNames;
	private StringList coderNames;
	private StringList datasetNames;
	private StringList countryNames;
	private StringList contributorNames;
	private StringList regionNames;
	private StringList locationNames;
	private StringList continentNames;

	/**
	 * 
	 */
	public Stag(final AccountManager accountManager, final Kidarep source)
	{
		//
		this.accountManager = accountManager;
		this.kidarep = source;

		//
		update();
	}

	/**
	 * 
	 * @return
	 */
	private StringList findAtlasCodeNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getAtlasCode() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getAtlasCode());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findAuthorNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getAuthor() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getAuthor());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findCoderNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getCoder() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getCoder());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findContinentNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getContinent() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getContinent());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findContributorNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Contributor contributor : this.kidarep.findContributors(Account.Status.ACTIVATED))
		{
			strings.add(contributor.getFullName());
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findCountryNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getCountry() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getCountry());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findDatasetNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getName() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getName());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findLocationNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getLocation() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getLocation());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findOrganizationNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Contributor contributor : this.kidarep.findContributors(Account.Status.ACTIVATED))
		{
			if (contributor.account().getOrganization() != null)
			{
				strings.add(contributor.account().getOrganization());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	private StringList findRegionNames()
	{
		StringList result;

		HashSet<String> strings = new HashSet<String>();

		//
		for (Dataset dataset : this.kidarep.datasetsFromIndex())
		{
			if ((dataset.getRegion() != null) && (dataset.getStatus() == Dataset.Status.VALIDATED))
			{
				strings.add(dataset.getRegion());
			}
		}

		//
		result = new StringList();

		//
		for (String string : strings)
		{
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	public long getActivatedAccountCount()
	{
		return this.activatedAccountCount;
	}

	public StringList getAtlasCodeNames()
	{
		return this.atlasCodeNames;
	}

	public StringList getAuthorNames()
	{
		return this.authorNames;
	}

	public StringList getCoderNames()
	{
		return this.coderNames;
	}

	public StringList getContinentNames()
	{
		return this.continentNames;
	}

	public StringList getContributorNames()
	{
		return this.contributorNames;
	}

	public StringList getCountryNames()
	{
		return this.countryNames;
	}

	public StringList getDatasetNames()
	{
		return this.datasetNames;
	}

	public long getIndividualCount()
	{
		return this.individualCount;
	}

	public StringList getLocationNames()
	{
		return this.locationNames;
	}

	public StringList getOrganizationNames()
	{
		return this.organizationNames;
	}

	public long getPublishedDatasetCount()
	{
		return this.publishedDatasetCount;
	}

	public long getPublishedDatasetFileCount()
	{
		return this.publishedDatasetFileCount;
	}

	public StringList getRegionNames()
	{
		return this.regionNames;
	}

	public long getRelationCount()
	{
		return this.relationCount;
	}

	public long getUnionCount()
	{
		return this.unionCount;
	}

	/**
	 * 
	 */
	public void update()
	{
		//
		updateActivatedAccountCount();
		updatePublishedDatasetCount();
		updatePublishedDatasetFileCount();
		this.individualCount = this.kidarep.countOfIndividuals();
		this.unionCount = this.kidarep.countOfUnions();
		this.relationCount = this.kidarep.countOfRelations();

		//
		this.organizationNames = findOrganizationNames();

		//
		this.atlasCodeNames = findAtlasCodeNames();
		this.authorNames = findAuthorNames();
		this.coderNames = findCoderNames();
		this.continentNames = findContinentNames();
		this.datasetNames = findDatasetNames();
		this.countryNames = findCountryNames();
		this.contributorNames = findContributorNames();
		this.locationNames = findLocationNames();
		this.regionNames = findRegionNames();
	}

	/**
	 * 
	 */
	public void updateActivatedAccountCount()
	{
		this.activatedAccountCount = this.accountManager.countOfActivatedAccounts();
	}

	/**
	 * 
	 */
	public void updatePublishedDatasetCount()
	{
		this.publishedDatasetCount = this.kidarep.countOfPublishedDatasets();
	}

	/**
	 * 
	 */
	public void updatePublishedDatasetFileCount()
	{
		this.publishedDatasetFileCount = this.kidarep.countOfPublishedDatasetFiles();
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public static CountAndRate analyzeCircuitCensus(final String pattern, final Net source) throws PuckException
	{
		CountAndRate result;

		//
		CensusCriteria criteria = new CensusCriteria();

		criteria.setPattern(pattern);
		criteria.setClosingRelation("SPOUSE");
		criteria.setChainClassification("SIMPLE");
		criteria.setCircuitType(CircuitType.CIRCUIT);
		criteria.setFiliationType(FiliationType.COGNATIC);
		criteria.setRestrictionType(RestrictionType.NONE);
		criteria.setSiblingMode(SiblingMode.FULL);
		criteria.setSymmetryType(SymmetryType.PERMUTABLE);

		//
		CircuitFinder finder = new CircuitFinder(new Segmentation(source), criteria);

		//
		finder.findCircuits();

		//
		finder.count();

		long count = finder.getCircuitCoupleCount();
		double density = finder.getCircuitCoupleDensity();

		result = new CountAndRate(count, density);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static AttributeDescriptors buildAttributeDescriptorStatistics(final Net source)
	{
		AttributeDescriptors result;

		try
		{
			result = AttributeWorker.getExogenousAttributeDescriptors(source, null).sort();
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN STAG REFRESHING", exception);
			result = new AttributeDescriptors();
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildBasicStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			int errorCount = 0;

			if (source != null)
			{
				Chronometer chrono = new Chronometer().start();

				//
				result.put(StatisticTag.NUM_OF_INDIVIDUALS.name(), source.individuals().size());

				//
				GenderedInt genders = StatisticsWorker.genderDistribution(source.individuals());
				result.put(StatisticTag.NUM_OF_MEN.name(), genders.getMenValue());
				result.put(StatisticTag.NUM_OF_WOMEN.name(), genders.getWomenValue());
				result.put(StatisticTag.NUM_OF_UNKNOWN.name(), genders.getUnknownValue());

				//
				GenderedDouble percentages = StatisticsWorker.genderPercentageDistribution(genders);
				result.put(StatisticTag.RATE_OF_MEN.name(), percentages.getMenValue());
				result.put(StatisticTag.RATE_OF_WOMEN.name(), percentages.getWomenValue());
				result.put(StatisticTag.RATE_OF_UNKNOWN.name(), percentages.getUnknownValue());

				//
				result.put(StatisticTag.NUM_OF_UNIONS.name(), StatisticsWorker.numberOfPartnerships(source.families()));
				int marriageCount = StatisticsWorker.numberOfMarriages(source.families());
				result.put(StatisticTag.NUM_OF_MARRIAGES.name(), marriageCount);
				result.put(StatisticTag.DENSITY_OF_MARRIAGES.name(), StatisticsWorker.densityOfMarriages(marriageCount, source.individuals().size()));

				result.put(StatisticTag.NUM_OF_NON_SINGLE_MEN.name(), StatisticsWorker.numberOfNotSingles(source.individuals(), Gender.MALE));
				result.put(StatisticTag.NUM_OF_NON_SINGLE_WOMEN.name(), StatisticsWorker.numberOfNotSingles(source.individuals(), Gender.FEMALE));

				result.put(StatisticTag.NUM_OF_RELATION_MODELS.name(), source.relationModels().size());

				result.put(StatisticTag.NUM_OF_RELATIONS.name(), source.relations().size());

				int filiationCount = StatisticsWorker.numberOfFiliationTies(source.families());
				result.put(StatisticTag.NUM_OF_PARENT_CHILD_TIES.name(), filiationCount);
				result.put(StatisticTag.DENSITY_OF_FILIATION.name(), StatisticsWorker.densityOfFiliations(filiationCount, source.individuals().size()));

				int numberOfFertileMarriages = StatisticsWorker.numberOfFertileMarriages(source.families());
				result.put(StatisticTag.NUM_OF_FERTILE_MARRIAGES.name(), numberOfFertileMarriages);
				result.put(StatisticTag.RATE_OF_FERTILE_MARRIAGES.name(), MathUtils.percent(numberOfFertileMarriages, marriageCount));

				result.put(StatisticTag.NUM_OF_COWIFE_RELATIONS.name(), StatisticsWorker.numberOfCoSpouseRelations(source.individuals(), Gender.FEMALE));
				result.put(StatisticTag.NUM_OF_COHUSBAND_RELATIONS.name(), StatisticsWorker.numberOfCoSpouseRelations(source.individuals(), Gender.MALE));

				//
				IntWithMax component = null;
				try
				{
					component = StatisticsWorker.numberOfComponents(source.individuals());
					result.put(StatisticTag.NUM_OF_COMPONENTS.name(), component.value());
					result.put(StatisticTag.MAX_OF_COMPONENTS.name(), component.max());
				}
				catch (Exception exception)
				{
					logger.warn("Error on components.", exception);
					errorCount += 1;
				}

				//
				try
				{
					Partition<Individual> agnaticPartition = PartitionMaker.createRaw("PATRIC partition", source.individuals(), "PATRIC");

					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_AGNATIC.name(), agnaticPartition.meanShare());
					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_AGNATIC_WO_SINGLETON.name(), agnaticPartition.meanShare(2));
					result.put(StatisticTag.MAX_OF_COMPONENTS_SHARE_AGNATIC.name(), agnaticPartition.maxShare());
				}
				catch (PuckException exception)
				{
					logger.warn("Error on PartitionMaker.", exception);
					errorCount += 1;
				}

				try
				{
					Partition<Individual> uterinePartition = PartitionMaker.createRaw("MATRIC partition", source.individuals(), "MATRIC");

					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_UTERINE.name(), uterinePartition.meanShare());
					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_UTERINE_WO_SINGLETON.name(), uterinePartition.meanShare(2));
					result.put(StatisticTag.MAX_OF_COMPONENTS_SHARE_UTERINE.name(), uterinePartition.maxShare());
				}
				catch (PuckException exception)
				{
					logger.warn("Error on PartitionMaker.", exception);
					errorCount += 1;
				}

				result.put(StatisticTag.NUM_OF_ELEMENTARY_CYCLES.name(), StatisticsWorker.cyclomatic(marriageCount, filiationCount, source.individuals().size(), component.value()));

				try
				{
					Chronometer chronoBis = new Chronometer();
					result.put(StatisticTag.DEPTH.name(), StatisticsWorker.depth(source.individuals()));
					result.put(StatisticTag.DEPTH_MEAN.name(), StatisticsWorker.meanDepth(source.individuals()));

					logger.debug("depth time: {}s", chronoBis.step().interval() / 1000);
				}
				catch (Exception exception)
				{
					logger.warn("Error on depth.", exception);
					errorCount += 1;
				}

				result.put(StatisticTag.MEAN_SPOUSE_OF_MEN.name(), StatisticsWorker.meanNumberOfSpouses(source.individuals(), Gender.MALE));
				result.put(StatisticTag.MEAN_SPOUSE_OF_WOMEN.name(), StatisticsWorker.meanNumberOfSpouses(source.individuals(), Gender.FEMALE));

				result.put(StatisticTag.MEAN_AGNATIC_FRATRY_SIZE.name(), StatisticsWorker.meanSibsetSize(source.individuals(), Gender.MALE));
				result.put(StatisticTag.MEAN_UTERINE_FRATRY_SIZE.name(), StatisticsWorker.meanSibsetSize(source.individuals(), Gender.FEMALE));
				result.put(StatisticTag.MEAN_CHILDREN_PER_FERTILE_COUPLE.name(), StatisticsWorker.meanSibsetSize(source.families()));

				logger.debug("Total Time: {}s", chrono.step().interval() / 1000);

				//
				result.put(StatisticTag.BASIC_GSTATISTICS_ERROR.name(), errorCount);
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN basic statistics building.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public static Properties buildBasicStatistics(final RelationModel source)
	{
		Properties result;

		result = new Properties();

		try
		{
			int errorCount = 0;

			if (source != null)
			{
				RoleRelationWorker worker = new RoleRelationWorker(source, 2);

				result.put(StatisticTag.SELF_NAME.name(), worker.getRelations().getSelfName());
				result.put(StatisticTag.NUM_OF_TERMS.name(), worker.getTerms().size());
				result.put(StatisticTag.NUM_OF_FEMALES.name(), worker.getTermsByGender(Gender.FEMALE).size());
				result.put(StatisticTag.NUM_OF_FEMALES_EXCLUSIVE.name(), worker.getTermsByExclusiveGender(Gender.FEMALE).size());
				result.put(StatisticTag.NUM_OF_FEMALES_SPEAKER.name(), worker.getTermsByEgoGender(Gender.FEMALE).size());
				result.put(StatisticTag.NUM_OF_FEMALES_SPEAKER_EXCLUSIVE.name(), worker.getTermsByExclusiveEgoGender(Gender.FEMALE).size());

				result.put(StatisticTag.NUM_OF_MALES.name(), worker.getTermsByGender(Gender.MALE).size());
				result.put(StatisticTag.NUM_OF_MALES_EXCLUSIVE.name(), worker.getTermsByExclusiveGender(Gender.MALE).size());
				result.put(StatisticTag.NUM_OF_MALES_SPEAKER.name(), worker.getTermsByEgoGender(Gender.MALE).size());
				result.put(StatisticTag.NUM_OF_MALES_SPEAKER_EXCLUSIVE.name(), worker.getTermsByExclusiveEgoGender(Gender.MALE).size());

				result.put(StatisticTag.GENERATION_PATTERN_FEMALE.name(), worker.getGenerationPatterns()[1]);
				result.put(StatisticTag.GENERATION_PATTERN_MALE.name(), worker.getGenerationPatterns()[0]);
				result.put(StatisticTag.GENERATION_PATTERN_TOTAL.name(), worker.getGenerationPatterns()[3]);

				result.put(StatisticTag.NUM_OF_AUTORECIPROCAL_TERMS.name(), worker.getAutoReciprocalRoles().size());
				result.put(StatisticTag.RECURSIVE_TERMS.name(), worker.getRecursiveRolePattern());
				result.put(StatisticTag.ASCENDANT_CLASSIFICATION.name(), worker.getCollateralClassification()[0].name());
				result.put(StatisticTag.COUSIN_CLASSIFICATION.name(), worker.getCollateralClassification()[1].name());

				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_COMPONENTS.name(), worker.getGraphStatistics(Gender.FEMALE, "NRCOMPONENTS").intValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_CONCENTRATION.name(), worker.getGraphStatistics(Gender.FEMALE, "CONCENTRATION").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name(), worker.getGraphStatistics(Gender.FEMALE, "DENSITY").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT.name(), worker.getGraphStatistics(Gender.FEMALE, "MEANCLUSTERINGCOEFF").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE.name(), worker.getGraphStatistics(Gender.FEMALE, "MEANDEGREE").doubleValue());

				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_COMPONENTS.name(), worker.getGraphStatistics(Gender.MALE, "NRCOMPONENTS").intValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_CONCENTRATION.name(), worker.getGraphStatistics(Gender.MALE, "CONCENTRATION").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name(), worker.getGraphStatistics(Gender.MALE, "DENSITY").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT.name(), worker.getGraphStatistics(Gender.MALE, "MEANCLUSTERINGCOEFF").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE.name(), worker.getGraphStatistics(Gender.MALE, "MEANDEGREE").doubleValue());

				//
				result.put(StatisticTag.BASIC_TSTATISTICS_ERROR.name(), errorCount);
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN STAG REFRESHING", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildCircuitStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			int errorCount = 0;

			if (source != null)
			{
				Chronometer chrono = new Chronometer().start();

				//
				try
				{
					CountAndRate data = analyzeCircuitCensus(CROSS_FIRST_COUSIN_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES.name(), data.getRate());

					logger.debug("step 08: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on cross first cousin marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(LEVIRATE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_LEVIRATE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_LEVIRATE_MARRIAGES.name(), data.getRate());

					logger.debug("step 09: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on levirate marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(SORORATE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_SORORATE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_SORORATE_MARRIAGES.name(), data.getRate());

					logger.debug("step 10: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on sororate marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(DOUBLE_OR_EXCHANGE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name(), data.getRate());

					logger.debug("step 11: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on double or exchange marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(NIECE_NEPHEW_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_NIECE_NEPHEW_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_NIECE_NEPHEW_MARRIAGES.name(), data.getRate());

					logger.debug("step 12: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on Niece/Nephew marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(PARALLEL_FIRST_COUSIN_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name(), data.getRate());

					logger.debug("step 13: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on parallel first cousin marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(FIRST_COUSIN_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_FIRST_COUSIN_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_FIRST_COUSIN_MARRIAGES.name(), data.getRate());

					logger.debug("step 14: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on first cousin marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(DOUBLE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_DOUBLE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_DOUBLE_MARRIAGES.name(), data.getRate());

					logger.debug("step 15: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on double marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(EXCHANGE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_EXCHANGE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_EXCHANGE_MARRIAGES.name(), data.getRate());

					logger.debug("step 16: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on exchange marriages.", exception);
					errorCount += 1;
				}

				//
				result.put(StatisticTag.BASIC_GSTATISTICS_ERROR.name(), errorCount);
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN basic statistics building.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildControlStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			for (ControlReporter.ControlType type : ControlReporter.ControlType.values())
			{
				Report report = ControlReporter.reportControls(source, (ResourceBundle) null, type);
				if (report != null)
				{
					result.put(type.name(), report.status());
				}
			}
		}
		catch (Exception exception)
		{
			logger.error("Error controls.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildControlStatistics(final RelationModel source)
	{
		Properties result;

		result = new Properties();

		try
		{
			for (RelationModelReporter.ControlType type : RelationModelReporter.ControlType.values())
			{
				Report report = RelationModelReporter.reportControl(source, type);
				if (report != null)
				{
					result.put(type.name(), report.status());
				}
			}
		}
		catch (Exception exception)
		{
			logger.error("Error controls.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildGeographyStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			if (source != null)
			{
				result.put(StatisticTag.GEOGRAPHY_AVAILABLE.name(), (source.getGeography2() != null));
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN geography statistics building.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicAncestorChains(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 7
		try
		{
			Chronometer chrono = new Chronometer();
			MultiPartition<Chain> partition = CircuitFinder.createAncestorChains(new Segmentation(source), 3);
			ReportChart reportChart = StatisticsReporter.createMultiPartitionChart(partition);
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 07: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicBiasNetWeights(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 2
		try
		{
			Chronometer chrono = new Chronometer();
			BIASCounts counts = StatisticsWorker.biasNetWeights(source.individuals());
			ReportChart reportChart = StatisticsReporter.createGenderBIASNetWeightChart(counts);
			BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
			result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);

			logger.debug("graphic 02: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicBiasWeights(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 1
		try
		{
			Chronometer chrono = new Chronometer();
			BIASCounts counts = StatisticsWorker.biasWeights(source.individuals());
			ReportChart reportChart = StatisticsReporter.createGenderBIASWeightChart(counts);
			BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
			result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);

			logger.debug("graphic 01: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicCompleteness(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 4
		try
		{
			Chronometer chrono = new Chronometer();
			FiliationCounts counts = StatisticsWorker.completeness(source.individuals(), 10);
			ReportChart reportChart = StatisticsReporter.createCompletenessChart(counts);
			BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
			result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);

			logger.debug("graphic 04: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicComponents(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 3
		try
		{
			Chronometer chrono = new Chronometer();
			ReportChart reportChart = StatisticsReporter.createComponentsChart(source.individuals(), 1000);
			BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
			result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);

			logger.debug("graphic 03: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicConsanguinePairs(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 8
		try
		{
			Chronometer chrono = new Chronometer();
			double[][] consCount = StatisticsWorker.countConsanguinePairs(new Segmentation(source), 3);
			ReportChart reportChart = StatisticsReporter.createArrayChart("Consanguines per person", consCount, new String[] { "HH", "FF", "HF", "FH" });
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				reportChart.setHeadersLegend("Canonic Degree");
				reportChart.setLinesLegend("Consanguines");
				// chart.setVerticalMax(100.0);
				reportChart.setIntegerHorizontalUnit(true);

				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 08: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicFirstCousinMarriages(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 6
		try
		{
			Chronometer chrono = new Chronometer();
			Partition<Chain> partition = CircuitFinder.createFirstCousinMarriages(new Segmentation(source));
			ReportChart reportChart = StatisticsReporter.createPartitionChart(partition);
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 06: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicGenders(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 9
		try
		{
			Chronometer chrono = new Chronometer();
			PartitionCriteria partitionCriteria = new PartitionCriteria(IndividualValuator.EndogenousLabel.GENDER.name());
			partitionCriteria.setType(PartitionType.RAW);
			Partition<Individual> partition = PartitionMaker.create("Gender", source.individuals(), partitionCriteria);
			ReportChart reportChart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, null);
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				reportChart.setTitle("Genders");
				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 09: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicPEDG2(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 10
		try
		{
			Chronometer chrono = new Chronometer();
			PartitionCriteria partitionCriteria = PartitionCriteria.createRaw(IndividualValuator.EndogenousLabel.PEDG.name(), String.valueOf(2));
			Partition<Individual> partition = PartitionMaker.create("PEDG 2", source.individuals(), partitionCriteria);
			ReportChart reportChart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, new PartitionCriteria(EndogenousLabel.GENDER.name()));
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				reportChart.setTitle("PEDG 2");
				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 10: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicPEDG3(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 11
		try
		{
			Chronometer chrono = new Chronometer();
			PartitionCriteria partitionCriteria = PartitionCriteria.createRaw(IndividualValuator.EndogenousLabel.PEDG.name(), String.valueOf(3));
			Partition<Individual> partition = PartitionMaker.create("PEDG 3", source.individuals(), partitionCriteria);
			ReportChart reportChart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, new PartitionCriteria(EndogenousLabel.GENDER.name()));
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				reportChart.setTitle("PEDG 3");
				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 11: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicSibsetDistribution(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 5
		try
		{
			Chronometer chrono = new Chronometer();
			FiliationCounts counts = StatisticsWorker.sibsetDistribution(source.individuals());
			ReportChart reportChart = StatisticsReporter.createSibsetDistributionChart(counts);
			BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
			result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);

			logger.debug("graphic 05: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFiles buildGraphicStatistics(final Net source)
	{
		LoadedFiles result;

		result = new LoadedFiles();

		int width = GENEALOGY_GRAPHIC_WIDTH;
		int height = GENEALOGY_GRAPHIC_HEIGHT;

		result.add(buildGraphicBiasWeights(source, width, height));
		result.add(buildGraphicBiasNetWeights(source, width, height));
		result.add(buildGraphicComponents(source, width, height));
		result.add(buildGraphicCompleteness(source, width, height));
		result.add(buildGraphicSibsetDistribution(source, width, height));
		result.add(buildGraphicFirstCousinMarriages(source, width, height));
		result.add(buildGraphicAncestorChains(source, width, height));
		result.add(buildGraphicConsanguinePairs(source, width, height));
		result.add(buildGraphicGenders(source, width, height));
		result.add(buildGraphicPEDG2(source, width, height));
		result.add(buildGraphicPEDG3(source, width, height));
		result.add(buildGraphicUnionStatus(source, width, height));

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFiles buildGraphicStatistics(final RelationModel source)
	{
		LoadedFiles result;

		logger.debug("Building terminology graphic statistics");

		result = new LoadedFiles();

		File tmpFile = null;
		try
		{
			Net net = RoleRelationMaker.createNet(source, 2);
			EntityData[] data = Puck2KinOath.convert(net.individuals());
			KinOathDiagram diagram = new KinOathDiagram(data);

			//
			tmpFile = new File(System.getProperty("java.io.tmpdir") + "/diagram-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + ".svg");
			logger.debug("tmp=[{}]", tmpFile);
			KinOathFile.save(tmpFile, diagram, "Generated by https://www.kinsources.net/");
			byte[] buffer = FileUtils.readFileToByteArray(tmpFile);
			LoadedFile target = new LoadedFile("kinoath-diagram.svg", buffer);

			result.add(target);
		}
		catch (PuckException exception)
		{
			exception.printStackTrace();
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
		}
		finally
		{
			if (tmpFile != null)
			{
				tmpFile.delete();
			}
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static LoadedFile buildGraphicUnionStatus(final Net source, final int width, final int height)
	{
		LoadedFile result;

		// 12
		try
		{
			Chronometer chrono = new Chronometer();
			PartitionCriteria partitionCriteria = new PartitionCriteria(IndividualValuator.EndogenousLabel.UNIONSTATUS.name());
			partitionCriteria.setType(PartitionType.RAW);
			Partition<Individual> partition = PartitionMaker.create("Union Status", source.individuals(), partitionCriteria);
			ReportChart reportChart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, new PartitionCriteria(EndogenousLabel.GENDER.name()));
			if (reportChart == null)
			{
				result = null;
			}
			else
			{
				reportChart.setTitle("Union Status");
				BufferedImage image = ReportChartMaker.createBufferedImage(reportChart, width, height);
				result = Kiwa.saveImage(reportChart.getTitle() + ".png", image);
			}

			logger.debug("graphic 12: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
		}
		catch (Exception exception)
		{
			logger.error("Error graphics.", exception);
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public static LoadedFiles refreshStatistics(final Statistics target, final Net net)
	{
		LoadedFiles result;

		result = new LoadedFiles();

		if ((target != null) && (net != null))
		{
			Chronometer chrono = new Chronometer();

			target.clear();
			target.add(StatisticTag.COMPUTING_BEGIN_DATETIME.name(), DateTime.now().toString());
			target.addAll(Stag.buildControlStatistics(net));
			target.add(StatisticTag.COMPUTING_CONTROLS_TIME.name(), chrono.step().runningInterval() / 1000);
			target.addAll(Stag.buildBasicStatistics(net));
			target.add(StatisticTag.COMPUTING_BASICS_TIME.name(), chrono.step().runningInterval() / 1000);
			target.addAll(Stag.buildGeographyStatistics(net));
			target.add(StatisticTag.COMPUTING_GEOGRAPHY_TIME.name(), chrono.step().runningInterval() / 1000);

			target.attributeDescriptors().addAll(Stag.buildAttributeDescriptorStatistics(net));
			target.add(StatisticTag.COMPUTING_ATTRIBUTES_TIME.name(), chrono.step().runningInterval() / 1000);
			target.addAll(Stag.buildCircuitStatistics(net));
			target.add(StatisticTag.COMPUTING_CIRCUITS_TIME.name(), chrono.step().runningInterval() / 1000);

			LoadedFiles graphicFilesRaw = Stag.buildGraphicStatistics(net);
			for (LoadedFile loadedFile : graphicFilesRaw)
			{
				LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), loadedFile.header());
				target.graphics().add(header);
				result.add(new LoadedFile(header, loadedFile.data()));
			}

			//
			target.add(StatisticTag.GRAPHICS_COUNT.name(), target.graphics().size());
			target.add(StatisticTag.COMPUTING_TIME.name(), chrono.step().interval() / 1000);
			target.add(StatisticTag.COMPUTING_END_DATETIME.name(), DateTime.now().toString());
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 * @Deprecated See Kiwa.rebuildStatisticsByThread
	 */
	@Deprecated
	public static LoadedFiles refreshStatistics(final Statistics target, final RelationModel model)
	{
		LoadedFiles result;

		result = new LoadedFiles();

		if ((target != null) && (model != null))
		{
			Chronometer chrono = new Chronometer();

			target.clear();
			target.addAll(Stag.buildControlStatistics(model));
			target.addAll(Stag.buildBasicStatistics(model));

			LoadedFiles graphicFilesRaw = Stag.buildGraphicStatistics(model);
			for (LoadedFile loadedFile : graphicFilesRaw)
			{
				LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), loadedFile.header());
				target.graphics().add(header);
				result.add(new LoadedFile(header, loadedFile.data()));
			}

			//
			target.add(StatisticTag.GRAPHICS_COUNT.name(), target.graphics().size());
			target.add(StatisticTag.COMPUTING_TIME.name(), chrono.step().interval() / 1000);
			target.add(StatisticTag.COMPUTING_END_DATETIME.name(), DateTime.now().toString());
		}

		logger.debug("Done.");

		//
		return result;
	}
}
