/**
 * Copyright 2017      Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.stag;

import java.util.Comparator;

import org.kinsources.kiwa.utils.KiwaUtils;

/**
 * 
 * @author TIP
 */
public class StatisticTagComparator implements Comparator<StatisticTag>
{
	public enum Criteria
	{
		NAME
	}

	private Criteria criteria;

	/**
	 * 
	 * @param criteria
	 */
	public StatisticTagComparator(final Criteria criteria)
	{
		this.criteria = criteria;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final StatisticTag alpha, final StatisticTag bravo)
	{
		int result;

		result = compare(alpha, bravo, this.criteria);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final StatisticTag alpha, final StatisticTag bravo, final Criteria criteria)
	{
		int result;

		switch (criteria)
		{
			case NAME:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.name();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.name();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			default:
				throw new IllegalStateException("Unknown criteria [" + criteria + "]");
		}

		//
		return result;
	}
}
