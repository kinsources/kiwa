/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actulog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.util.StringList;
import fr.devinsy.util.StringListWriter;
import fr.devinsy.util.rss.RSSElement;
import fr.devinsy.util.rss.RSSWriter;

/**
 * 
 * @author Christian P. Momon
 */
public class Actulog
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Actulog.class);
	public static final String RSS_NAME = "kinsources_news";
	private long lastId;
	private Wires wires;
	private WireIdIndex idWireIndex;

	/**
	 * 
	 */
	public Actulog()
	{
		this.lastId = 0;
		this.wires = new Wires();
		this.idWireIndex = new WireIdIndex();
	}

	/**
	 * 
	 */
	public Actulog(final long autoIncrementValue)
	{
		this.lastId = autoIncrementValue;
		this.wires = new Wires();
		this.idWireIndex = new WireIdIndex();
	}

	/**
	 * 
	 */
	public Wire createWire(final String title, final Locale locale)
	{
		Wire result;

		result = new Wire(nextId(), title, locale);
		this.wires.add(result);
		this.idWireIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Wire getWireById(final long wireId)
	{
		Wire result;

		result = this.idWireIndex.getById(wireId);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Wire getWireById(final Long wireId)
	{
		Wire result;

		if (wireId == null)
		{
			result = null;
		}
		else
		{
			result = this.idWireIndex.getById(wireId);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	public Wire getWireById(final String wireId)
	{
		Wire result;

		Long targetId;
		if (NumberUtils.isDigits(wireId))
		{
			targetId = Long.parseLong(wireId);
		}
		else
		{
			targetId = null;
		}

		//
		result = getWireById(targetId);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastId()
	{
		long result;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextId()
	{
		long result;

		this.lastId += 1;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void rebuildIndexes()
	{
		this.idWireIndex.rebuild(this.wires);
	}

	/**
	 * 
	 * @param account
	 */
	public void removeWire(final Wire wire)
	{
		//
		this.wires.remove(wire);

		//
		this.idWireIndex.remove(wire);
	}

	/**
	 * 
	 */
	public void resetLastId()
	{
		this.lastId = this.wires.lastId();
	}

	/**
	 * 
	 * @param language
	 * @return
	 * @throws IOException
	 */
	public String toRSS(final int feedSize, final Locale locale, final String websiteUrl) throws IOException
	{
		String result;

		//
		ResourceBundle bundle = ResourceBundle.getBundle("org.kinsources.kiwa.actulog.rss", locale);

		//
		StringListWriter target = new StringListWriter();
		RSSWriter out = new RSSWriter(target);

		//
		List<RSSElement> elements = new ArrayList<RSSElement>();
		elements.add(new RSSElement("pubDate", DateTime.now()));
		elements.add(new RSSElement("lastBuildDate", DateTime.now()));
		elements.add(new RSSElement("generator", "Generated by Kiwa"));
		elements.add(new RSSElement("language", locale.getLanguage()));

		//
		out.writeChannel(bundle.getString("FEED_TITLE"), websiteUrl, bundle.getString("FEED_DESCRIPTION"), elements.toArray(new RSSElement[0]));

		//
		if (this.wires.isEmpty())
		{
			out.writeItem("No event.", null, null, new RSSElement("category", "system"), new RSSElement("author", "Kiwa"));
		}
		else
		{
			for (Wire wire : this.wires.latestPublishedWires(feedSize, locale))
			{
				//
				elements.clear();
				elements.add(new RSSElement("guid", "kinsources_news_" + wire.getId(), "isPermaLink", "false"));
				elements.add(new RSSElement("pubDate", wire.getPublicationDate()));
				if (wire.getAuthor() != null)
				{
					elements.add(new RSSElement("author", wire.getAuthor()));
				}

				//
				String wireLink;
				StringList linkBuffer = new StringList();
				linkBuffer.append(websiteUrl);
				linkBuffer.append(SimpleServletDispatcher.rewriteShortUrl("/actulog/wire", "xhtml", String.valueOf(wire.getId()), wire.getTitle()));
				wireLink = linkBuffer.toString();
				elements.add(new RSSElement("link", wireLink));

				//
				out.writeItem(wire.getTitle(), wireLink, wire.getLeadParagraph(), elements.toArray(new RSSElement[0]));
			}
		}

		//
		out.close();

		//
		result = target.toString();

		logger.debug("rss={}", result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Wires wires()
	{
		return this.wires;
	}
}
