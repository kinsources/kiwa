/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.util.Properties;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.kinsources.kiwa.accounts.Account;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Request
{
	/**
	 * 
	 */
	public enum Status
	{
		OPENED,
		CANCELLED,
		ACCEPTED,
		ACCEPTED_WITH_REVISIONS,
		DECLINED
	}

	/**
	 * 
	 */
	public enum Type
	{
		DATASET_PUBLISH,
		DATASET_UNPUBLISH,
		COOPTATION,
		UNCOOPTATION,
		ISSUE
	}

	private long id;
	private Status status;
	private long contributorId;
	private DateTime creationDate;
	private String contributorComment;
	private Type type;
	private Properties properties;
	private Votes votes;
	private Comments comments;
	private String decisionComment;
	private Long decisionMakerId;
	private DateTime decisionDate;

	/**
	 * 
	 */
	public Request(final long id, final long contributorId, final String contributorComment, final Type type, final Properties parameters)
	{
		this.id = id;
		this.status = Status.OPENED;
		this.contributorId = contributorId;
		this.creationDate = DateTime.now();
		this.contributorComment = contributorComment;
		this.type = type;
		this.properties = new Properties();
		if (parameters != null)
		{
			this.properties.putAll(parameters);
		}
		this.votes = new Votes();
		this.comments = new Comments();
	}

	/**
	 * 
	 * @return
	 */
	public Comments comments()
	{
		return this.comments;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfComments()
	{
		long result;

		result = this.comments().size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfVotes()
	{
		long result;

		result = this.votes().size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int delay()
	{
		int result;

		if (this.decisionDate == null)
		{
			result = Days.daysBetween(this.creationDate, DateTime.now()).getDays();
		}
		else
		{
			result = Days.daysBetween(this.creationDate, this.decisionDate).getDays();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getContributorComment()
	{
		String result;

		if (this.contributorComment == null)
		{
			result = "";
		}
		else
		{
			result = this.contributorComment;
		}

		//
		return result;
	}

	public long getContributorId()
	{
		return this.contributorId;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public String getDecisionComment()
	{
		return this.decisionComment;
	}

	public DateTime getDecisionDate()
	{
		return this.decisionDate;
	}

	public Long getDecisionMakerId()
	{
		return this.decisionMakerId;
	}

	public long getId()
	{
		return this.id;
	}

	public Status getStatus()
	{
		return this.status;
	}

	public Type getType()
	{
		return this.type;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isContributor(final Account account)
	{
		boolean result;

		if ((account == null) || (this.contributorId != account.getId()))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	public Properties properties()
	{
		return this.properties;
	}

	/**
	 * 
	 */
	public void setAccepted(final long decisionMakerId, final String comment)
	{
		setDecision(Status.ACCEPTED, decisionMakerId, comment);
	}

	/**
	 * 
	 */
	public void setAcceptedWithRevisions(final long decisionMakerId, final String comment)
	{
		setDecision(Status.ACCEPTED_WITH_REVISIONS, decisionMakerId, comment);
	}

	/**
	 * 
	 */
	public void setCanceled(final long decisionMakerId, final String comment)
	{
		setDecision(Status.CANCELLED, decisionMakerId, comment);
	}

	public void setContributorComment(final String contributorComment)
	{
		this.contributorComment = contributorComment;
	}

	public void setContributorId(final long contributorId)
	{
		this.contributorId = contributorId;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	/**
	 * 
	 */
	public void setDecision(final Status decisionStatus, final long decisionMakerId, final String comment)
	{
		if (decisionStatus != null)
		{
			this.status = decisionStatus;
			this.decisionDate = DateTime.now();
			this.decisionMakerId = decisionMakerId;
			this.decisionComment = comment;
		}
	}

	public void setDecisionComment(final String decisionComment)
	{
		this.decisionComment = decisionComment;
	}

	public void setDecisionDate(final DateTime decisionDate)
	{
		this.decisionDate = decisionDate;
	}

	public void setDecisionMakerId(final Long decisionMakerId)
	{
		this.decisionMakerId = decisionMakerId;
	}

	/**
	 * 
	 */
	public void setDeclined(final long decisionMakerId, final String comment)
	{
		setDecision(Status.DECLINED, decisionMakerId, comment);
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	public void setType(final Type type)
	{
		this.type = type;
	}

	public Votes votes()
	{
		return this.votes;
	}

	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public Vote.Value voteValueOf(final long accountId)
	{
		Vote.Value result;

		Vote vote = this.votes().getByOwnerId(accountId);
		if (vote == null)
		{
			result = Vote.Value.NONE;
		}
		else
		{
			result = vote.getValue();
		}

		//
		return result;
	}
}
