/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Account
{
	public enum EmailScope
	{
		PUBLIC,
		PRIVATE
	}

	public enum Status
	{
		NOT_CREATED,
		CREATED,
		ACTIVATED,
		SUSPENDED,
		REMOVED,
	}

	public static final int NO_ID = 0;

	private long id;
	private Status status;
	private String firstNames;
	private String lastName;
	public static final String NAME_PATTERN = "[^<>=+!\\f\\n\\t\\r]*[^<>=+!\\f\\n\\s\\t\\r]{2,}[^<>=+!\\f\\n\\t\\r]*";
	private String email;
	public static final String EMAIL_PATTERN = ".+@.+\\..+";
	private EmailScope emailScope;
	private Password password;
	public static final String PASSWORD_PATTERN = ".*[^\\s]{8,}.*";
	private DateTimeZone timeZone;
	private DateTime creationDate;
	private DateTime editionDate;
	private DateTime lastConnection;
	private Roles roles;
	private Attributes attributes;
	private String businessCard;
	private String country;
	private boolean emailNotification;
	private String organization;
	private String website;
	public static final String PATTERN_WEBSITE = "https?://[\\w\\d][\\.\\w\\d]*\\.[\\w\\d]+/?.*";

	/**
     * 
     */
	public Account(final long id, final String email, final String firstNames, final String lastName, final String password)
	{
		this.id = id;
		this.status = Status.CREATED;
		setFullName(firstNames, lastName);
		setEmail(email);
		this.emailScope = EmailScope.PRIVATE;
		this.password = new Password();
		setPassword(password);
		this.creationDate = this.editionDate;
		this.timeZone = DateTimeZone.getDefault();
		this.roles = new Roles();
		this.attributes = new Attributes();
		this.emailNotification = false;
	}

	public Attributes attributes()
	{
		return this.attributes;
	}

	public String getBusinessCard()
	{
		return this.businessCard;
	}

	public String getCountry()
	{
		return this.country;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public String getEmail()
	{
		return this.email;
	}

	public EmailScope getEmailScope()
	{
		return this.emailScope;
	}

	public String getFirstNames()
	{
		return this.firstNames;
	}

	/**
	 * 
	 * @return
	 */
	public String getFullName()
	{
		String result;

		if (StringUtils.isBlank(this.firstNames))
		{
			if (StringUtils.isBlank(this.lastName))
			{
				result = null;
			}
			else
			{
				result = this.lastName;
			}
		}
		else
		{
			if (StringUtils.isBlank(this.lastName))
			{
				result = this.firstNames;
			}
			else
			{
				result = this.firstNames + " " + this.lastName;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getFullNameReversed()
	{
		String result;

		if (StringUtils.isBlank(this.firstNames))
		{
			if (StringUtils.isBlank(this.lastName))
			{
				result = null;
			}
			else
			{
				result = this.lastName;
			}
		}
		else
		{
			if (StringUtils.isBlank(this.lastName))
			{
				result = this.firstNames;
			}
			else
			{
				result = this.lastName + " " + this.firstNames;
			}
		}

		//
		return result;
	}

	public long getId()
	{
		return this.id;
	}

	public DateTime getLastConnection()
	{
		return this.lastConnection;
	}

	public String getLastName()
	{
		return this.lastName;
	}

	public String getOrganization()
	{
		return this.organization;
	}

	public Status getStatus()
	{
		return this.status;
	}

	public DateTimeZone getTimeZone()
	{
		return this.timeZone;
	}

	/**
	 * 
	 * @return
	 */
	public String getTimeZoneGMT()
	{
		String result;

		long offset = this.timeZone.getOffset(new Date().getTime());

		long hour = (offset / (60 * 1000)) / 60;
		long min = (offset / (60 * 1000)) % 60;

		result = String.format("GMT%+03d:%02d", hour, min);

		//
		return result;
	}

	public String getWebsite()
	{
		return this.website;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isActivated()
	{
		boolean result;

		if (this.status == Status.ACTIVATED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	public boolean isEmailNotification()
	{
		return this.emailNotification;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotActivated()
	{
		boolean result;

		result = !isActivated();

		//
		return result;
	}

	/**
	 * 
	 * @param roleId
	 * @return
	 */
	public boolean isNotRole(final int roleId)
	{
		boolean result;

		result = !isRole(roleId);

		//
		return result;
	}

	/**
	 * 
	 * @param roleId
	 * @return
	 */
	public boolean isNotRole(final Role role)
	{
		boolean result;

		result = !isRole(role);

		//
		return result;
	}

	/**
	 * 
	 * @param roleId
	 * @return
	 */
	public boolean isRole(final int roleId)
	{
		boolean result;

		result = this.roles.contains(roleId);

		//
		return result;
	}

	/**
	 * 
	 * @param roleId
	 * @return
	 */
	public boolean isRole(final Role role)
	{
		boolean result;

		if (role == null)
		{
			result = false;
		}
		else
		{
			result = isRole(role.getId());
		}

		//
		return result;
	}

	public Password password()
	{
		return this.password;
	}

	public Roles roles()
	{
		return this.roles;
	}

	/**
	 * 
	 * @param businessCard
	 */
	public void setBusinessCard(final String businessCard)
	{
		this.businessCard = businessCard;
	}

	/**
	 * 
	 * @param country
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	/**
	 * 
	 * @param newEmail
	 */
	public void setEmail(final String newEmail)
	{
		if (StringUtils.isBlank(newEmail))
		{
			throw new IllegalArgumentException("email is null.");
		}
		else if (!newEmail.matches(EMAIL_PATTERN))
		{
			throw new IllegalArgumentException("Incorrect format.");
		}
		else
		{
			this.email = newEmail;
		}
	}

	public void setEmailNotification(final boolean emailNotification)
	{
		this.emailNotification = emailNotification;
	}

	/**
	 * 
	 * @param emailScope
	 */
	public void setEmailScope(final EmailScope emailScope)
	{
		if (emailScope == null)
		{
			this.emailScope = EmailScope.PRIVATE;
		}
		else
		{
			this.emailScope = emailScope;
		}
	}

	/**
	 * 
	 * @param firstNames
	 */
	public void setFullName(final String firstNames, final String lastName)
	{
		if (StringUtils.isBlank(firstNames))
		{
			if (StringUtils.isBlank(lastName))
			{
				throw new IllegalArgumentException("Full Name is blank.");
			}
			else
			{
				if (lastName.matches(NAME_PATTERN))
				{
					this.firstNames = null;
					this.lastName = lastName;
				}
				else
				{
					throw new IllegalArgumentException("Incorrect last name format.");
				}
			}
		}
		else
		{
			if (StringUtils.isBlank(lastName))
			{
				if (firstNames.matches(NAME_PATTERN))
				{
					this.firstNames = null;
					this.lastName = firstNames;
				}
				else
				{
					throw new IllegalArgumentException("Incorrect first names format.");
				}
			}
			else
			{
				if (!firstNames.matches(NAME_PATTERN))
				{
					throw new IllegalArgumentException("Incorrect first names format.");
				}
				else if (!lastName.matches(NAME_PATTERN))
				{
					throw new IllegalArgumentException("Incorrect last name format.");
				}
				else
				{
					this.firstNames = firstNames;
					this.lastName = lastName;
				}
			}
		}
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setLastConnection(final DateTime lastConnection)
	{
		this.lastConnection = lastConnection;
	}

	/**
	 * 
	 * @param organization
	 */
	public void setOrganization(final String organization)
	{
		this.organization = organization;
	}

	/**
	 * 
	 * @param password
	 */
	public void setPassword(final String value)
	{
		if (StringUtils.isBlank(value))
		{
			this.password.set(null);
			this.editionDate = this.password.editionDate();

		}
		else if (value.matches(PASSWORD_PATTERN))
		{
			this.password.set(value);
			this.editionDate = this.password.editionDate();

		}
		else
		{
			throw new IllegalArgumentException("Incorrect password format.");
		}
	}

	/**
	 * 
	 * @param password
	 */
	public void setPassword(final String digest, final DateTime editionDate)
	{
		if (digest == null)
		{
			throw new IllegalArgumentException("digest is null.");
		}
		else
		{
			this.password = new Password(digest, editionDate);
		}
	}

	/**
	 * 
	 * @param role
	 * @return
	 */
	public Role setRole(final Role role)
	{
		Role result;

		result = this.roles.setRole(role);

		//
		return result;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	/**
	 * 
	 * @param timeZone
	 */
	public void setTimeZone(final DateTimeZone zone)
	{
		if (zone == null)
		{
			throw new IllegalArgumentException("timeZome is null.");
		}
		else
		{
			this.timeZone = zone;
		}
	}

	/**
	 * 
	 * @param timeZone
	 */
	public void setTimeZone(final String timeZoneID)
	{
		if (StringUtils.isBlank(timeZoneID))
		{
			throw new IllegalArgumentException("timeZoneID is null.");
		}
		else if (TimeZone.getTimeZone(timeZoneID) == null)
		{
			throw new IllegalArgumentException("Unknown timeZone");
		}
		else
		{
			setTimeZone(DateTimeZone.forID(timeZoneID));
		}
	}

	/**
	 * 
	 * @param website
	 */
	public void setWebsite(final String website)
	{
		if ((StringUtils.isBlank(website)) || (website.matches(PATTERN_WEBSITE)))
		{
			this.website = website;
		}
		else
		{
			throw new IllegalArgumentException("Incorrect website format [{" + website + "}].");
		}
	}

	/**
     * 
     */
	@Override
	public String toString()
	{
		String result;

		result = String.valueOf(this.id);

		//
		return result;
	}

	public void touch()
	{

		this.editionDate = DateTime.now();
	}

	/**
     * 
     */
	public void updateLastConnection()
	{

		this.lastConnection = DateTime.now();
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public static Long getId(final Account account)
	{
		Long result;

		if (account == null)
		{
			result = null;
		}
		else
		{
			result = account.getId();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean isActivated(final Account source)
	{
		boolean result;

		if (source == null)
		{
			result = false;
		}
		else
		{
			result = source.isActivated();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean isNotActivated(final Account source)
	{
		boolean result;

		result = !isActivated(source);

		//
		return result;
	}

	/**
	 * 
	 * @param role
	 * @return
	 */
	public static boolean isNotRole(final Account account, final Role role)
	{
		boolean result;

		result = !isRole(account, role);

		//
		return result;
	}

	/**
	 * 
	 * @param role
	 * @return
	 */
	public static boolean isRole(final Account account, final Role role)
	{
		boolean result;

		if (account == null)
		{
			result = false;
		}
		else
		{
			result = account.isRole(role);
		}

		//
		return result;
	}
}
