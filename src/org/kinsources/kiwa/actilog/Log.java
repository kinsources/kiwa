/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actilog;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Log
{
	public enum Level
	{
		ALL,
		DEBUG,
		INFO,
		WARN,
		ERROR,
		FATAL,
		OFF
	}

	private long id;
	private DateTime time;
	private Level level;
	private String event;
	private String details;

	/**
	 * 
	 * @param category
	 * @param event
	 * @param comment
	 */
	public Log(final long id, final DateTime time, final Level level, final String event, final String details)
	{
		//
		if (level == null)
		{
			throw new IllegalArgumentException("level is null");
		}
		else if (event == null)
		{
			throw new IllegalArgumentException("event is null");
		}
		else
		{
			this.id = id;
			this.time = time;
			this.level = level;
			this.event = event;
			this.details = details;
		}
	}

	public String getDetails()
	{
		return this.details;
	}

	public String getEvent()
	{
		return this.event;
	}

	public long getId()
	{
		return this.id;
	}

	public Level getLevel()
	{
		return this.level;
	}

	public DateTime getTime()
	{
		return this.time;
	}
}
