/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actilog;

import java.util.Iterator;

import org.joda.time.DateTime;
import org.kinsources.kiwa.actilog.Log.Level;
import org.slf4j.helpers.MessageFormatter;

/**
 * 
 * @author Christian P. Momon
 */
public class Actilog
{
	private Level currentLogLevel;
	private long lastId;
	private Logs logs;

	/**
	 * 
	 */
	public Actilog()
	{
		this.currentLogLevel = Level.ALL;
		this.lastId = 0;
		this.logs = new Logs();
	}

	/**
	 * 
	 */
	public Actilog(final long autoIncrementValue)
	{
		this.currentLogLevel = Level.ALL;
		this.lastId = autoIncrementValue;
		this.logs = new Logs();
	}

	/**
	 * 
	 * @return
	 */
	public Level getLogLevel()
	{
		return this.currentLogLevel;
	}

	public long lastId()
	{
		long result;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	public Log log(final Level level, final String event)
	{
		Log result;

		//
		if (level == null)
		{
			throw new IllegalArgumentException("level is null");
		}
		else if (event == null)
		{
			throw new IllegalArgumentException("event is null");
		}
		else if ((this.currentLogLevel != Level.OFF) && (level.ordinal() >= this.currentLogLevel.ordinal()))
		{
			result = log(level, event, null);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	synchronized public Log log(final Level level, final String event, final String details)
	{
		Log result;

		//
		if (level == null)
		{
			throw new IllegalArgumentException("level is null");
		}
		else if (event == null)
		{
			throw new IllegalArgumentException("event is null");
		}
		else if ((this.currentLogLevel != Level.OFF) && (level.ordinal() >= this.currentLogLevel.ordinal()))
		{
			//
			result = new Log(nextId(), DateTime.now(), level, event, details);
			this.logs.add(result);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	public Log log(final Level level, final String event, final String detailFormat, final Object... details)
	{
		Log result;

		//
		if (level == null)
		{
			throw new IllegalArgumentException("level is null");
		}
		else if (event == null)
		{
			throw new IllegalArgumentException("event is null");
		}
		else if ((this.currentLogLevel != Level.OFF) && (level.ordinal() >= this.currentLogLevel.ordinal()))
		{
			result = log(level, event, MessageFormatter.arrayFormat(detailFormat, details).getMessage());
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Logs logs()
	{
		return this.logs;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextId()
	{
		long result;

		this.lastId += 1;

		result = this.lastId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void purge(final int days)
	{
		//
		DateTime limit = DateTime.now().minusDays(days);

		//
		Iterator<Log> iterator = this.logs.iterator();
		while (iterator.hasNext())
		{
			//
			Log log = iterator.next();

			//
			if (log.getTime().isBefore(limit))
			{
				iterator.remove();
			}
		}
	}

	/**
	 * 
	 */
	public void resetLastId()
	{
		this.lastId = this.logs.lastId();
	}

	/**
	 * 
	 * @param currentLevel
	 */
	public void setLogLevel(final Level currentLevel)
	{
		this.currentLogLevel = currentLevel;
	}
}
