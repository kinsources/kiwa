/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.seligo;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 */
public class Interval implements Comparable<Interval>
{
	public enum EndpointStatus
	{
		INCLUDED,
		EXCLUDED
	}

	private static final Logger logger = LoggerFactory.getLogger(Interval.class);

	private String name;
	private double min;
	private boolean minIncluded;
	private double max;
	private boolean maxIncluded;

	/**
	 * 
	 * @param min
	 * @param max
	 */
	public Interval(final Double min, final Double max)
	{
		set(min, max);
	}

	/**
	 * 
	 * @param min
	 * @param max
	 */
	public Interval(final Double min, final EndpointStatus minStatus, final Double max, final EndpointStatus maxStatus)
	{
		set(min, minStatus, max, maxStatus);
	}

	/**
	 * 
	 * @param min
	 * @param max
	 */
	public Interval(final String min, final String max)
	{
		Double minValue;
		if (NumberUtils.isNumber(min))
		{
			minValue = Double.parseDouble(min);
		}
		else
		{
			minValue = null;
		}

		//
		Double maxValue;
		if (NumberUtils.isNumber(max))
		{
			maxValue = Double.parseDouble(max);
		}
		else
		{
			maxValue = null;
		}

		//
		set(minValue, maxValue);
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(final Interval source)
	{
		int result;

		if (source == null)
		{
			result = 1;
		}
		else if (this.min == source.getMin())
		{
			if ((!this.minIncluded) && (source.isMinIncluded()))
			{
				result = +1;
			}
			else if ((this.minIncluded) && (!source.isMinIncluded()))
			{
				result = -1;
			}
			else
			{
				result = 0;
			}
		}
		else if (this.min < source.getMin())
		{
			result = -1;
		}
		else
		{
			result = +1;
		}

		//
		return result;
	}

	public double getMax()
	{
		return this.max;
	}

	public double getMin()
	{
		return this.min;
	}

	public String getName()
	{
		return this.name;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isBiggest()
	{
		boolean result;

		if ((this.min == Double.NEGATIVE_INFINITY) && (this.max == Double.POSITIVE_INFINITY))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isBiggestPercentage()
	{
		boolean result;

		if ((this.min <= 0) && (this.max >= 100))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isBiggestPositive()
	{
		boolean result;

		if ((this.min <= 0) && (this.max == Double.POSITIVE_INFINITY))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	public boolean isMaxIncluded()
	{
		return this.maxIncluded;
	}

	public boolean isMinIncluded()
	{
		return this.minIncluded;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotBiggest()
	{
		boolean result;

		result = !isBiggest();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotBiggestPercentage()
	{
		boolean result;

		result = !isBiggestPercentage();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotBiggestPositive()
	{
		boolean result;

		result = !isBiggestPositive();

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean matches(final double value)
	{
		boolean result;

		if ((value > this.min) && (value < this.max))
		{
			result = true;
		}
		else if ((value == this.min) && (this.minIncluded))
		{
			result = true;
		}
		else if ((value == this.max) && (this.maxIncluded))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean matches(final Double value)
	{
		boolean result;

		if (value == null)
		{
			result = false;
		}
		else
		{
			result = matches(value.doubleValue());
		}

		// logger.debug("matches [{}]o[{}]=>[{}]", toStringWithName(), value,
		// result);

		//
		return result;
	}

	/**
	 * 
	 * @param min
	 * @param max
	 */
	private void set(final Double min, final Double max)
	{
		set(min, EndpointStatus.INCLUDED, max, EndpointStatus.INCLUDED);
	}

	/**
	 * 
	 * @param min
	 * @param max
	 */
	private void set(final Double min, final EndpointStatus minStatus, final Double max, final EndpointStatus maxStatus)
	{
		this.minIncluded = (minStatus == EndpointStatus.INCLUDED);
		this.maxIncluded = (maxStatus == EndpointStatus.INCLUDED);

		if (min == null)
		{
			this.min = Double.NEGATIVE_INFINITY;
		}
		else
		{
			this.min = min;
		}

		if (max == null)
		{
			this.max = Double.POSITIVE_INFINITY;
		}
		else
		{
			this.max = max;
		}
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * 
	 */
	@Override
	public String toString()
	{
		String result;

		result = toString(this);

		//
		return result;
	}

	/**
	 * 
	 */
	public String toStringWithName()
	{
		String result;

		if (this.name == null)
		{
			result = toString(this);
		}
		else
		{
			result = this.name + "=" + toString(this);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static String toString(final Interval source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			//
			// String targetName;
			// if (source.getName() == null) {
			// targetName = "";
			// } else {
			// targetName = source.getName();
			// }

			//
			char minBoard;
			if (source.isMinIncluded())
			{
				minBoard = '[';
			}
			else
			{
				minBoard = ']';
			}

			char maxBoard;
			if (source.isMaxIncluded())
			{
				maxBoard = ']';
			}
			else
			{
				maxBoard = '[';
			}

			//
			String targetMin;
			if (source.getMin() == Double.NEGATIVE_INFINITY)
			{
				targetMin = "-∞";
			}
			else
			{
				targetMin = MathUtils.toString(source.getMin());
			}

			//
			String targetMax;
			if (source.getMax() == Double.POSITIVE_INFINITY)
			{
				targetMax = "+∞";
			}
			else
			{
				targetMax = MathUtils.toString(source.getMax());
			}

			//
			result = String.format("%c%s,%s%c", minBoard, targetMin, targetMax, maxBoard);
		}

		//
		return result;
	}
}
