/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.seligo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The <code>Intervals</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Intervals implements Iterable<Interval>
{
	private List<Interval> intervals;

	/**
	 * 
	 */
	public Intervals()
	{
		super();

		this.intervals = new ArrayList<Interval>();
	}

	/**
	 * 
	 */
	public Intervals(final int initialCapacity)
	{
		this.intervals = new ArrayList<Interval>(initialCapacity);
	}

	/**
	 * 
	 */
	public Interval add(final Interval source)
	{
		Interval result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.intervals.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	public Interval add(final String name, final String min, final String max)
	{
		Interval result;

		result = new Interval(min, max);
		result.setName(name);

		this.add(result);

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.intervals.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Intervals copy()
	{
		Intervals result;

		result = new Intervals(this.intervals.size());

		for (Interval account : this.intervals)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Interval getByIndex(final int index)
	{
		Interval result;

		result = this.intervals.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.intervals.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Interval> iterator()
	{
		Iterator<Interval> result;

		result = this.intervals.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Interval account)
	{
		this.intervals.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public Intervals reverse()
	{
		Intervals result;

		//
		java.util.Collections.reverse(this.intervals);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.intervals.size();

		//
		return result;
	}
}
