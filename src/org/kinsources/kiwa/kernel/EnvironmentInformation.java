/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kernel;

import javax.mail.Session;
import javax.naming.Binding;
import javax.naming.ConfigurationException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.StringList;

/**
 * 
 * 
 * @author Christian P. Momon
 */
public class EnvironmentInformation
{
	private static final Logger logger = LoggerFactory.getLogger(EnvironmentInformation.class);;

	private String name;
	private String websiteUrl;
	private String log4jFilePathname;
	private String mailSettings;
	private String jdbcSettings;
	private Long mailPauseTime;

	/**
	 * @throws ConfigurationException
	 * 
	 */
	public EnvironmentInformation() throws ConfigurationException
	{

		try
		{
			//
			Context env = (Context) new InitialContext().lookup("java:comp/env");

			//
			this.name = (String) env.lookup("kiwa.environment.name");

			//
			this.websiteUrl = (String) env.lookup("kiwa.website.url");
			if ((this.websiteUrl != null) && (!this.websiteUrl.endsWith("/")))
			{
				this.websiteUrl = this.websiteUrl + "/";
			}

			//
			this.log4jFilePathname = (String) env.lookup("kiwa.log4j.path");

			//
			this.mailPauseTime = (Long) env.lookup("kiwa.eucles.pause_timer");

			//
			Context initialContext = new InitialContext();
			Context environmentContext = (Context) initialContext.lookup("java:comp/env");

			//
			{
				Session session = (Session) environmentContext.lookup("mail/kiwamailer");

				StringList mailSettingsBuffer = new StringList();
				for (Object key : session.getProperties().keySet().toArray())
				{
					mailSettingsBuffer.append((String) key).append("=").append(session.getProperties().getProperty((String) key)).append(",");
				}
				mailSettingsBuffer.removeLast();
				this.mailSettings = mailSettingsBuffer.toString();
			}

			//
			{
				// TODO
				//
				// Apache Tomcat uses tomcat-dbcp.jar
				// Debian Tomcat uses commons-dbcp.jar
				//
				// So, there is a conflict for package of BasicDataSource.

				StringList jdbcSettingsBuffer = new StringList();

				NamingEnumeration<Binding> bindings = environmentContext.listBindings("jdbc");

				Binding binding = bindings.next();
				org.apache.tomcat.jdbc.pool.DataSource dataSource = (org.apache.tomcat.jdbc.pool.DataSource) binding.getObject();
				jdbcSettingsBuffer.append("Driver class name=").append(dataSource.getDriverClassName()).append(",");
				jdbcSettingsBuffer.append("URL=").append(dataSource.getUrl()).append(",");
				jdbcSettingsBuffer.append("Username=").append(dataSource.getUsername());

				this.jdbcSettings = jdbcSettingsBuffer.toString();
				// this.jdbcSettings = "n/a";
			}

		}
		catch (NamingException exception)
		{
			//
			logger.error("Error setting the java:comp/env context: " + exception.getMessage());
			logger.error(ExceptionUtils.getStackTrace(exception));
			logger.error("Environment information:");
			logger.error("- name=[" + this.name + "]");
			logger.error("- websiteUrl=[" + this.websiteUrl + "]");
			logger.error("- log4jPath=[" + this.log4jFilePathname + "]");
			logger.error("- mail settings=[" + this.mailSettings + "]");
			logger.error("- JDBC settings=[" + this.jdbcSettings + "]");
			logger.error("- mailPauseTime=[" + this.mailPauseTime + "]");

			//
			throw new ConfigurationException("Error setting environment information: " + exception.getMessage());

		}
		catch (Exception exception)
		{
			//
			logger.error("Error setting EnvironmentInformation: " + exception.getMessage());
			logger.error(ExceptionUtils.getStackTrace(exception));
			logger.error("Environment information:");
			logger.error("- name=[" + this.name + "]");
			logger.error("- websiteUrl=[" + this.websiteUrl + "]");
			logger.error("- log4jPath=[" + this.log4jFilePathname + "]");
			logger.error("- mail settings=[" + this.mailSettings + "]");
			logger.error("- JDBC settings=[" + this.jdbcSettings + "]");
			logger.error("- mailPauseTime=[" + this.mailPauseTime + "]");

			//
			throw new ConfigurationException("Error setting environment information: " + exception.getMessage());
		}

		// Check availability of all fields.
		if (isNotAvailable())
		{
			//
			logger.error("Environment information:");
			logger.error("- name=[" + this.name + "]");
			logger.error("- websiteUrl=[" + this.websiteUrl + "]");
			logger.error("- log4jPath=[" + this.log4jFilePathname + "]");
			logger.error("- mail settings=[" + this.mailSettings + "]");
			logger.error("- JDBC settings=[" + this.jdbcSettings + "]");
			logger.error("- mailPauseTime=[" + this.mailPauseTime + "]");
			throw new ConfigurationException("Environment information is not fully available.");
		}
	}

	/**
	 * 
	 * @return
	 */
	public boolean isAvailable()
	{
		boolean result;

		if ((StringUtils.isBlank(this.name)) || (StringUtils.isBlank(this.websiteUrl)) || (StringUtils.isBlank(this.log4jFilePathname)) || (StringUtils.isBlank(this.mailSettings))
				|| (StringUtils.isBlank(this.jdbcSettings)) || (this.mailPauseTime == null) || (StringUtils.isBlank(this.jdbcSettings)))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotAvailable()
	{
		boolean result;

		result = !isAvailable();

		return result;
	}

	public String jdbcSettings()
	{
		return this.jdbcSettings;
	}

	public String log4jFilePathname()
	{
		return this.log4jFilePathname;
	}

	public Long mailPauseTime()
	{
		return this.mailPauseTime;
	}

	public String mailSettings()
	{
		return this.mailSettings;
	}

	public String name()
	{
		return this.name;
	}

	public String websiteUrl()
	{
		return this.websiteUrl;
	}
}
