/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kernel;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.EmailScope;
import org.kinsources.kiwa.accounts.AccountManager;
import org.kinsources.kiwa.accounts.Accounts;
import org.kinsources.kiwa.accounts.Role;
import org.kinsources.kiwa.accounts.Roles;
import org.kinsources.kiwa.accounts.XMLAccountManager;
import org.kinsources.kiwa.actalog.Actalog;
import org.kinsources.kiwa.actalog.EventLog;
import org.kinsources.kiwa.actalog.EventLog.Category;
import org.kinsources.kiwa.actalog.XMLActalog;
import org.kinsources.kiwa.actilog.Actilog;
import org.kinsources.kiwa.actilog.Log;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.actilog.Logs;
import org.kinsources.kiwa.actilog.XMLActilog;
import org.kinsources.kiwa.actulog.Actulog;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.actulog.XMLActulog;
import org.kinsources.kiwa.agora.Agora;
import org.kinsources.kiwa.agora.Forum;
import org.kinsources.kiwa.agora.Message;
import org.kinsources.kiwa.agora.Topic;
import org.kinsources.kiwa.agora.XMLAgora;
import org.kinsources.kiwa.eucles.Eucles;
import org.kinsources.kiwa.hico.Article;
import org.kinsources.kiwa.hico.Hico;
import org.kinsources.kiwa.hico.XMLHico;
import org.kinsources.kiwa.kernel.exceptions.KiwaBadFileFormatException;
import org.kinsources.kiwa.kernel.exceptions.KiwaControlException;
import org.kinsources.kiwa.kernel.exceptions.KiwaException;
import org.kinsources.kiwa.kidarep.Collaborator;
import org.kinsources.kiwa.kidarep.Collection;
import org.kinsources.kiwa.kidarep.Collections;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.kidarep.FormatIssue;
import org.kinsources.kiwa.kidarep.Kidarep;
import org.kinsources.kiwa.kidarep.LoadedFile;
import org.kinsources.kiwa.kidarep.LoadedFileHeader;
import org.kinsources.kiwa.kidarep.LoadedFileHeaders;
import org.kinsources.kiwa.kidarep.LoadedFiles;
import org.kinsources.kiwa.kidarep.Statistics;
import org.kinsources.kiwa.kidarep.XMLKidarep;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.sciboard.Requests;
import org.kinsources.kiwa.sciboard.Sciboard;
import org.kinsources.kiwa.sciboard.Vote;
import org.kinsources.kiwa.sciboard.XMLSciboard;
import org.kinsources.kiwa.stag.Stag;
import org.kinsources.kiwa.stag.StatisticTag;
import org.kinsources.kiwa.utils.Chronometer;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.graphs.GraphType;
import org.tip.puck.io.bar.BARTXTFile;
import org.tip.puck.io.bar.BARXLSFile;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.workers.RelationModelReporter;
import org.tip.puck.net.workers.AttributeFilters;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.ControlReporter.ControlType;
import org.tip.puck.report.Report;

import fr.devinsy.kiss4web.CookieHelper;
import fr.devinsy.kiss4web.CookieHelper.Scope;
import fr.devinsy.kiss4web.SimpleSecurityAgent;
import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.util.FileTools;
import fr.devinsy.util.StringList;
import fr.devinsy.util.rss.RSSCache;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.util.xml.XMLZipWriter;
import fr.devinsy.xidyn.views.CharterView;

/**
 * Class Kiwa uses singleton holder to be sure than Plaf modules are initialized.
 * 
 * 
 * @author Christian P. Momon
 */
public class Kiwa
{
	private static class SingletonHolder
	{
		private static final Kiwa instance = new Kiwa();
	}

	public enum Status
	{
		INIT_FAILED,
		OPENED,
		MAINTENANCE
	}

	public static int STATISTICS_COMPUTING_MAXDELAY = 20000; // 20s.

	public static int DEFAULT_RESET_PASSWORD_VALIDITY = 6 * 60; // Minutes.

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Kiwa.class);

	private BuildInformation buildInformation;
	private EnvironmentInformation environmentInformation;
	private KiwaCharterView charterView;
	private CharterView emailCharterView;
	private String webContentPath;
	private String[] languages;
	private static final String KIWA_LANGUAGE_COOKIE = "kiwa.language";
	private SimpleSecurityAgent securityAgent;
	private KiwaStorer storer;
	private Status status;
	private int resetPasswordValidity;

	private AccountManager accountManager;
	private Actalog actalog;
	private Actilog actilog;
	private Actulog actulog;
	private Agora agora;
	private Eucles eucles;
	private Hico hico;
	private Kidarep kidarep;
	private Sciboard sciboard;
	private Stag stag;

	/**
	 * @throws Exception
	 * 
	 */
	private Kiwa()
	{
		//
		String currentLog = "Kiwa";

		//
		try
		{
			//
			currentLog = "Kiwa";
			logger.info("Kiwa initializing...");
			long startTime = new Date().getTime();
			logKiwaInit(currentLog, "STARTING");

			//
			currentLog = "BuildInformation";
			this.buildInformation = new BuildInformation();
			logger.info("  build information=[" + this.buildInformation.toString() + "]");
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "EnvironmentInformation";
			this.environmentInformation = new EnvironmentInformation();
			logger.info("  name=[" + this.environmentInformation.name() + "]");
			logger.info("  websiteUrl=[" + this.environmentInformation.websiteUrl() + "]");
			logger.info("  log4jPath=[" + this.environmentInformation.log4jFilePathname() + "]");
			logger.info("  mail settings=[" + this.environmentInformation.mailSettings() + "]");
			logger.info("  JDBC settings=[" + this.environmentInformation.jdbcSettings() + "]");
			logger.info("  mailPauseTime=[" + this.environmentInformation.mailPauseTime() + "]");
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Languages";
			this.languages = new String[] { "en" }; // , "fr", "de" };
			Locale[] locales = { Locale.ENGLISH, Locale.FRENCH, new Locale("de") };
			Locale.setDefault(Locale.ENGLISH);
			logKiwaInit(currentLog, "PASSED");

			//
			this.resetPasswordValidity = DEFAULT_RESET_PASSWORD_VALIDITY;

			//
			currentLog = "Storer";
			this.storer = new KiwaStorer("jdbc/kiwadb");
			logKiwaInit(currentLog, "PASSED");

			/*
			 * PATCH ZONE 
			 */
			currentLog = "Patches";
			// this.storer.patch20151223();
			this.storer.patch20161028();
			this.storer.patch20161112();
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Actilog";
			this.actilog = this.storer.getActilog();
			if (this.actilog == null)
			{
				//
				this.actilog = new Actilog();

				//
				this.storer.store(this.actilog);
			}
			logKiwaInit(currentLog, "PASSED");

			//
			log(Level.INFO, "events.kiwa_start");

			//
			currentLog = "SecurityAgent";
			String kiwaRandomKey = this.storer.getKiwaRandomKey();
			if (kiwaRandomKey == null)
			{
				kiwaRandomKey = RandomStringUtils.random(128);
				this.storer.storeKiwaRandomKey(kiwaRandomKey);
			}
			this.securityAgent = new SimpleSecurityAgent("kiwa", kiwaRandomKey);
			this.securityAgent.setAuthenticationDuration(2 * 24 * 60 * 60);
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Status";
			this.status = this.storer.getKiwaStatus();
			if (this.status == null)
			{
				this.status = Status.OPENED;
				this.storer.storeKiwaStatus(this.status);
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "AccountManager";
			this.accountManager = this.storer.getAccountManager();
			if (this.accountManager == null)
			{
				//
				this.accountManager = new AccountManager();

				//
				KiwaRoles.VISITOR = this.accountManager.createRole("Visitor");
				KiwaRoles.WEBMASTER = this.accountManager.createRole("Webmaster");
				KiwaRoles.CONTRIBUTOR = this.accountManager.createRole("Contributor");
				KiwaRoles.SCIBOARDER = this.accountManager.createRole("Sci-board member");
				KiwaRoles.SCIBOARD_CHIEF = this.accountManager.createRole("Sci-board chief");
				KiwaRoles.MODERATOR = this.accountManager.createRole("Moderator");
				KiwaRoles.REDACTOR = this.accountManager.createRole("Redactor");
				KiwaRoles.FORMAT_INSPECTOR = this.accountManager.createRole("Format Inspector");

				//
				this.storer.store(this.accountManager);
			}
			else
			{
				KiwaRoles.VISITOR = this.accountManager.roles().getByName("Visitor");
				KiwaRoles.WEBMASTER = this.accountManager.roles().getByName("Webmaster");
				KiwaRoles.CONTRIBUTOR = this.accountManager.roles().getByName("Contributor");
				KiwaRoles.SCIBOARDER = this.accountManager.roles().getByName("Sci-board member");
				KiwaRoles.SCIBOARD_CHIEF = this.accountManager.roles().getByName("Sci-board chief");
				KiwaRoles.MODERATOR = this.accountManager.roles().getByName("Moderator");
				KiwaRoles.REDACTOR = this.accountManager.roles().getByName("Redactor");
				KiwaRoles.FORMAT_INSPECTOR = this.accountManager.roles().getByName("Format Inspector");
				if (KiwaRoles.FORMAT_INSPECTOR == null)
				{
					KiwaRoles.FORMAT_INSPECTOR = this.accountManager.createRole("Format Inspector");
				}
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Actalog";
			this.actalog = this.storer.getActalog();
			if (this.actalog == null)
			{
				this.actalog = new Actalog();
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Actulog";
			this.actulog = this.storer.getActulog();
			if (this.actulog == null)
			{
				this.actulog = new Actulog();
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Agora";
			this.agora = this.storer.getAgora();
			if (this.agora == null)
			{
				this.agora = new Agora();
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Hico";
			this.hico = this.storer.getHico();
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Kidarep";
			this.kidarep = this.storer.getKidarep(this.accountManager.accounts());
			if (this.kidarep == null)
			{
				this.kidarep = new Kidarep();
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Sciboard";
			this.sciboard = this.storer.getSciboard();
			if (this.sciboard == null)
			{
				this.sciboard = new Sciboard();
			}
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Stag";
			this.stag = new Stag(this.accountManager, this.kidarep);
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Eucles";
			this.eucles = new Eucles(this.environmentInformation.mailPauseTime());
			logKiwaInit(currentLog, "PASSED");

			//
			currentLog = "Kiwa";
			logKiwaInit(currentLog, "STARTED");
			long endTime = new Date().getTime();
			logger.info("Kiwa initialized in {}ms.", endTime - startTime);
		}
		catch (Exception exception)
		{
			logKiwaInit(currentLog, "FAILED");
			logger.warn("KIWA INIT FAILED: " + exception.getMessage());
			logger.warn(ExceptionUtils.getStackTrace(exception));
			this.status = Kiwa.Status.INIT_FAILED;
		}
	}

	/**
	 * 
	 * @return
	 */
	public AccountManager accountManager()
	{
		return this.accountManager;
	}

	/**
	 * 
	 * @return
	 */
	public Actalog actalog()
	{
		return this.actalog;
	}

	/**
	 * 
	 * @return
	 */
	public Actilog actilog()
	{
		return this.actilog;
	}

	/**
	 * 
	 * @param fullName
	 * @param email
	 * @param password
	 * @return
	 */
	public void activateAccount(final Account account)
	{
		//
		account.setStatus(Account.Status.ACTIVATED);
		account.setRole(KiwaRoles.VISITOR);
		account.setRole(KiwaRoles.CONTRIBUTOR);
		account.touch();

		//
		this.storer.store(account);

		//
		this.stag.updateActivatedAccountCount();
	}

	/**
	 * 
	 * @param id
	 */
	public void activateAccount(final long accountId)
	{
		Account target = this.accountManager.getAccountById(accountId);

		if (target == null)
		{
			throw new IllegalArgumentException("accountId is null.");
		}
		else
		{
			activateAccount(target);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Actulog actulog()
	{
		return this.actulog;
	}

	/**
	 * 
	 * @param dataset
	 * @param name
	 * @param data
	 * @return
	 */
	public LoadedFileHeader addAttachment(final Dataset dataset, final String name, final byte[] data)
	{
		LoadedFileHeader result;

		result = this.kidarep.addAttachment(dataset, name, data);

		this.storer.storeKidarepFile(result.id(), data);
		this.storer.store(dataset);

		//
		return result;
	}

	/**
	 * 
	 * @param request
	 * @param ownerId
	 * @param comment
	 */
	public void addComment(final Request request, final long ownerId, final String comment)
	{
		//
		this.sciboard.addComment(request, ownerId, comment);

		//
		this.storer.store(request);
	}

	/**
	 * This method add a comment in the current dataset sciboard request if on is alive from user action. Useful to
	 * notify the sciboard that contributor edited metadata or attachment files.
	 * 
	 * @param dataset
	 * @param ownerId
	 * @param comment
	 */
	public void addCommentFromAutomaticNotification(final Dataset dataset, final long ownerId, final String comment)
	{
		// Add notification in request comments.
		if (dataset.getStatus() == Dataset.Status.SUBMITTED)
		{
			Requests queries = this.sciboard.findRequestsByDatasetId(String.valueOf(dataset.getId())).findByStatus(Request.Status.OPENED);

			if (!queries.isEmpty())
			{
				Request query = queries.getByIndex(0);
				addComment(query, ownerId, "**Automatic notification**\n" + comment);
			}
		}
	}

	/**
	 * 
	 * @param dataset
	 * @param originFile
	 * @param controls
	 * @param locale
	 * @throws Exception
	 */
	public void addDatasetGenealogyFile(final Dataset dataset, final LoadedFile originFile, final List<ControlType> controls, final Locale locale) throws Exception
	{
		logger.debug("originFile.data size=" + originFile.data().length);

		//
		removeDatasetOriginFile(dataset);

		//
		this.kidarep.addDatasetGenealogyFile(dataset, originFile, controls, locale);
		this.storer.storeKidarepFile(dataset.getOriginFile().id(), originFile.data());
		this.storer.store(dataset);

		//
		rebuildGenealogyStatisticsByThread(dataset, dataset.getOriginFile());
	}

	/**
	 * 
	 * @param dataset
	 * @param originFile
	 * @param controls
	 * @param locale
	 * @throws Exception
	 */
	public void addDatasetTerminologyFile(final Dataset dataset, final LoadedFile originFile, final List<RelationModelReporter.ControlType> controls, final Locale locale) throws Exception
	{
		logger.debug("originFile.data size=" + originFile.data().length);

		//
		removeDatasetOriginFile(dataset);

		//
		this.kidarep.addDatasetTerminologyFile(dataset, originFile, controls, locale);
		this.storer.storeKidarepFile(dataset.getOriginFile().id(), originFile.data());
		this.storer.store(dataset);

		//
		rebuildTerminologyStatistics(dataset, dataset.getOriginFile());
	}

	/**
	 * 
	 * @return
	 */
	public Agora agora()
	{
		return this.agora;
	}

	public BuildInformation buildInformation()
	{
		return this.buildInformation;
	}

	/**
	 * 
	 * @return
	 */
	public String buildRequestDescription(final Request source)
	{
		String result;

		if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			switch (source.getType())
			{
				case COOPTATION:
					result = "";
				break;

				case DATASET_PUBLISH:
				{
					String datasetId = source.properties().getProperty("dataset_id");
					logger.debug("datasetId=[{}]", datasetId);
					Dataset dataset = this.kidarep.getDatasetById(datasetId);
					if (dataset == null)
					{
						result = "Deprecated id";
					}
					else
					{
						String icon;
						if (dataset.isGenealogy())
						{
							icon = "<img src=\"/charter/commons/dataset-genealogy.png\" alt=\"genealogy\" style=\"vertical-align: text-bottom;\" />";
						}
						else
						{
							icon = "<img src=\"/charter/commons/dataset-terminology.png\" alt=\"terminology\" style=\"vertical-align: text-bottom;\" />";
						}

						result = String.format("%s (ID %s %s)", XMLTools.escapeXmlBlank(dataset.getName()), datasetId, icon);
					}
				}
				break;

				case DATASET_UNPUBLISH:
				{
					String datasetId = source.properties().getProperty("dataset_id");
					Dataset dataset = this.kidarep.getDatasetById(datasetId);
					if (dataset == null)
					{
						result = "Deprecated id";
					}
					else
					{
						result = String.format("%s (ID %s)", XMLTools.escapeXmlBlank(dataset.getName()), datasetId);
					}
				}
				break;

				case ISSUE:
					result = source.properties().getProperty("title");
				break;

				case UNCOOPTATION:
					result = "";
				break;

				default:
					result = "";
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void cancelFormatIssue(final FormatIssue source) throws Exception
	{
		if (source == null)
		{
			throw new IllegalArgumentException("formatIssue is null.");
		}
		else if (source.getStatus() != FormatIssue.Status.WAITING)
		{
			throw new IllegalArgumentException("Bad status.");
		}
		else
		{
			this.kidarep.removeFormatIssue(source);

			this.storer.remove(source);
		}
	}

	/**
	 * 
	 * @param request
	 * @param comment
	 * @throws Exception
	 */
	public synchronized void cancelRequest(final Request request, final String comment) throws Exception
	{
		//
		LoadedFileHeaders previousFiles = new LoadedFileHeaders();
		if (request.getType() == Request.Type.DATASET_PUBLISH)
		{
			String datasetId = request.properties().getProperty("dataset_id");
			Dataset dataset = this.kidarep.getDatasetById(datasetId);
			if (dataset.getPublicFile() != null)
			{
				previousFiles.add(dataset.getPublicFile());
				previousFiles.addAll(dataset.getPublicFile().statistics().graphics());
			}
		}

		//
		this.sciboard.cancelRequest(request, comment);
		//
		this.storer.store(request);

		//
		if (request.getType() == Request.Type.DATASET_PUBLISH)
		{
			for (LoadedFileHeader file : previousFiles)
			{
				this.storer.removeKidarepFile(file.id());
			}
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public LoadedFileHeaders censusLoadedFileHeaders() throws Exception
	{
		LoadedFileHeaders result;

		result = new LoadedFileHeaders();

		//
		for (Dataset dataset : this.kidarep.datasets())
		{
			//
			for (LoadedFileHeader attachment : dataset.attachments())
			{
				result.add(attachment);
			}

			//
			if (dataset.getOriginFile() != null)
			{
				result.add(dataset.getOriginFile());
				for (LoadedFileHeader graphic : dataset.getOriginFile().statistics().graphics())
				{
					result.add(graphic);
				}
			}

			//
			if (dataset.getPublicFile() != null)
			{
				result.add(dataset.getPublicFile());
				for (LoadedFileHeader graphic : dataset.getPublicFile().statistics().graphics())
				{
					result.add(graphic);
				}
			}

			//
			if (dataset.collaborators() != null)
			{
				for (Collaborator collaborator : dataset.collaborators())
				{
					result.add(collaborator.getFile());
					for (LoadedFileHeader graphic : collaborator.getFile().statistics().graphics())
					{
						result.add(graphic);
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @throws Exception
	 */
	public void clearStatistics(final Dataset source) throws Exception
	{
		LoadedFileHeaders previousFiles = new LoadedFileHeaders();

		//
		if (source.getOriginFile() != null)
		{
			previousFiles.addAll(source.getOriginFile().statistics().graphics());
			source.getOriginFile().statistics().clear();
		}
		if (source.getPublicFile() != null)
		{
			previousFiles.addAll(source.getPublicFile().statistics().graphics());
			source.getPublicFile().statistics().clear();
		}
		for (Collaborator collaborator : source.collaborators())
		{
			previousFiles.addAll(collaborator.getFile().statistics().graphics());
			collaborator.getFile().statistics().clear();
		}

		//
		this.storer.store(source);
		for (LoadedFileHeader file : previousFiles)
		{
			this.storer.removeKidarepFile(file.id());
		}
	}

	/**
	 * 
	 * @param dataset
	 * @throws Exception
	 */
	public void clearStatistics(final Dataset dataset, final Statistics target) throws Exception
	{
		LoadedFileHeaders previousFiles = new LoadedFileHeaders();

		previousFiles.addAll(target.graphics());
		target.clear();

		for (LoadedFileHeader file : previousFiles)
		{
			this.storer.removeKidarepFile(file.id());
		}
	}

	/**
	 * @throws KiwaException
	 * @throws Exception
	 * 
	 */
	public void clearStatistics(final Datasets source) throws KiwaException
	{
		//
		logger.debug("Starting datasets statistics clear...");

		//
		StringList errors = new StringList();
		long startTime = new Date().getTime();
		int datasetIndex = 0;
		for (Dataset dataset : source)
		{
			datasetIndex += 1;

			try
			{
				logger.info("==== {}/{} {}", datasetIndex, source.size(), dataset.getName());
				clearStatistics(dataset);
			}
			catch (Exception exception)
			{
				logger.error("rebuild statistics failed for datasetId [" + dataset.getId() + "]", exception);
				errors.append(dataset.getId());
			}
		}

		//
		long endTime = new Date().getTime();
		logger.debug("Dataset statistics rebuild in {}ms.", endTime - startTime);

		if (!errors.isEmpty())
		{
			errors.set(0, "rebuild statistics gone some errors: ");
			throw new KiwaException("rebuild statistics gone some errors: " + errors.toStringWithCommas());
		}
	}

	/**
	 * 
	 * @throws KiwaException
	 */
	public void clearStatisticsAll() throws KiwaException
	{
		clearStatistics(this.kidarep.datasetsFromIndex());
	}

	/**
	 * 
	 * @throws KiwaException
	 */
	public void clearStatisticsGenealogies() throws KiwaException
	{
		clearStatistics(this.kidarep.datasetsFromIndex().findGenealogies());
	}

	/**
	 * 
	 * @throws KiwaException
	 */
	public void clearStatisticsTerminologies() throws KiwaException
	{
		clearStatistics(this.kidarep.datasetsFromIndex().findTerminologies());
	}

	/**
	 * 
	 * @param request
	 * @param ownerId
	 * @throws Exception
	 */
	public void closeRequest(final Request request, final long decisionMakerId, final Request.Status decision, final String comment) throws Exception
	{
		if (decision == Request.Status.ACCEPTED)
		{
			//
			request.setAccepted(decisionMakerId, comment);

			//
			if (request.getType() == Request.Type.DATASET_PUBLISH)
			{
				//
				String datasetId = request.properties().getProperty("dataset_id");
				Dataset dataset = Kiwa.instance().kidarep().getDatasetById(datasetId);

				//
				publishDataset(dataset);

				//
				logEvent(Category.PUBLICATION, dataset.getContributor().getFullName(), dataset.getName());
			}
			else if (request.getType() == Request.Type.DATASET_UNPUBLISH)
			{
				//
				String datasetId = request.properties().getProperty("dataset_id");
				Dataset dataset = Kiwa.instance().kidarep().getDatasetById(datasetId);

				//
				unpublishDataset(dataset);
			}

			//
			this.storer.store(request);
		}
		else if ((decision == Request.Status.DECLINED) || ((decision == Request.Status.ACCEPTED_WITH_REVISIONS)))
		{
			request.setDecision(decision, decisionMakerId, comment);

			if (request.getType() == Request.Type.DATASET_PUBLISH)
			{
				String datasetId = request.properties().getProperty("dataset_id");
				Dataset dataset = Kiwa.instance().kidarep().getDatasetById(datasetId);

				if (dataset.getPublicFile() != null)
				{
					this.storer.removeKidarepFile(dataset.getPublicFile().id());
					dataset.setPublicFile(null);
				}

				dataset.setStatus(Dataset.Status.CREATED);

				this.storer.store(dataset);
			}

			this.storer.store(request);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void confirmFormatIssue(final FormatIssue source) throws Exception
	{
		if (source == null)
		{
			throw new IllegalArgumentException("formatIssue is null.");
		}
		else if (source.getStatus() != FormatIssue.Status.WAITING)
		{
			throw new IllegalArgumentException("Bad status.");
		}
		else
		{
			source.setStatus(FormatIssue.Status.READY);

			this.storer.store(source);
		}
	}

	/**
	 * 
	 * @param email
	 * @param fullName
	 * @param password
	 * @param emailScope
	 * @param organization
	 * @param businessCard
	 * @param country
	 * @param timeZone
	 * @param emailNotification
	 */
	public Account createAccount(final String email, final String firstName, final String lastName, final String password, final EmailScope emailScope, final String organization,
			final String businessCard, final String country, final String website, final DateTimeZone timeZone, final boolean emailNotification)
	{
		Account result;

		result = this.accountManager.getAccountByEmail(email);
		if ((result != null) && (result.getStatus() != Account.Status.CREATED))
		{
			throw new IllegalArgumentException("This email is already in use.");
		}
		else
		{
			//
			if (result != null)
			{
				// Recycle a ghost account.
				result.setFullName(firstName, lastName);
				result.setPassword(password);
			}
			else
			{
				// Create account.
				result = this.accountManager.createAccount(firstName, lastName, email, password);

				result.setRole(KiwaRoles.VISITOR);
			}

			// If the account is the first one, give it webmaster rights.
			if (this.accountManager.accounts().size() == 1)
			{
				logger.info("Set first account as webmaster");
				result.setStatus(Account.Status.ACTIVATED);
				result.setRole(KiwaRoles.WEBMASTER);
			}

			//
			result.setEmailScope(emailScope);
			result.setOrganization(organization);
			result.setBusinessCard(businessCard);
			result.setWebsite(website);
			result.setCountry(country);
			result.setTimeZone(timeZone);
			result.setEmailNotification(emailNotification);

			//
			this.storer.store(result);

			// Update contributor list.
			this.kidarep.getContributorById(result.getId());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @param author
	 * @param visible
	 * @param locale
	 * @param text
	 * @return
	 */
	public Article createArticle(final String name, final String author, final boolean visible, final Locale locale, final String text)
	{
		Article result;

		//
		result = this.hico.createArticle(name, locale);

		//
		result.setAuthor(author);
		result.setVisible(visible);
		result.setText(text);

		//
		this.storer.store(result);

		//
		if (name.equals(Hico.BUBBLE_ARTICLE_NAME))
		{
			this.hico.resetBubbles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param collaboratorAccount
	 * @param description
	 * @param filters
	 * @return
	 * @throws Exception
	 */
	public void createCollaborator(final Dataset dataset, final Account collaboratorAccount, final String description, final AttributeFilters filters) throws Exception
	{
		if (dataset.getOriginFile() != null)
		{
			//
			LoadedFile originFile = getLoadedFile(dataset.getOriginFile());

			//
			LoadedFile collaboratorFile = this.kidarep.createCollaborator(dataset, collaboratorAccount, description, filters, originFile);

			//
			this.storer.storeKidarepFile(collaboratorFile.header().id(), collaboratorFile.data());
			this.storer.store(dataset);

			//
			rebuildStatistics(dataset, dataset.collaborators().getByAccountId(collaboratorAccount.getId()).getFile());
		}
	}

	/**
	 * 
	 * @param contributor
	 * @param name
	 * @param description
	 * @param scope
	 * @return
	 */
	public Collection createCollection(final Contributor contributor, final String name, final String description, final Collection.Scope scope)
	{
		Collection result;

		//
		result = this.kidarep.createCollection(contributor, name, description, scope);

		//
		this.storer.store(result);

		//
		return result;
	}

	/**
	 * 
	 * @param contributor
	 * @param name
	 * @return
	 */
	public Dataset createDataset(final Contributor contributor, final String name)
	{
		Dataset result;

		//
		result = this.kidarep.createDataset(contributor, name);

		//
		this.storer.store(result);

		//
		return result;
	}

	/**
	 * 
	 * @param contributorId
	 * @param datasetId
	 * @param file
	 * @return
	 */
	public FormatIssue createFormatIssue(final long contributorId, final long datasetId, final LoadedFile file)
	{
		FormatIssue result;

		//
		result = this.kidarep.createFormatIssue(contributorId, datasetId, file);

		//
		this.storer.store(result);

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	public Forum createForum(final String title, final String subtitle)
	{
		Forum result;

		//
		result = this.agora.createForum(title, subtitle);

		//
		this.storer.store(this.agora.forums());

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subTitle
	 * @return
	 */
	public Message createMessage(final Topic parent, final Account author, final String text)
	{
		Message result;

		//
		result = this.agora.createMessage(parent, author.getId(), author.getFullName(), text);

		//
		this.storer.store(result);

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	public synchronized Topic createTopic(final Forum forum, final String title, final String text, final boolean sticky, final Topic.Status status, final Account author)
	{
		Topic result;

		//
		result = this.agora.createTopic(forum, title);
		result.setSticky(sticky);
		result.setStatus(status);

		//
		Message message = this.agora.createMessage(result, author.getId(), author.getFullName(), text);

		//
		this.storer.store(result);
		this.storer.store(message);

		//
		return result;
	}

	/**
	 * 
	 * @param email
	 * @param fullName
	 * @param password
	 * @param emailScope
	 * @param organization
	 * @param businessCard
	 * @param country
	 * @param timeZone
	 * @param emailNotification
	 */
	public Wire createWire(final String title, final String author, final DateTime publicationDate, final Locale locale, final String leadParagraph, final String body)
	{
		Wire result;

		//
		result = this.actulog.createWire(title, locale);

		//
		result.setAuthor(author);
		result.setPublicationDate(publicationDate);
		result.setLeadParagraph(leadParagraph);
		result.setBody(body);

		//
		this.storer.store(result);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void downForum(final Forum forum) throws Exception
	{
		//
		this.agora.downForum(forum);

		//
		this.storer.store(this.agora.forums());
	}

	/**
	 * 
	 * @return
	 */
	public EnvironmentInformation environmentInformation()
	{
		return this.environmentInformation;
	}

	/**
	 * 
	 * @return
	 */
	public Eucles eucles()
	{
		return this.eucles;
	}

	/**
	 * 
	 * @param out
	 * @param criteria
	 * @throws IOException
	 */
	public void export(final OutputStream out, final KiwaExportCriteria criteria) throws IOException
	{
		//
		XMLZipWriter target = null;
		try
		{
			//
			target = new XMLZipWriter(out, "Generated by Kiwa.");

			//
			if (criteria.isAccounts())
			{
				target.openEntry("accounts.xml");
				target.writeXMLHeader((String[]) null);
				XMLAccountManager.write(target, this.accountManager);
				target.closeEntry();
			}

			//
			if (criteria.isActalog())
			{
				target.openEntry("actalog.xml");
				target.writeXMLHeader((String[]) null);
				XMLActalog.write(target, this.actalog);
				target.closeEntry();
			}

			//
			if (criteria.isActilog())
			{
				target.openEntry("actilog.xml");
				target.writeXMLHeader((String[]) null);
				XMLActilog.write(target, this.actilog);
				target.closeEntry();
			}

			//
			if (criteria.isActulog())
			{
				target.openEntry("actalug.xml");
				target.writeXMLHeader((String[]) null);
				XMLActulog.write(target, this.actulog);
				target.closeEntry();
			}

			//
			if (criteria.isAgora())
			{
				target.openEntry("agora.xml");
				target.writeXMLHeader((String[]) null);
				XMLAgora.write(target, this.agora);
				target.closeEntry();
			}

			//
			if (criteria.isHico())
			{
				target.openEntry("hico.xml");
				target.writeXMLHeader((String[]) null);
				XMLHico.write(target, this.hico);
				target.closeEntry();
			}

			//
			if (criteria.isKidarep())
			{
				target.openEntry("kidarep.xml");
				target.writeXMLHeader((String[]) null);
				XMLKidarep.write(target, this.kidarep);
				target.closeEntry();
			}

			//
			if (criteria.isSciboard())
			{
				target.openEntry("sciboard.xml");
				target.writeXMLHeader((String[]) null);
				XMLSciboard.write(target, this.sciboard);
				target.closeEntry();
			}

		}
		finally
		{
			if (target != null)
			{
				target.close();
			}
		}
	}

	/**
	 * 
	 * 
	 * @param source
	 * @return
	 */
	public Dataset.Type extractDatasetTypeFromRequest(final Request source)
	{
		Dataset.Type result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			switch (source.getType())
			{
				case DATASET_PUBLISH:
				{
					String datasetId = source.properties().getProperty("dataset_id");
					logger.debug("datasetId=[{}]", datasetId);
					Dataset dataset = this.kidarep.getDatasetById(datasetId);
					if (dataset == null)
					{
						result = null;
					}
					else
					{
						if (dataset.isGenealogy())
						{
							result = Dataset.Type.GENEALOGY;
						}
						else
						{
							result = Dataset.Type.TERMINOLOGY;
						}
					}
				}
				break;

				case DATASET_UNPUBLISH:
				{
					String datasetId = source.properties().getProperty("dataset_id");
					Dataset dataset = this.kidarep.getDatasetById(datasetId);
					if (dataset == null)
					{
						result = null;
					}
					else
					{
						if (dataset.isGenealogy())
						{
							result = Dataset.Type.GENEALOGY;
						}
						else
						{
							result = Dataset.Type.TERMINOLOGY;
						}
					}
				}
				break;

				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public StringList findOrphanSubkeys() throws Exception
	{
		StringList result;

		result = new StringList();

		//
		StringList subkeys = this.storer.database().getAllSubkeys("kidarep.files");

		//
		LoadedFileHeaders headers = censusLoadedFileHeaders();

		//
		for (String subkeyValue : subkeys)
		{
			long subkey = Long.parseLong(subkeyValue);

			if (headers.getById(subkey) == null)
			{
				result.add(subkeyValue);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Account getAuthentifiedAccount(final HttpServletRequest request, final HttpServletResponse response)
	{
		Account result;

		if (this.securityAgent.isAuthenticated(request, response))
		{
			String idParameter = securityAgent().accountId(request);
			if ((StringUtils.isBlank(idParameter)) || (!NumberUtils.isNumber(idParameter)))
			{
				result = null;
			}
			else
			{
				result = this.accountManager.getAccountById(Long.parseLong(idParameter));

				if (result.getStatus() != Account.Status.ACTIVATED)
				{
					result = null;
				}
			}
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Long getAuthentifiedAccountId(final HttpServletRequest request, final HttpServletResponse response)
	{
		Long result;

		if (this.securityAgent.isAuthenticated(request, response))
		{
			String idParameter = this.securityAgent.accountId(request);
			if ((StringUtils.isBlank(idParameter)) || (!NumberUtils.isNumber(idParameter)))
			{
				result = null;
			}
			else
			{
				result = Long.parseLong(idParameter);
			}
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public KiwaCharterView getCharterView()
	{
		return this.charterView;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @return
	 */
	public DatasetFile getDatasetFile(final Dataset dataset, final Account account)
	{
		DatasetFile result;

		if (dataset == null)
		{
			result = null;
		}
		else if (account == null)
		{
			result = dataset.getPublicFile();
		}
		else
		{
			if (dataset.getOriginFile() == null)
			{
				result = null;
			}
			else if (dataset.isContributor(account))
			{
				result = dataset.getOriginFile();
			}
			else
			{
				Collaborator collaborator = dataset.collaborators().getByAccountId(account.getId());
				if (collaborator == null)
				{
					result = dataset.getPublicFile();
				}
				else
				{
					result = collaborator.getFile();
				}
			}
		}

		if (result == null)
		{
			logger.debug("getDatasetFile => [null]");
		}
		else
		{
			logger.debug("getDatasetFile => [{}][{}]", result.id(), result.name());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public CharterView getEmailCharterView()
	{
		return this.emailCharterView;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public byte[] getKidarepFile(final long id)
	{
		byte[] result;

		result = this.storer.getKidarepFile(id);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public LoadedFile getLoadedFile(final LoadedFileHeader source)
	{
		LoadedFile result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			byte[] data = this.storer.getKidarepFile(source.id());

			result = new LoadedFile(source, data);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public LoadedFile getLoadedFileDetached(final LoadedFileHeader source)
	{
		LoadedFile result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			byte[] data = this.storer.getKidarepFile(source.id());

			result = new LoadedFile(new LoadedFileHeader(source), data);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getResetPasswordValidity()
	{
		return this.resetPasswordValidity;
	}

	/**
	 * 
	 * @return
	 */
	public Kiwa.Status getStatus()
	{
		return this.status;
	}

	/**
	 *
	 */
	public String getUserLanguage(final HttpServletRequest request)
	{
		String result;

		//
		result = (String) CookieHelper.getCookieValue(request, KIWA_LANGUAGE_COOKIE);

		//
		if (result == null)
		{
			Locale locale = request.getLocale();
			if (locale != null)
			{
				String candidate = locale.getLanguage();
				if (ArrayUtils.contains(this.languages, candidate))
				{
					result = candidate;
				}
			}
		}

		//
		return result;
	}

	/**
	 *
	 */
	public Locale getUserLocale(final HttpServletRequest request)
	{
		Locale result;

		//
		String language = (String) CookieHelper.getCookieValue(request, KIWA_LANGUAGE_COOKIE);

		//
		if (language == null)
		{
			result = Locale.ENGLISH; // TODO request.getLocale();
		}
		else
		{
			result = new Locale(language);
		}

		//
		if (result == null)
		{
			result = Locale.ENGLISH;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getWebContentPath()
	{
		return this.webContentPath;
	}

	/**
	 * 
	 * @return
	 */
	public String getWebsiteUrl()
	{
		return this.environmentInformation.websiteUrl();
	}

	/**
	 * 
	 * @return
	 */
	public Hico hico()
	{
		return this.hico;
	}

	/**
	 * 
	 * @return
	 */
	public String howToCite(final Dataset source)
	{
		String result;

		StringList howToCite = new StringList();
		if (StringUtils.isBlank(source.getAuthor()))
		{
			howToCite.append("Unknown author");
		}
		else
		{
			howToCite.append(source.getAuthor());
		}
		if (source.getPublicationDate() == null)
		{
			howToCite.append(" (----), ");
		}
		else
		{
			howToCite.append(" (").append(source.getPublicationDate().toString("YYYY")).append("), ");
		}
		howToCite.append(source.getName()).append(" dataset, ");
		howToCite.appendln(permanentURI(source));

		result = howToCite.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param out
	 * @param criteria
	 * @throws IOException
	 */
	public void importArticles(final byte[] source) throws Exception
	{

		Hico hicoData = XMLHico.load(source);

		logger.debug("articles count=" + hicoData.articles().size());

		//
		for (Article article : hicoData.articles())
		{
			Article current = this.hico.getArticleById(article.getId());

			if (current == null)
			{
				this.hico.articles().add(article);
			}
			else
			{
				removeArticle(article.getId());
				this.hico.articles().add(article);
			}
			this.storer.store(article);
		}

		//
		this.hico.resetLastId();
		this.hico.rebuildIndexes();
		this.hico.resetBubbles();
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isModerator(final Account account)
	{
		boolean result;

		if ((account == null) || (account.isNotRole(KiwaRoles.MODERATOR)))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isRedactor(final Account account)
	{
		boolean result;

		if ((account == null) || (account.isNotRole(KiwaRoles.REDACTOR)))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isSciboarder(final Account account)
	{
		boolean result;

		if ((account == null) || (account.isNotRole(KiwaRoles.SCIBOARDER)))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isScichief(final Account account)
	{
		boolean result;

		if ((account == null) || (account.isNotRole(KiwaRoles.SCIBOARD_CHIEF)))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param account
	 * @return
	 */
	public boolean isWebmaster(final Account account)
	{
		boolean result;

		if ((account == null) || (account.isNotRole(KiwaRoles.WEBMASTER)))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Kidarep kidarep()
	{
		return this.kidarep;
	}

	/**
	 * 
	 */
	public String[] languages()
	{
		return this.languages;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	public Log log(final Level level, final String event)
	{
		Log result;

		result = log(level, event, (String) null);

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	public Log log(final Level level, final String event, final String detail)
	{
		Log result;

		//
		result = this.actilog.log(level, event, detail);

		if (result != null)
		{
			this.storer.store(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	public Log log(final Level level, final String event, final String detailFormat, final Object... details)
	{
		Log result;

		//
		result = this.actilog.log(level, event, detailFormat, details);

		if (result != null)
		{
			this.storer.store(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param category
	 * @param comment
	 */
	public EventLog logEvent(final Category category, final String... arguments)
	{
		EventLog result;

		result = this.actalog.logEvent(category, arguments);

		if (result != null)
		{
			this.storer.store(result);
		}

		RSSCache.instance().setOudated(Actalog.RSS_NAME);

		//
		return result;
	}

	/**
	 * 
	 * @param currentLogItem
	 * @param status
	 */
	protected void logKiwaInit(final String currentLogItem, final String status)
	{
		logger.info(String.format("%s%s%s", currentLogItem, StringUtils.repeat('.', 40 - StringUtils.length(currentLogItem)), status));
	}

	/**
	 * 
	 * @param page
	 * @param request
	 */
	public void logPageHit(final String page, final HttpServletRequest request)
	{

		String accountId = this.securityAgent.accountId(request);
		String ip = request.getRemoteAddr();
		String userAgent = request.getHeader("User-Agent");
		String referer = request.getHeader("referer");
		String language = getUserLanguage(request);

		//
		log(Level.INFO, page, "[accountId={}][language={}][ip={}][userAgent={}][referer={}]", accountId, language, ip, userAgent, referer);
	}

	/**
	 * 
	 * @param subject
	 * @param html
	 * @throws Exception
	 */
	public void notifyFormatInspectors(final String subject, final CharSequence content, final Locale locale) throws Exception
	{

		//
		Accounts accounts = this.accountManager.findAccountsByRole(KiwaRoles.FORMAT_INSPECTOR);

		//
		String newSubject = "[Kinsources] " + subject;

		// Build the mail content.
		StringList html = new StringList();
		html.appendln("<div style=\"margin-left:100px;\">");
		html.appendln("<h1>Format Inspector Notification</h1>");
		html.appendln(content.toString());
		html.appendln("<small>You're receiving this notification because you are a Kinsources Format Inspector with email notification activated.</small><br/>");
		html.appendln("<br/>");
		html.appendln("</div>");

		StringBuffer newContent = getEmailCharterView().getHtml(null, locale, html.toString());

		//
		for (Account account : accounts)
		{
			if (account.isEmailNotification())
			{
				this.eucles.sendMail(account.getEmail(), "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, newSubject, newContent);
			}
		}
	}

	/**
	 * 
	 * @param subject
	 * @param html
	 * @throws Exception
	 */
	public void notifyMembers(final Accounts accounts, final String subject, final CharSequence content, final Locale locale) throws Exception
	{
		//
		String newSubject = "[Kinsources] " + subject;

		// Build the mail content.
		StringList html = new StringList();
		html.appendln("<div style=\"margin-left:100px;\">");
		html.appendln("<h1>Moderator Notification</h1>");
		html.appendln(content.toString());
		html.appendln("<small>You're receiving this notification because you are a Kinsources member with email notification activated.</small><br/>");
		html.appendln("<br/>");
		html.appendln("</div>");

		StringBuffer newContent = getEmailCharterView().getHtml(null, locale, html.toString());

		//
		for (Account account : accounts)
		{
			if (account.isEmailNotification())
			{
				this.eucles.sendMail(account.getEmail(), "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, newSubject, newContent);
			}
		}
	}

	/**
	 * 
	 * @param subject
	 * @param html
	 * @throws Exception
	 */
	public void notifyModerators(final Account author, final String subject, final CharSequence content, final Locale locale) throws Exception
	{

		//
		Accounts accounts = this.accountManager.findAccountsByRole(KiwaRoles.MODERATOR);
		accounts.remove(author);

		//
		String newSubject = "[Kinsources] " + subject;

		// Build the mail content.
		StringList html = new StringList();
		html.appendln("<div style=\"margin-left:100px;\">");
		html.appendln("<h1>Moderator Notification</h1>");
		html.appendln(content.toString());
		html.appendln("<small>You're receiving this notification because you are a Kinsources forum moderator with email notification activated.</small><br/>");
		html.appendln("<br/>");
		html.appendln("</div>");

		StringBuffer newContent = getEmailCharterView().getHtml(null, locale, html.toString());

		//
		for (Account account : accounts)
		{
			if (account.isEmailNotification())
			{
				this.eucles.sendMail(account.getEmail(), "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, newSubject, newContent);
			}
		}
	}

	/**
	 * 
	 * @param subject
	 * @param html
	 * @throws Exception
	 */
	public void notifySciboarderChiefs(final Account author, final String subject, final CharSequence content, final Locale locale) throws Exception
	{
		//
		Accounts accounts = this.accountManager.findAccountsByRole(KiwaRoles.SCIBOARD_CHIEF);
		accounts.remove(author);

		//
		String newSubject = "[Kinsources] " + subject;

		// Build the mail content.
		StringList html = new StringList();
		html.appendln("<div style=\"margin-left:100px;\">");
		html.appendln("<h1>Sciboarder Notification</h1>");
		html.appendln(content.toString());
		html.appendln("<small>You're receiving this notification because you are a Kinsources sciboard chief.</small><br/>");
		html.appendln("<br/>");
		html.appendln("</div>");

		StringBuffer newContent = getEmailCharterView().getHtml(null, locale, html.toString());

		//
		for (Account account : accounts)
		{
			// Do not check the email notification setting because sciboarder
			// must receive sciboard notifications (issue #109).
			this.eucles.sendMail(account.getEmail(), "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, newSubject, newContent);
		}
	}

	/**
	 * 
	 * @param subject
	 * @param html
	 * @throws Exception
	 */
	public void notifySciboarders(final Account author, final String subject, final CharSequence content, final Locale locale) throws Exception
	{
		//
		Accounts accounts = this.accountManager.findAccountsByRole(KiwaRoles.SCIBOARDER);
		accounts.remove(author);

		//
		String newSubject = "[Kinsources] " + subject;

		// Build the mail content.
		StringList html = new StringList();
		html.appendln("<div style=\"margin-left:100px;\">");
		html.appendln("<h1>Sciboarder Notification</h1>");
		html.appendln(content.toString());
		html.appendln("<small>You're receiving this notification because you are a Kinsources sciboard member.</small><br/>");
		html.appendln("<br/>");
		html.appendln("</div>");

		StringBuffer newContent = getEmailCharterView().getHtml(null, locale, html.toString());

		//
		for (Account account : accounts)
		{
			// Do not check the email notification setting because sciboarder
			// must receive sciboard notifications (issue #109).
			this.eucles.sendMail(account.getEmail(), "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, newSubject, newContent);
		}
	}

	/**
	 * 
	 * @param subject
	 * @param html
	 * @throws Exception
	 */
	public void notifyWebmasters(final Account author, final String subject, final CharSequence content, final Locale locale) throws Exception
	{
		//
		Accounts accounts = this.accountManager.findAccountsByRole(KiwaRoles.WEBMASTER);
		accounts.remove(author);

		//
		String newSubject = "[Kinsources] " + subject;

		// Build the mail content.
		StringList html = new StringList();
		html.appendln("<div style=\"margin-left:100px;\">");
		html.appendln("<h1>Webmaster Notification</h1>");
		html.appendln(content.toString());
		html.appendln("<small>You're receiving this notification because you are a Kinsources webmaster with email notification activated.</small><br/>");
		html.appendln("<br/>");
		html.appendln("</div>");

		StringBuffer newContent = getEmailCharterView().getHtml(null, locale, html.toString());

		//
		for (Account account : accounts)
		{
			if (account.isEmailNotification())
			{
				this.eucles.sendMail(account.getEmail(), "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, newSubject, newContent);
			}
		}
	}

	/**
	 * 
	 * @param dataset
	 * @return
	 */
	public String permanentURI(final Dataset dataset)
	{
		String result;

		//
		if (dataset == null)
		{
			result = null;
		}
		else
		{
			result = StringUtils.removeEnd(getWebsiteUrl(), "/")
					+ SimpleServletDispatcher.rewriteShortUrl("/kidarep/dataset", "xhtml", String.valueOf(dataset.getId()), XMLTools.escapeXmlBlank(dataset.getName()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void publishDataset(final Dataset source)
	{
		if (source != null)
		{
			//
			source.setPublished();

			this.storer.store(source);

			//
			this.stag.update();
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void publishWire(final long wireId) throws Exception
	{
		Wire target = this.actulog.getWireById(wireId);

		if (target == null)
		{
			throw new IllegalArgumentException("Unknown wire.");
		}
		else
		{
			target.publish();

			this.storer.store(target);
		}
	}

	/**
	 * 
	 * @param days
	 * @throws Exception
	 */
	public void purgeActalog(final int days) throws Exception
	{
		//
		DateTime limit = DateTime.now().minusDays(days);

		//
		for (EventLog eventLog : this.actalog.eventLogs().copy())
		{
			if (eventLog.getTime().isBefore(limit))
			{
				this.storer.remove(eventLog);
			}
		}

		//
		this.actalog.purge(days);
	}

	/**
	 * 
	 * @param days
	 * @throws Exception
	 */
	public synchronized void purgeActilog(final int days) throws Exception
	{
		//
		Logs logs = this.actilog.logs().copy().findBefore(DateTime.now().minusDays(days));

		//
		this.storer.remove(logs);

		//
		this.actilog.purge(days);
	}

	/**
	 * 
	 * @param days
	 * @throws Exception
	 */
	public void purgeFormatIssues(final int days) throws Exception
	{
		//
		DateTime limit = DateTime.now().minusDays(days);

		//
		for (FormatIssue formatIssue : this.kidarep.formatIssues().find(FormatIssue.Status.WAITING))
		{
			if (formatIssue.getCreationDate().isBefore(limit))
			{
				this.storer.remove(formatIssue);
			}
		}

		//
		this.kidarep.purgeFormatIssues(1);
	}

	/**
	 * @throws Exception
	 * @throws NumberFormatException
	 * 
	 */
	public void purgeOrphans() throws NumberFormatException, Exception
	{
		StringList orphanIds = findOrphanSubkeys();

		for (String orphanId : orphanIds)
		{
			logger.info("Purge orphan kidrarep file [{}/{}]", orphanId, orphanIds.size());
			this.storer.removeKidarepFile(Long.parseLong(orphanId));
		}
	}

	/**
	 * 
	 * @param net
	 */
	public void putPermanentLinkAttribute(final Net net, final Dataset dataset)
	{
		//
		if ((net != null) && (dataset != null))
		{
			net.attributes().put("Kinsources permanent link", Kiwa.instance().permanentURI(dataset));
		}
	}

	/**
	 * 
	 * @param dataset
	 * @param datasetFile
	 * @throws Exception
	 */
	public void rebuildGenealogyStatistics(final Dataset dataset, final DatasetFile datasetFile) throws Exception
	{
		if ((dataset != null) && (datasetFile != null))
		{
			//
			LoadedFileHeaders previousGraphics = datasetFile.statistics().graphics().copy();

			//
			LoadedFile loadedFile = getLoadedFile(datasetFile);
			Net net = loadNet(loadedFile);

			LoadedFiles newFiles = Stag.refreshStatistics(datasetFile.statistics(), net);

			//
			this.storer.store(dataset);
			for (LoadedFile file : newFiles)
			{
				this.storer.storeKidarepFile(file.header().id(), file.data());
			}
			for (LoadedFileHeader file : previousGraphics)
			{
				this.storer.removeKidarepFile(file.id());
			}
		}
	}

	/**
	 * This method rebuilds genealogy statistics. In case of huge dataset, the computing can take a very long time, so a
	 * thread way is used on them.
	 * 
	 * The computing sort is changed for thread way.
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void rebuildGenealogyStatisticsByThread(final Dataset dataset, final DatasetFile datasetFile) throws Exception
	{
		if ((dataset != null) && (datasetFile != null))
		{
			Thread thread = new Thread()
			{
				/**
				 * 
				 */
				@Override
				public void run()
				{
					Chronometer chrono = new Chronometer();
					try
					{
						//
						datasetFile.statistics().clear();
						datasetFile.statistics().add(StatisticTag.COMPUTING_BEGIN_DATETIME.name(), DateTime.now().toString());

						//
						LoadedFile loadedFile = getLoadedFile(datasetFile);
						final Net net = loadNet(loadedFile);

						int width = Stag.GENEALOGY_GRAPHIC_WIDTH;
						int height = Stag.GENEALOGY_GRAPHIC_HEIGHT;

						// Build basics statistics.
						{
							chrono.step();
							datasetFile.statistics().addAll(Stag.buildControlStatistics(net));
							datasetFile.statistics().add(StatisticTag.COMPUTING_CONTROLS_TIME.name(), chrono.step().partialInterval() / 1000);
							datasetFile.statistics().addAll(Stag.buildBasicStatistics(net));
							datasetFile.statistics().add(StatisticTag.COMPUTING_BASICS_TIME.name(), chrono.step().partialInterval() / 1000);
							datasetFile.statistics().attributeDescriptors().addAll(Stag.buildAttributeDescriptorStatistics(net));
							datasetFile.statistics().add(StatisticTag.COMPUTING_ATTRIBUTES_TIME.name(), chrono.step().partialInterval() / 1000);
						}

						// Build geography statistics.
						{
							datasetFile.statistics().addAll(Stag.buildGeographyStatistics(net));
							datasetFile.statistics().add(StatisticTag.COMPUTING_GEOGRAPHY_TIME.name(), chrono.step().runningInterval() / 1000);

							Kiwa.this.storer().store(dataset);
						}

						// Build circuit statistics.
						{
							datasetFile.statistics().addAll(Stag.buildCircuitStatistics(net));
							datasetFile.statistics().add(StatisticTag.COMPUTING_CIRCUITS_TIME.name(), chrono.step().partialInterval() / 1000);

							if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
							{
								Kiwa.this.storer().store(dataset);
							}
						}

						// 3
						{
							LoadedFile graphicFile = Stag.buildGraphicComponents(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(3, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_COMPONENTS_TIME.name(), chrono.step().partialInterval() / 1000);
								// No dataset store.
							}
						}

						// 4
						{
							LoadedFile graphicFile = Stag.buildGraphicCompleteness(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(4, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_COMPLETENESS_TIME.name(), chrono.step().partialInterval() / 1000);
								// No dataset store.
							}
						}

						// 5
						{
							LoadedFile graphicFile = Stag.buildGraphicSibsetDistribution(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(5, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_SIBSETDISTRIBUTION_TIME.name(), chrono.step().partialInterval() / 1000);
								Kiwa.this.storer().store(dataset);
							}
						}

						// 8
						{
							LoadedFile graphicFile = Stag.buildGraphicConsanguinePairs(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(8, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_CONSANGUINEPAIRS_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 1
						{
							LoadedFile graphicFile = Stag.buildGraphicBiasWeights(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(1, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_BIASWEIGHTS_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 2
						{
							LoadedFile graphicFile = Stag.buildGraphicBiasNetWeights(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(2, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_BIASNETWEIGHTS_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 9
						{
							LoadedFile graphicFile = Stag.buildGraphicGenders(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(9, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_GENDERS_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 10
						{
							LoadedFile graphicFile = Stag.buildGraphicPEDG2(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(10, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_PEDG2_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 11
						{
							LoadedFile graphicFile = Stag.buildGraphicPEDG3(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(11, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_PEDG3_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 12
						{
							LoadedFile graphicFile = Stag.buildGraphicUnionStatus(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(12, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_UNIONSTATUS_TIME.name(), chrono.step().partialInterval() / 1000);

								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 6
						{
							LoadedFile graphicFile = Stag.buildGraphicFirstCousinMarriages(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(6, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_FIRSTCOUSINMARRIAGES_TIME.name(), chrono.step().partialInterval() / 1000);
								if (chrono.interval() > STATISTICS_COMPUTING_MAXDELAY)
								{
									Kiwa.this.storer().store(dataset);
								}
							}
						}

						// 7
						{
							LoadedFile graphicFile = Stag.buildGraphicAncestorChains(net, width, height);
							if (graphicFile != null)
							{
								LoadedFileHeader header = new LoadedFileHeader(Kiwa.instance().kidarep().nextFileId(), graphicFile.header());
								Kiwa.this.storer.storeKidarepFile(header.id(), graphicFile.data());
								datasetFile.statistics().graphics().addToPosition(7, header);
								datasetFile.statistics().add(StatisticTag.COMPUTING_GRAPHIC_ANCESTORCHAINS_TIME.name(), chrono.step().partialInterval() / 1000);
								// No dataset store.
							}
						}

						datasetFile.statistics().add(StatisticTag.GRAPHICS_COUNT.name(), datasetFile.statistics().graphics().size());
						datasetFile.statistics().add(StatisticTag.COMPUTING_TIME.name(), chrono.step().interval() / 1000);
						datasetFile.statistics().add(StatisticTag.COMPUTING_END_DATETIME.name(), DateTime.now().toString());

						Kiwa.this.storer().store(dataset);
					}
					catch (Exception exception)
					{
						logger.debug("Error in thread.", exception);
					}
					finally
					{
						logger.debug("THREAD OVER [{}][{}][{}s].", Thread.currentThread().getId(), Thread.currentThread().getName(), chrono.step().interval() / 1000);
					}
				}
			};
			logger.debug("starting thread [{}][{}]", thread.getId(), thread.getName());
			thread.start();
			thread.join(STATISTICS_COMPUTING_MAXDELAY);
			if (thread.isAlive())
			{
				logger.debug("LET THE COMPUTING THREAD RUNNING…");
			}
		}
	}

	/**
	 * 
	 * @param dataset
	 * @throws Exception
	 */
	public void rebuildStatistics(final Dataset source) throws Exception
	{
		switch (source.getType())
		{
			case GENEALOGY:
			{
				logger.debug("rebuild genealogy statistics for origine file");
				rebuildGenealogyStatisticsByThread(source, source.getOriginFile());

				logger.debug("rebuild genealogy statistics for public file");
				rebuildGenealogyStatisticsByThread(source, source.getPublicFile());

				logger.debug("rebuild genealogy statistics for collaborator files");
				for (Collaborator collaborator : source.collaborators())
				{
					rebuildGenealogyStatisticsByThread(source, collaborator.getFile());
				}
			}
			break;

			case TERMINOLOGY:
			{
				logger.debug("rebuild terminology statistics for origine file");
				rebuildTerminologyStatistics(source, source.getOriginFile());

				logger.debug("rebuild terminology statistics for public file");
				rebuildTerminologyStatistics(source, source.getPublicFile());
			}
			break;
		}
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void rebuildStatistics(final Dataset dataset, final DatasetFile datasetFile) throws Exception
	{
		if ((dataset != null) && (datasetFile != null))
		{
			switch (dataset.getType())
			{
				case GENEALOGY:
					rebuildGenealogyStatisticsByThread(dataset, datasetFile);
				break;

				case TERMINOLOGY:
					rebuildTerminologyStatistics(dataset, datasetFile);
				break;
			}
		}
	}

	/**
	 * @throws KiwaException
	 * @throws Exception
	 * 
	 */
	public void rebuildStatistics(final Datasets source) throws KiwaException
	{
		//
		logger.debug("Starting datasets statistics rebuild...");

		//
		StringList errors = new StringList();
		long startTime = new Date().getTime();
		int datasetIndex = 0;
		for (Dataset dataset : source)
		{
			datasetIndex += 1;

			try
			{
				logger.info("==== {}/{} {}: {}", datasetIndex, source.size(), dataset.getId(), dataset.getName());
				rebuildStatistics(dataset);
			}
			catch (Exception exception)
			{
				logger.error("rebuild statistics failed for datasetId [" + dataset.getId() + "]", exception);
				errors.append(dataset.getId());
			}
		}

		//
		long endTime = new Date().getTime();
		logger.debug("Dataset statistics rebuild in {}ms.", endTime - startTime);

		if (!errors.isEmpty())
		{
			errors.set(0, "rebuild statistics gone some errors: ");
			throw new KiwaException("rebuild statistics gone some errors: " + errors.toStringWithCommas());
		}
	}

	/**
	 * 
	 * @param dataset
	 * @throws KiwaException
	 */
	public void rebuildStatisticsAll() throws KiwaException
	{
		rebuildStatistics(this.kidarep.datasetsFromIndex());
	}

	/**
	 * 
	 * @param dataset
	 * @throws KiwaException
	 */
	public void rebuildStatisticsGenealogies() throws KiwaException
	{
		rebuildStatistics(this.kidarep.datasetsFromIndex().find(Dataset.Type.GENEALOGY).findWithKinshipFile());
	}

	/**
	 * 
	 * @param dataset
	 * @throws KiwaException
	 */
	public void rebuildStatisticsTerminologies() throws KiwaException
	{
		rebuildStatistics(this.kidarep.datasetsFromIndex().find(Dataset.Type.TERMINOLOGY).findWithKinshipFile());
	}

	/**
	 * 
	 * @param dataset
	 * @throws Exception
	 */
	public void rebuildTerminologyStatistics(final Dataset dataset, final DatasetFile datasetFile) throws Exception
	{
		if ((dataset != null) && (datasetFile != null))
		{
			//
			LoadedFileHeaders previousGraphics = datasetFile.statistics().graphics().copy();

			//
			LoadedFile loadedFile = getLoadedFile(datasetFile);
			RelationModel model = loadTerminology(loadedFile);

			LoadedFiles newFiles = Stag.refreshStatistics(datasetFile.statistics(), model);

			//
			for (LoadedFile file : newFiles)
			{
				this.storer.storeKidarepFile(file.header().id(), file.data());
			}
			this.storer.store(dataset);
			for (LoadedFileHeader file : previousGraphics)
			{
				this.storer.removeKidarepFile(file.id());
			}
		}
	}

	/**
	 * 
	 * @param id
	 */
	public void removeAccount(final long accountId)
	{
		//
		Account target = this.accountManager.getAccountById(accountId);

		if (target == null)
		{
			throw new IllegalArgumentException("accountId is null.");
		}
		else
		{
			//
			this.accountManager.removeAccount(target);
			target.touch();

			//
			this.storer.store(target);

			//
			this.stag.updateActivatedAccountCount();
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeAllArticles() throws Exception
	{
		//
		for (Article article : this.hico.articles().copy())
		{
			//
			this.hico.removeArticle(article);

			//
			this.storer.remove(article);
		}

		//
		this.hico.resetLastId();

		//
		this.hico.resetBubbles();
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeArticle(final long articleId) throws Exception
	{
		Article target = this.hico.getArticleById(articleId);

		if (target == null)
		{
			throw new IllegalArgumentException("Unknown article.");
		}
		else
		{
			//
			this.hico.removeArticle(target);

			//
			this.storer.remove(target);

			//
			if (target.getName().equals(Hico.BUBBLE_ARTICLE_NAME))
			{
				this.hico.resetBubbles();
			}
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeCollaborator(final Dataset dataset, final Collaborator collaborator) throws Exception
	{
		if ((dataset != null) && (collaborator != null))
		{
			//
			dataset.collaborators().remove(collaborator);

			//
			this.storer.store(dataset);

			//
			this.storer.removeKidarepFile(collaborator.getFile().id());
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeCollaborator(final Dataset dataset, final long collaboratorId) throws Exception
	{
		if (dataset != null)
		{
			removeCollaborator(dataset, dataset.collaborators().getByAccountId(collaboratorId));
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeCollection(final Collection target) throws Exception
	{
		if (target == null)
		{
			throw new IllegalArgumentException("Unknown collection.");
		}
		else
		{
			this.kidarep.removeCollection(target);

			this.storer.remove(target);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeDataset(final Dataset source) throws Exception
	{
		if (source == null)
		{
			throw new IllegalArgumentException("dataset is null.");
		}
		else if (source.getStatus() == Dataset.Status.CREATED)
		{
			// Note collections containing the dataset.
			Collections collections = source.getContributor().findCollectionsWith(source);

			//
			this.kidarep.removeDataset(source);

			// Store new collections state.
			for (Collection collection : collections)
			{
				this.storer.store(collection);
			}

			//
			this.storer.remove(source);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeDatasetAttachment(final Dataset dataset, final LoadedFileHeader attachment) throws Exception
	{
		//
		dataset.attachments().remove(attachment);

		//
		this.storer.store(dataset);
		this.storer.removeKidarepFile(attachment.id());
	}

	/**
	 * 
	 * @param id
	 */
	public void removeDatasetFromCollection(final Dataset source, final Collection collection)
	{
		//
		this.kidarep.removeDatasetFromCollection(source, collection);

		//
		this.storer.store(collection);
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeDatasetOriginFile(final Dataset dataset) throws Exception
	{
		//
		LoadedFileHeaders previousFiles = new LoadedFileHeaders();
		if (dataset.getOriginFile() != null)
		{
			previousFiles.add(dataset.getOriginFile());
			previousFiles.addAll(dataset.getOriginFile().statistics().graphics());
		}

		//
		this.kidarep.removeDatasetFile(dataset);

		//
		this.storer.store(dataset);
		for (LoadedFileHeader file : previousFiles)
		{
			this.storer.removeKidarepFile(file.id());
		}

	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeFormatIssue(final FormatIssue source) throws Exception
	{
		if (source == null)
		{
			throw new IllegalArgumentException("formatIssue is null.");
		}
		else
		{
			//
			this.kidarep.removeFormatIssue(source);

			//
			this.storer.remove(source);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeForum(final Forum forum) throws Exception
	{
		//
		if (forum == null)
		{
			throw new IllegalArgumentException("Unknown forum.");
		}
		else
		{
			// Note: must use copy to avoid concurrent access.
			for (Topic topic : forum.topics().copy())
			{
				// Note: must use copy to avoid concurrent access.
				for (Message message : topic.messages().copy())
				{
					//
					this.agora.removeMessage(message);

					//
					this.storer.remove(message);
				}

				//
				this.agora.removeTopic(topic);

				//
				this.storer.remove(topic);
			}

			//
			this.agora.removeForum(forum);

			//
			this.storer.store(this.agora.forums());
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeForum(final long forumId) throws Exception
	{
		removeForum(this.agora.getForumById(forumId));
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeMessage(final long messageId) throws Exception
	{
		removeMessage(this.agora.getMessageById(messageId));
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeMessage(final Message message) throws Exception
	{
		//
		if (message == null)
		{
			throw new IllegalArgumentException("Unknown message.");
		}
		else
		{
			//
			this.agora.removeMessage(message);

			//
			this.storer.remove(message);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeTopic(final long topicId) throws Exception
	{
		removeTopic(this.agora.getTopicById(topicId));
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeTopic(final Topic topic) throws Exception
	{
		if (topic == null)
		{
			throw new IllegalArgumentException("Unknown topic.");
		}
		else
		{
			// Note: must use copy to avoid concurrent access.
			for (Message message : topic.messages().copy())
			{
				//
				this.agora.removeMessage(message);

				//
				this.storer.remove(message);
			}

			//
			this.agora.removeTopic(topic);

			//
			this.storer.remove(topic);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void removeWire(final long wireId) throws Exception
	{
		//
		Wire target = this.actulog.getWireById(wireId);

		if (target == null)
		{
			throw new IllegalArgumentException("Unknown wire.");
		}
		else
		{
			//
			this.actulog.removeWire(target);

			//
			this.storer.remove(target);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Sciboard sciboard()
	{
		return this.sciboard;
	}

	/**
	 * 
	 * @return
	 */
	public SimpleSecurityAgent securityAgent()
	{
		return this.securityAgent;
	}

	/**
	 * 
	 * @param to
	 * @param subject
	 * @param html
	 */
	public void sendEmail(final String to, final String subject, final CharSequence html)
	{
		this.eucles.sendMail(to, "kinsources@kinsources.net", "webmaster@kinsources.net", null, null, subject, html);
	}

	/**
	 * 
	 * @param account
	 * @param password
	 */
	public void setAccountPassword(final Account account, final String password)
	{
		//
		account.setPassword(password);

		//
		this.storer.store(account);
	}

	/**
	 * 
	 * @param charterView
	 */
	public void setCharterView(final KiwaCharterView view)
	{
		this.charterView = view;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public void setDataRoot(final String key, final Object root)
	{
		// if (key != null) {
		// this.database.put(new Element(key, root));
		// }
	}

	/**
	 * 
	 * @param charterView
	 */
	public void setEmailCharterView(final CharterView view)
	{
		this.emailCharterView = view;
	}

	/**
	 * 
	 * @param value
	 */
	public void setLogLevel(final Level value)
	{
		if (value == null)
		{
			throw new IllegalArgumentException("value is null.");
		}
		else
		{
			//
			this.actilog.setLogLevel(value);

			//
			this.storer.store(this.actilog);
		}
	}

	/**
	 * 
	 * @param value
	 *            in minutes
	 */
	public void setResetPasswordValidity(final int value)
	{
		this.resetPasswordValidity = value;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(final Kiwa.Status status)
	{
		this.status = status;

		this.storer.storeKiwaStatus(status);
	}

	/**
	 *
	 */
	public void setUserLanguage(final HttpServletResponse response, final String language)
	{
		if (language == null)
		{
			response.addCookie(CookieHelper.buildCookie(KIWA_LANGUAGE_COOKIE, "", 0, Scope.HTTP_AND_HTTPS));
		}
		else
		{
			response.addCookie(CookieHelper.buildCookie(KIWA_LANGUAGE_COOKIE, language, 10 * 365 * 24 * 60 * 60, Scope.HTTP_AND_HTTPS));
		}
	}

	/**
	 * 
	 * @param request
	 * @param ownerId
	 * @param value
	 */
	public void setVote(final Request request, final long ownerId, final Vote.Value value)
	{
		//
		this.sciboard.setVote(request, ownerId, value);

		//
		this.storer.store(request);
	}

	/**
	 * 
	 * @param path
	 */
	public void setWebContentPath(final String path)
	{
		this.webContentPath = path;
	}

	public Stag stag()
	{
		return this.stag;
	}

	/**
	 * 
	 * @return
	 */
	public KiwaStorer storer()
	{
		return this.storer;
	}

	/**
	 * 
	 * @param dataset
	 * @param comment
	 * @throws Exception
	 */
	public Request submitDataset(final Dataset dataset, final Account account, final String comment) throws Exception
	{
		Request result;

		//
		Properties parameters = new Properties();
		parameters.setProperty("dataset_id", String.valueOf(dataset.getId()));

		if (dataset.isGenealogy())
		{
			parameters.setProperty("filter", dataset.publicAttributeFilters().toString());
		}

		//
		result = this.sciboard.createRequest(account.getId(), comment, Request.Type.DATASET_PUBLISH, parameters);

		//
		if (dataset.getOriginFile() != null)
		{
			LoadedFile originFile = getLoadedFile(dataset.getOriginFile());

			LoadedFile publicFile = this.kidarep.addPublicDatasetFile(dataset, originFile);
			this.storer.storeKidarepFile(dataset.getPublicFile().id(), publicFile.data());
		}

		//
		dataset.setSubmitted();

		//
		this.storer.store(dataset);
		this.storer.store(result);

		//
		if (dataset.getOriginFile() != null)
		{
			rebuildStatistics(dataset, dataset.getPublicFile());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param filter
	 * @param comment
	 * @throws PuckException
	 */
	public Request submitDatasetUnpublish(final Dataset dataset, final Account account, final String comment) throws PuckException
	{
		Request result;

		//
		Properties parameters = new Properties();
		parameters.setProperty("dataset_id", String.valueOf(dataset.getId()));

		//
		result = this.sciboard.createRequest(account.getId(), comment, Request.Type.DATASET_UNPUBLISH, parameters);

		//
		this.storer.store(result);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	public void suspendAccount(final long accountId)
	{
		//
		Account target = this.accountManager.getAccountById(accountId);

		if (target == null)
		{
			throw new IllegalArgumentException("accountId is null.");
		}
		else
		{
			//
			target.setStatus(Account.Status.SUSPENDED);
			target.touch();

			//
			this.storer.store(target);

			//
			this.stag.updateActivatedAccountCount();
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void transferDataset(final Contributor newContributor, final Dataset source) throws Exception
	{
		if (source == null)
		{
			throw new IllegalArgumentException("dataset is null.");
		}
		else if (newContributor == null)
		{
			throw new IllegalArgumentException("New contributor is null.");
		}
		else if (source.getStatus() == Dataset.Status.VALIDATED)
		{
			//
			this.kidarep.transferDataset(newContributor, source);

			//
			this.storer.store(source);
		}
	}

	/**
	 * 
	 * @param dataset
	 */
	public void unpublishDataset(final Dataset dataset) throws Exception
	{
		//
		if (dataset != null)
		{
			LoadedFileHeaders previousFiles = new LoadedFileHeaders();
			if (dataset.getPublicFile() != null)
			{
				previousFiles.add(dataset.getPublicFile());
				previousFiles.addAll(dataset.getPublicFile().statistics().graphics());
				if (dataset.collaborators() != null)
				{
					for (Collaborator collaborator : dataset.collaborators())
					{
						previousFiles.add(collaborator.getFile());
						previousFiles.addAll(collaborator.getFile().statistics().graphics());
					}
				}
			}

			//
			dataset.setUnpublished();

			//
			this.stag.update();

			//
			this.storer.store(dataset);

			//
			for (LoadedFileHeader file : previousFiles)
			{
				this.storer.removeKidarepFile(file.id());
			}
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void unpublishWire(final long wireId) throws Exception
	{
		Wire target = this.actulog.getWireById(wireId);

		if (target == null)
		{
			throw new IllegalArgumentException("Unknown wire.");
		}
		else
		{
			target.unpublish();

			this.storer.store(target);
		}
	}

	/**
	 * 
	 * @param email
	 * @param fullName
	 * @param password
	 * @param emailScope
	 * @param organization
	 * @param businessCard
	 * @param country
	 * @param timeZone
	 * @param emailNotification
	 */
	public void updateAccount(final Account account, final String firstName, final String lastName, final String password, final EmailScope emailScope, final String organization,
			final String businessCard, final String country, final String website, final DateTimeZone timeZone, final boolean emailNotification)
	{
		logger.debug("[firstName={}][lastName={}][password={}][emailScope={}][organization={}][businessCard={}][country={}][website={}][timeZone={}][emailNotification={}]", firstName, lastName,
				password, emailScope, organization, businessCard, country, website, timeZone, emailNotification);

		if (account == null)
		{
			throw new IllegalArgumentException("account is null.");
		}
		else if (account.getStatus() != Account.Status.ACTIVATED)
		{
			throw new IllegalArgumentException("Invalid account.");
		}
		else
		{
			//
			account.setFullName(firstName, lastName);
			if (StringUtils.isNotBlank(password))
			{
				account.setPassword(password);
			}
			account.setOrganization(organization);
			account.setBusinessCard(businessCard);
			account.setWebsite(website);
			account.setCountry(country);
			account.setTimeZone(timeZone);
			account.setEmailScope(emailScope);
			account.setEmailNotification(emailNotification);

			//
			this.storer.store(account);
		}
	}

	/**
	 * 
	 * @param email
	 * @param fullName
	 * @param password
	 * @param emailScope
	 * @param organization
	 * @param businessCard
	 * @param country
	 * @param timeZone
	 * @param emailNotification
	 */
	public void updateAccountByWebmaster(final Account account, final String firstName, final String lastName, final String email, final EmailScope emailScope, final String organization,
			final String businessCard, final String country, final String website, final DateTimeZone timeZone, final boolean emailNotification, final Roles roles)
	{

		logger.debug("[firstName={},lastName={}][email={}][emailScope={}][organization={}][businessCard={}][country={}][website={}][timeZone={}][emailNotification={}]", firstName, lastName, email,
				emailScope, organization, businessCard, country, website, timeZone, emailNotification);

		if (account == null)
		{
			throw new IllegalArgumentException("account is null.");
		}
		else
		{

			Account emailAccount = this.accountManager.getAccountByEmail(email);

			if ((emailAccount != null) && (emailAccount.getId() != account.getId()))
			{
				throw new IllegalArgumentException("Email already in use.");
			}
			else
			{
				//
				account.setFullName(firstName, lastName);
				if (!StringUtils.equals(email, account.getEmail()))
				{
					account.setEmail(email);
					this.accountManager.updateIndexes(account);
				}
				account.setEmail(email);
				account.setOrganization(organization);
				account.setBusinessCard(businessCard);
				account.setWebsite(website);
				account.setCountry(country);
				account.setTimeZone(timeZone);
				account.setEmailScope(emailScope);
				account.setEmailNotification(emailNotification);

				account.roles().clear();
				for (Role role : roles)
				{
					account.roles().setRole(role);
				}

				//
				this.storer.store(account);
			}
		}
	}

	/**
	 * 
	 * @param fullName
	 * @param email
	 * @param password
	 * @return
	 */
	public void updateAccountEmail(final Account account, final String newEmail)
	{
		//
		account.setEmail(newEmail);
		account.touch();
		this.accountManager.updateIndexes(account);

		//
		this.storer.store(account);
	}

	/**
	 * 
	 */
	public void updateAccountLastConnection(final Account account)
	{
		account.updateLastConnection();
		this.storer.store(account);
	}

	/**
	 * 
	 * @param wire
	 * @param title
	 * @param author
	 * @param publicationDate
	 * @param locale
	 * @param leadParagraph
	 * @param body
	 */
	public void updateArticle(final Article article, final String name, final String author, final boolean visibility, final Locale locale, final String text)
	{
		//
		String oldName = article.getName();

		//
		this.hico.updateArticle(article, name, author, visibility, locale, text);

		//
		this.storer.store(article);

		//
		if (((StringUtils.equals(name, Hico.BUBBLE_ARTICLE_NAME))) || (StringUtils.equals(oldName, Hico.BUBBLE_ARTICLE_NAME)))
		{
			this.hico.resetBubbles();
		}
	}

	/**
	 * 
	 * @param collaboratorId
	 * @param description
	 * @param filters
	 * @throws Exception
	 */
	public Collaborator updateCollaborator(final Dataset dataset, final long collaboratorId, final String description, final AttributeFilters filters) throws Exception
	{
		Collaborator result;

		//
		result = dataset.collaborators().getByAccountId(collaboratorId);

		//
		if (result == null)
		{
			throw new IllegalArgumentException("Unknown collaborator.");
		}
		else if (dataset.getOriginFile() == null)
		{
			throw new IllegalArgumentException("Unavailable dataset file.");
		}
		else
		{
			//
			LoadedFileHeaders previousFiles = new LoadedFileHeaders();
			if (dataset.getOriginFile() != null)
			{
				previousFiles.add(result.getFile());
				previousFiles.addAll(result.getFile().statistics().graphics());
			}

			//
			LoadedFile sourceFile = getLoadedFile(dataset.getOriginFile());

			//
			LoadedFile collaboratorFile = this.kidarep.updateCollaborator(dataset, result, description, filters, sourceFile);

			//
			this.storer.storeKidarepFile(collaboratorFile.header().id(), collaboratorFile.data());
			this.storer.store(dataset);
			for (LoadedFileHeader file : previousFiles)
			{
				this.storer.removeKidarepFile(file.id());
			}

			//
			rebuildStatistics(dataset, result.getFile());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param contributor
	 * @param name
	 * @return
	 */
	public void updateDataset(final Dataset dataset)
	{
		this.storer.store(dataset);
	}

	/**
	 * 
	 * @param forum
	 * @param title
	 * @param subtitle
	 */
	public void updateForum(final Forum forum, final String title, final String subtitle)
	{
		logger.debug("[title={}][subtitle={}]", title, subtitle);

		if (forum == null)
		{
			throw new IllegalArgumentException("forum is null.");
		}
		else
		{
			//
			forum.setTitle(title);
			forum.setSubtitle(subtitle);

			//
			this.storer.store(this.agora.forums());
		}
	}

	/**
	 * 
	 * @param forum
	 * @param title
	 * @param subtitle
	 */
	public void updateMessage(final Message message, final String text)
	{
		if (message == null)
		{
			throw new IllegalArgumentException("message is null.");
		}
		else
		{
			message.setText(text);

			this.storer.store(message);
		}
	}

	/**
	 * 
	 * @param forum
	 * @param title
	 * @param subtitle
	 */
	public void updateTopic(final Topic topic, final long forumId, final String title, final boolean sticky, final Topic.Status status)
	{
		logger.debug("[title={}][forumId={}][status={}][sticky={}]", title, forumId, status, sticky);

		if (topic == null)
		{
			throw new IllegalArgumentException("topic is null.");
		}
		else
		{
			//
			topic.setTitle(title);
			topic.setStatus(status);
			topic.setSticky(sticky);

			//
			if (forumId != topic.getForum().getId())
			{
				//
				Forum target = this.agora.getForumById(forumId);

				//
				target.topics().add(topic);
				topic.getForum().topics().remove(topic);
				topic.setForum(target);
			}

			//
			this.storer.store(topic);
		}
	}

	/**
	 * 
	 * @param wire
	 * @param title
	 * @param author
	 * @param publicationDate
	 * @param locale
	 * @param leadParagraph
	 * @param body
	 */
	public void updateWire(final Wire wire, final String title, final String author, final DateTime publicationDate, final Locale locale, final String leadParagraph, final String body)
	{
		logger.debug("[title={}][author={}][publicationDate={}][locale={}][leadParagraph={}][body={}]", title, author, publicationDate, locale, leadParagraph, body);

		if (wire == null)
		{
			throw new IllegalArgumentException("wire is null.");
		}
		else
		{
			//
			wire.setTitle(title);
			wire.setAuthor(author);
			wire.setPublicationDate(publicationDate);
			wire.setLocale(locale);
			wire.setLeadParagraph(leadParagraph);
			wire.setBody(body);

			//
			this.storer.store(wire);
		}
	}

	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void upForum(final Forum forum) throws Exception
	{
		//
		this.agora.upForum(forum);

		//
		this.storer.store(this.agora.forums());
	}

	/**
	 * 
	 * @param source
	 * @param locale
	 * @param controls
	 * @return
	 * @throws KiwaControlException
	 * @throws IOException
	 * @throws KiwaBadFileFormatException
	 */
	public static Report controlNet(final LoadedFile source, final Locale locale, final List<ControlReporter.ControlType> controls) throws KiwaControlException, IOException,
			KiwaBadFileFormatException
	{
		Report result;

		try
		{
			Net net = Kiwa.loadNet(source);

			result = ControlReporter.controlNet(net, locale, controls);
		}
		catch (PuckException exception)
		{
			//
			logger.info("Exception detected controling file");
			logger.info(ExceptionUtils.getStackTrace(exception));

			//
			switch (PuckExceptions.valueOf(exception.getCode()))
			{
				case INVALID_PARAMETER:
					throw new IllegalArgumentException(exception.getMessage());

				case BAD_FILE_FORMAT:
				case UNSUPPORTED_ENCODING:
				case UNSUPPORTED_FILE_FORMAT:
					throw new KiwaBadFileFormatException(exception.getMessage());

				case FILE_NOT_FOUND:
				case IO_ERROR:
				case NOT_A_FILE:
					throw new IOException(exception.getMessage());

				default:
					throw new IllegalArgumentException(exception.getMessage());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetExtension
	 * @param sourceFileName
	 * @param sourceData
	 * @return
	 * @throws PuckException
	 */
	public static LoadedFile convert(final String targetExtension, final String targetName, final LoadedFile source) throws PuckException
	{
		LoadedFile result;

		Net net = loadNet(source);

		result = convertNet(targetExtension, targetName, net);

		//
		return result;
	}

	/**
	 * 
	 * @param targetExtension
	 * @param sourceFileName
	 * @param sourceData
	 * @return
	 * @throws PuckException
	 */
	public static LoadedFile convertNet(final String targetExtension, final String targetName, final Net net) throws PuckException
	{
		LoadedFile result;

		logger.info("Convert [{}] -> [{}]", targetName, targetExtension);

		if (targetExtension.equals("opaj"))
		{
			byte[] data = Kiwa.saveNetToByteArray(GraphType.OreGraph, net);

			result = new LoadedFile(FileTools.removeExtension(targetName) + "-oregraph.paj", data);
		}
		else if (targetExtension.equals("tpaj"))
		{
			byte[] data = Kiwa.saveNetToByteArray(GraphType.TipGraph, net);

			result = new LoadedFile(FileTools.removeExtension(targetName) + "-tipgraph.paj", data);
		}
		else if (targetExtension.equals("ppaj"))
		{
			byte[] data = Kiwa.saveNetToByteArray(GraphType.PGraph, net);

			result = new LoadedFile(FileTools.removeExtension(targetName) + "-pgraph.paj", data);
		}
		else
		{
			byte[] data = Kiwa.saveNetToByteArray(targetExtension, net);

			result = new LoadedFile(FileTools.setExtension(targetName, "." + targetExtension), data);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static Kiwa instance()
	{
		return SingletonHolder.instance;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws IOException
	 */
	public static BufferedImage loadImage(final LoadedFile source) throws IOException
	{
		BufferedImage result;

		ByteArrayInputStream buffer = new ByteArrayInputStream(source.data());
		result = ImageIO.read(buffer);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Net loadNet(final LoadedFile source) throws PuckException
	{
		Net result;

		result = Kiwa.loadNet(source, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws IOException
	 * @throws Exception
	 */
	public static Net loadNet(final LoadedFile source, final String charsetName) throws PuckException
	{
		Net result;

		try
		{
			//
			File tmp = null;
			try
			{
				String extension;
				if (source.name().matches("^.+\\.iur\\.(ods|txt|xls)$"))
				{
					extension = source.name().substring(source.name().lastIndexOf(".iur."));
				}
				else if (source.name().matches("^.+\\.bar\\.(ods|txt|xls)$"))
				{
					extension = source.name().substring(source.name().lastIndexOf(".bar."));
				}
				else
				{
					extension = source.name().substring(source.name().lastIndexOf("."));
				}

				tmp = new File(System.getProperty("java.io.tmpdir") + "/kinsources-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + "." + extension);
				logger.debug("tmp=[{}]", tmp);
				FileUtils.writeByteArrayToFile(tmp, source.data());

				result = PuckManager.loadNet(tmp, charsetName);

				//
				result.setLabel(source.name());
			}
			finally
			{
				if (tmp != null)
				{
					tmp.delete();
				}
			}
		}
		catch (IOException exception)
		{
			throw PuckExceptions.IO_ERROR.create("Temporary file error: " + exception.getMessage(), exception);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws IOException
	 * @throws Exception
	 */
	public static Net loadNetTXT1(final LoadedFile source, final String charsetName) throws PuckException
	{
		Net result;

		try
		{
			File tmp = null;
			try
			{
				tmp = new File(System.getProperty("java.io.tmpdir") + "/kinsources-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + "."
						+ FileTools.getExtension(source.name()));
				logger.debug("tmp=[{}]", tmp);
				FileUtils.writeByteArrayToFile(tmp, source.data());
				result = BARTXTFile.load(tmp, charsetName);
				result.setLabel(source.name());
			}
			finally
			{
				if (tmp != null)
				{
					tmp.delete();
				}
			}
		}
		catch (IOException exception)
		{
			throw PuckExceptions.IO_ERROR.create("Temporary file error: " + exception.getMessage(), exception);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws IOException
	 * @throws Exception
	 */
	public static Net loadNetXLS1(final LoadedFile source) throws PuckException
	{
		Net result;

		try
		{
			File tmp = null;
			try
			{
				tmp = new File(System.getProperty("java.io.tmpdir") + "/kinsources-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + "."
						+ FileTools.getExtension(source.name()));
				logger.debug("tmp=[{}]", tmp);
				FileUtils.writeByteArrayToFile(tmp, source.data());
				result = BARXLSFile.load(tmp);
				result.setLabel(source.name());
			}
			finally
			{
				if (tmp != null)
				{
					tmp.delete();
				}
			}
		}
		catch (IOException exception)
		{
			throw PuckExceptions.IO_ERROR.create("Temporary file error: " + exception.getMessage(), exception);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadTerminology(final LoadedFile source) throws PuckException
	{
		RelationModel result;

		result = Kiwa.loadTerminology(source, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param charsetName
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadTerminology(final LoadedFile source, final String charsetName) throws PuckException
	{
		RelationModel result;

		try
		{
			//
			File temporaryFile = null;
			try
			{
				String extension;
				if (source.name().matches("^.+\\.etic\\.(ods|txt|xls)$"))
				{
					extension = source.name().substring(source.name().lastIndexOf(".etic."));
				}
				else if (source.name().matches("^.+\\.emic\\.(ods|txt|xls)$"))
				{
					extension = source.name().substring(source.name().lastIndexOf(".emic."));
				}
				else
				{
					extension = source.name().substring(source.name().lastIndexOf(".term."));
				}

				temporaryFile = new File(System.getProperty("java.io.tmpdir") + "/kinsources-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + "." + extension);
				logger.debug("tmp=[{}]", temporaryFile);
				FileUtils.writeByteArrayToFile(temporaryFile, source.data());

				//
				result = PuckManager.loadRelationModel(temporaryFile);

				result.setName(source.name());
			}
			finally
			{
				if (temporaryFile != null)
				{
					temporaryFile.delete();
				}
			}
		}
		catch (IOException exception)
		{
			throw PuckExceptions.IO_ERROR.create("Temporary file error: " + exception.getMessage(), exception);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetFileName
	 * @param image
	 * @return
	 * @throws IOException
	 */
	public static LoadedFile saveImage(final String targetFileName, final BufferedImage image) throws IOException
	{
		LoadedFile result;

		if ((targetFileName == null) || (image == null))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			logger.debug("[targetFileName={}]", targetFileName);

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			ImageIO.write(image, "png", buffer);
			result = new LoadedFile(targetFileName, buffer.toByteArray());
			logger.debug("result length=[{}]", result.data().length);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetExtension
	 * @param sourceFileName
	 * @param sourceData
	 * @return
	 * @throws PuckException
	 */
	public static LoadedFile saveNet(final String targetFileName, final Net net) throws PuckException
	{
		LoadedFile result;

		logger.debug("[targetFileName={}]", targetFileName);

		byte[] data = Kiwa.saveNetToByteArray(FileTools.getExtension(targetFileName), net);

		result = new LoadedFile(targetFileName, data);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static byte[] saveNetToByteArray(final GraphType graphType, final Net net) throws PuckException
	{
		byte[] result;

		File tmp = null;
		try
		{
			tmp = new File(System.getProperty("java.io.tmpdir") + "/kinsources-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + "." + ".paj");
			logger.debug("tmp=[{}]", tmp);

			PAJFile.exportToPajek(net, tmp.getAbsolutePath(), graphType, null);

			result = FileUtils.readFileToByteArray(tmp);

		}
		catch (IOException exception)
		{
			throw PuckExceptions.IO_ERROR.create("Temporary file error: " + exception.getMessage(), exception);
		}
		finally
		{
			if (tmp != null)
			{
				tmp.delete();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static byte[] saveNetToByteArray(final String extension, final Net net) throws PuckException
	{
		byte[] result;

		File tmp = null;
		try
		{
			tmp = new File(System.getProperty("java.io.tmpdir") + "/kinsources-" + new Date().getTime() + "-" + RandomStringUtils.randomAlphanumeric(20) + "." + extension);
			logger.debug("tmp=[{}]", tmp);

			PuckManager.saveNet(tmp, net);

			result = FileUtils.readFileToByteArray(tmp);

		}
		catch (IOException exception)
		{
			throw PuckExceptions.IO_ERROR.create("Temporary file error: " + exception.getMessage(), exception);
		}
		finally
		{
			if (tmp != null)
			{
				tmp.delete();
			}
		}

		//
		return result;
	}
}
