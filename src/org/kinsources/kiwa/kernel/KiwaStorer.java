/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kernel;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.AccountManager;
import org.kinsources.kiwa.accounts.Accounts;
import org.kinsources.kiwa.accounts.XMLAccountManager;
import org.kinsources.kiwa.actalog.Actalog;
import org.kinsources.kiwa.actalog.EventLog;
import org.kinsources.kiwa.actalog.XMLActalog;
import org.kinsources.kiwa.actilog.Actilog;
import org.kinsources.kiwa.actilog.Log;
import org.kinsources.kiwa.actilog.Logs;
import org.kinsources.kiwa.actilog.XMLActilog;
import org.kinsources.kiwa.actulog.Actulog;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.actulog.XMLActulog;
import org.kinsources.kiwa.agora.Agora;
import org.kinsources.kiwa.agora.Forums;
import org.kinsources.kiwa.agora.Message;
import org.kinsources.kiwa.agora.Topic;
import org.kinsources.kiwa.agora.XMLAgora;
import org.kinsources.kiwa.hico.Article;
import org.kinsources.kiwa.hico.Hico;
import org.kinsources.kiwa.hico.XMLHico;
import org.kinsources.kiwa.kernel.Kiwa.Status;
import org.kinsources.kiwa.kidarep.Collaborator;
import org.kinsources.kiwa.kidarep.Collection;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.FormatIssue;
import org.kinsources.kiwa.kidarep.Kidarep;
import org.kinsources.kiwa.kidarep.LoadedFileHeader;
import org.kinsources.kiwa.kidarep.XMLKidarep;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.sciboard.Sciboard;
import org.kinsources.kiwa.sciboard.XMLSciboard;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.sikevadb.Element;
import fr.devinsy.sikevadb.Elements;
import fr.devinsy.sikevadb.SikevaDB;
import fr.devinsy.sikevadb.SikevaDBFactory;
import fr.devinsy.util.StringList;

/**
 * Class Kiwa uses singleton holder to be sure than Plaf modules are initialized.
 * 
 * 
 * @author Christian P. Momon
 */
public class KiwaStorer
{
	private static final Logger logger = LoggerFactory.getLogger(KiwaStorer.class);

	private SikevaDB database;
	private static final String ACCOUNTS_ACCOUNTS_KEY = "account_manager.accounts";
	private static final String ACCOUNTS_ROLES_KEY = "account_manager.roles";
	private static final String ACTALOG_EVENTLOGS_KEY = "actalog.logs";
	private static final String ACTILOG_LOGLEVEL_KEY = "actilog.log_level";
	private static final String ACTILOG_LOGS_KEY = "actilog.eventlogs";
	private static final String ACTULOG_WIRES_KEY = "actulog.wires";
	private static final String AGORA_FORUMS_KEY = "agora.forums";
	private static final String AGORA_TOPICS_KEY = "agora.topics";
	private static final String AGORA_MESSAGES_KEY = "agora.messages";
	private static final String HICO_ARTICLES_KEY = "hico.articles";
	private static final String KIDAREP_COLLECTIONS_KEY = "kidarep.collections";
	private static final String KIDAREP_DATASETS_KEY = "kidarep.datasets";
	private static final String KIDAREP_FILES_KEY = "kidarep.files";
	private static final String KIDAREP_FORMAT_ISSUES_KEY = "kidarep.format_issues";
	private static final String KIWA_RANDOMKEY_KEY = "kiwa.random_key";
	private static final String KIWA_STATUS_KEY = "kiwa.status";
	private static final String SCIBOARD_REQUESTS_KEY = "sciboard.requests";

	/**
	 * @throws Exception
	 * 
	 */
	public KiwaStorer(final String resourceName)
	{
		try
		{
			this.database = SikevaDBFactory.get(resourceName);
			this.database.open();
		}
		catch (Exception exception)
		{
			logger.error("Error opening database", exception);
			throw new IllegalArgumentException("Kiwa store init failed.");
		}
	}

	/**
	 * 
	 * @param resourceName
	 */
	public KiwaStorer(final String driverClassName, final String url, final String login, final String password)
	{
		try
		{
			this.database = SikevaDBFactory.get(driverClassName, url, login, password);
			this.database.open();
		}
		catch (Exception exception)
		{
			logger.error("Error opening database.", exception);
			throw new IllegalArgumentException("Kiwa store init failed.");
		}
	}

	/**
	 * 
	 * @return
	 */
	public SikevaDB database()
	{
		return this.database;
	}

	/**
	 * 
	 * @return
	 */
	public AccountManager getAccountManager()
	{
		AccountManager result;

		try
		{
			//
			String xmlRoles = this.database.getValue(ACCOUNTS_ROLES_KEY);

			if (xmlRoles == null)
			{
				result = null;
			}
			else
			{
				//
				result = new AccountManager();

				//
				XMLAccountManager.readRoles(result.roles(), xmlRoles);

				//
				StringList xmls = this.database.getValues(ACCOUNTS_ACCOUNTS_KEY);

				//
				for (String xml : xmls)
				{
					// logger.debug("[xml=" + xml + "]");

					//
					Account account = XMLAccountManager.readAccount(xml);

					//
					result.accounts().add(account);
				}

				//
				result.resetLastId();
				result.rebuildIndexes();
			}
		}
		catch (Exception exception)
		{
			logger.error("Error getting account managers.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Actalog getActalog()
	{
		Actalog result;

		try
		{
			//
			result = new Actalog();

			//
			StringList xmls = this.database.getValues(ACTALOG_EVENTLOGS_KEY);

			//
			for (String xml : xmls)
			{
				//
				EventLog event = XMLActalog.readEventLog(xml);

				//
				result.eventLogs().add(event);
			}

			//
			result.resetLastId();

		}
		catch (Exception exception)
		{
			logger.error("Error getting Actalog.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Actilog getActilog()
	{
		Actilog result;

		try
		{
			//
			result = new Actilog();

			//
			String value = this.database.getValue(ACTILOG_LOGLEVEL_KEY);
			if (value == null)
			{
				this.database.put(ACTILOG_LOGLEVEL_KEY, result.getLogLevel().toString());
			}
			else
			{
				result.setLogLevel(org.kinsources.kiwa.actilog.Log.Level.valueOf(value));
			}

			//
			StringList xmls = this.database.getValues(ACTILOG_LOGS_KEY);

			//
			for (String xml : xmls)
			{
				//
				Log log = XMLActilog.readLog(xml);

				//
				result.logs().add(log);
			}

			//
			result.resetLastId();

		}
		catch (Exception exception)
		{
			logger.error("Error getting Actilog.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Actulog getActulog()
	{
		Actulog result;

		try
		{
			//
			result = new Actulog();

			//
			StringList xmls = this.database.getValues(ACTULOG_WIRES_KEY);

			//
			for (String xml : xmls)
			{
				//
				Wire wire = XMLActulog.readWire(xml);

				//
				result.wires().add(wire);
			}

			//
			result.resetLastId();
			result.rebuildIndexes();

		}
		catch (Exception exception)
		{
			logger.error("Error getting Actulog.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Agora getAgora()
	{
		Agora result;

		try
		{
			//
			result = new Agora();

			//
			String xmlForums = this.database.getValue(AGORA_FORUMS_KEY);

			if (xmlForums != null)
			{
				//
				XMLAgora.readForums(result.forums(), xmlForums);

				//
				result.rebuildForumIndexes();

				//
				StringList xmls = this.database.getValues(AGORA_TOPICS_KEY);
				for (String xml : xmls)
				{
					//
					Topic topic = XMLAgora.readTopic(xml);
					topic.setForum(result.getForumById(topic.getForum().getId()));
					topic.getForum().topics().add(topic);
				}

				//
				result.rebuildTopicIndexes();

				//
				xmls = this.database.getValues(AGORA_MESSAGES_KEY);
				for (String xml : xmls)
				{
					//
					Message message = XMLAgora.readMessage(xml);
					message.setTopic(result.getTopicById(message.getTopic().getId()));
					message.getTopic().messages().add(message);
				}

				//
				result.rebuildMessageIndexes();
			}

			//
			result.resetLastIds();

		}
		catch (Exception exception)
		{
			logger.error("Error getting Agora.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Hico getHico()
	{
		Hico result;

		try
		{
			//
			result = new Hico();

			//
			StringList xmlArticles = this.database.getValues(HICO_ARTICLES_KEY);

			//
			if (xmlArticles != null)
			{
				for (String xml : xmlArticles)
				{
					Article article = XMLHico.readArticle(xml);

					result.articles().add(article);
				}
			}

			//
			result.resetLastId();
			result.rebuildIndexes();
			result.resetBubbles();

		}
		catch (Exception exception)
		{
			logger.error("Error getting Hico.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Kidarep getKidarep(final Accounts accounts)
	{
		Kidarep result;

		StringList xmls = null;
		try
		{
			//
			result = new Kidarep();

			//
			for (Account account : accounts)
			{
				result.contributors().add(new Contributor(account));
			}
			result.rebuildContributorIndex();

			//
			xmls = this.database.getValues(KIDAREP_DATASETS_KEY);
			for (String xml : xmls)
			{
				//
				Dataset dataset = XMLKidarep.readDataset(xml);

				//
				Contributor contributor = result.getContributorById(dataset.getContributor().getId());
				dataset.setContributor(contributor);
				contributor.personalDatasets().add(dataset);

				//
				for (Collaborator collaborator : dataset.collaborators())
				{
					Account account = accounts.getById(collaborator.getAccount().getId());
					collaborator.setAccount(account);
				}
			}
			result.rebuildDatasetIndex();
			result.resetLastDatasetId();

			//
			xmls = this.database.getValues(KIDAREP_COLLECTIONS_KEY);
			for (String xml : xmls)
			{
				Collection collection = XMLKidarep.readCollection(xml);

				Contributor contributor = result.getContributorById(collection.getOwner().getId());
				collection.setOwner(contributor);
				contributor.collections().add(collection);
			}
			result.rebuildCollectionIndex();
			result.resetLastCollectionId();

			//
			long max = 0;
			StringList keys = this.database.getSubkeys(KIDAREP_FILES_KEY);
			for (String key : keys)
			{
				long value = Long.parseLong(key);
				if (value > max)
				{
					max = value;
				}
			}
			result.resetLastFileId(max);

			//
			xmls = this.database.getValues(KIDAREP_FORMAT_ISSUES_KEY);
			for (String xml : xmls)
			{
				FormatIssue formatIssue = XMLKidarep.readFormatIssue(xml);

				result.formatIssues().add(formatIssue);
			}
			result.resetLastFormatIssueId();

		}
		catch (Exception exception)
		{
			if (xmls == null)
			{
				logger.error("xmls bugged: null", xmls);
			}
			else
			{
				logger.error("xmls bugged: {}", xmls.toString().substring(0, 300));
			}
			logger.error("Error getting Kidarep.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public byte[] getKidarepFile(final long id)
	{
		byte[] result;

		try
		{
			String value = this.database.getValue(KIDAREP_FILES_KEY, String.valueOf(id));

			if (value == null)
			{
				result = null;
			}
			else
			{
				result = Hex.decodeHex(value.toCharArray());
			}
		}
		catch (Exception exception)
		{
			logger.error("Error getting Kidarep file.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getKiwaRandomKey()
	{
		String result;

		try
		{
			//
			String value = this.database.getValue(KIWA_RANDOMKEY_KEY);
			if (value == null)
			{
				result = null;
			}
			else
			{
				result = new String(Base64.decodeBase64(value));
			}

		}
		catch (Exception exception)
		{
			logger.error("Error getting Kiwa Random Key.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Status getKiwaStatus()
	{
		Status result;

		try
		{
			//
			String value = this.database.getValue(KIWA_STATUS_KEY);
			if (value == null)
			{
				result = null;
			}
			else
			{
				result = Status.valueOf(value);
			}

		}
		catch (Exception exception)
		{
			logger.error("Error getting Kiwa Status.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Sciboard getSciboard()
	{
		Sciboard result;

		try
		{
			//
			result = new Sciboard();

			//
			StringList xmls = this.database.getValues(SCIBOARD_REQUESTS_KEY);
			for (String xml : xmls)
			{
				//
				Request request = XMLSciboard.readRequest(xml);

				result.requests().add(request);
			}

			//
			result.rebuildIndexes();

			//
			result.resetLastRequestId();

		}
		catch (Exception exception)
		{
			logger.error("Error getting Sciboard.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}

		//
		return result;
	}

	/**
	 * @throws Exception
	 * 
	 */
	public void patch20131115() throws Exception
	{

		// request.liner_id -> request.contributor_id
		// request.liner_comment -> request.contributor_comment
		Elements elements = this.database.getAllElements(SCIBOARD_REQUESTS_KEY);
		long count = 0;
		for (Element element : elements)
		{
			//
			System.out.println("actilog " + count++ + " " + element.getSubkey() + "/" + elements.size());

			//
			element.setValue(element.getValue().replace("liner_id", "contributor_id").replace("liner_comment", "contributor_comment"));

			//
			this.database.put(element.getKey(), element.getSubkey(), element.getValue());

			System.out.println("ok");
		}

		// dataset.liner_id -> dataset.contributor_id
		elements = this.database.getAllElements(KIDAREP_DATASETS_KEY);
		count = 0;
		for (Element element : elements)
		{
			//
			System.out.println("dataset " + count++ + " " + element.getSubkey() + "/" + elements.size());

			//
			element.setValue(element.getValue().replace("liner_id", "contributor_id"));

			//
			this.database.put(element.getKey(), element.getSubkey(), element.getValue());
		}

		// actilog: liner_id -> request.contributor_id
		elements = this.database.getAllElements(ACTILOG_LOGS_KEY);
		count = 0;
		for (Element element : elements)
		{
			//
			System.out.println("actilog " + count++ + " " + element.getSubkey() + "/" + elements.size());

			//
			element.setValue(element.getValue().replace("liner_id", "contributor_id"));

			//
			this.database.put(element.getKey(), element.getSubkey(), element.getValue());
		}

		System.out.println("over");
	}

	/**
	 * 
	 * Corpus -> Dataset Veto -> Vote
	 * 
	 * 
	 * @throws Exception
	 */
	public void patch20131211() throws Exception
	{

		logger.info("*******************************************************");
		logger.info("PATCH STARTING...");
		logger.info("*******************************************************");

		//
		this.database.clearAllArchive();

		//
		this.database.renameKey("kidarep.corpora", "kidarep.datasets");

		//
		this.database.replaceInValues(SCIBOARD_REQUESTS_KEY,

		"<veto>", "<vote>",

		"</veto>", "</vote>",

		"<vetos/>", "<votes/>",

		"<vetos", "<votes",

		"</vetos>", "</votes>",

		">corpus_id<", ">dataset_id<",

		">CORPUS_PUBLISH<", ">DATASET_PUBLISH<",

		">CORPUS_UNPUBLISH<", ">DATASET_UNPUBLISH<");

		//
		this.database.replaceInValues(KIDAREP_DATASETS_KEY,

		"<owner/>", "",

		"<owner>.*</owner>", "",

		"<corpus>", "<dataset>",

		"</corpus>", "</dataset>",

		"<corpus_file/>", "<dataset_file/>",

		"<corpus_file>", "<dataset_file>",

		"</corpus_file>", "</dataset_file>",

		"<corpora>", "<datasets>",

		"</corpora>", "</datasets>");

		//
		this.database.replaceInValues(ACTILOG_LOGS_KEY,

		"_corpus_", "_dataset_",

		">corpus", ">dataset",

		"corpus<", "dataset<",

		"corpusId", "datasetId");

		System.out.println("over");

		logger.info("*******************************************************");
		logger.info("PATCH DONE.");
		logger.info("*******************************************************");
	}

	/**
	 * Rename ACCEPTED_WITH_MODIFICATION as ACCEPTED_WITH_REVISIONS.
	 * 
	 * @throws Exception
	 */
	public void patch20151223() throws Exception
	{

		logger.info("*******************************************************");
		logger.info("PATCH 20151223 STARTING...");
		logger.info("*******************************************************");

		Elements elements = this.database.getAllElements(SCIBOARD_REQUESTS_KEY);
		long count = 0;
		for (Element element : elements)
		{
			//
			if (StringUtils.contains(element.getValue(), "<status>ACCEPTED_WITH_MODIFICATION</status>"))
			{
				//
				count += 1;
				logger.info("patch " + count + " " + element.getSubkey() + "/" + elements.size());

				logger.info(element.getValue());

				//
				element.setValue(element.getValue().replace("<status>ACCEPTED_WITH_MODIFICATION</status>", "<status>ACCEPTED_WITH_REVISIONS</status>"));

				logger.info(element.getValue());

				//
				this.database.put(element);
			}
		}

		logger.info("*******************************************************");
		logger.info("PATCH 20151223 DONE.");
		logger.info("*******************************************************");
	}

	/**
	 * 
	 * Add dataset type field after the status field.
	 * 
	 * 
	 * @throws Exception
	 */
	public void patch20161028() throws Exception
	{

		logger.info("*******************************************************");
		logger.info("PATCH STARTING 20161028…");
		logger.info("*******************************************************");

		Elements elements = this.database.getElements(KIDAREP_DATASETS_KEY);
		logger.info("Element count={}", elements.size());

		Element element = elements.get(0);
		// logger.info(element.getValue());

		if (element.getValue().contains("</status><type>KINSHIP</type>"))
		{
			this.database.replaceInValues(KIDAREP_DATASETS_KEY, "</status><type>KINSHIP</type>", "</status><type>GENEALOGY</type>");
			logger.info("Done.");
		}
		else if (element.getValue().contains("</status><type>"))
		{
			logger.info("No applied.");
		}
		else
		{
			this.database.replaceInValues(KIDAREP_DATASETS_KEY, "</status>", "</status><type>KINSHIP</type>");
			logger.info("Done.");
		}
		// this.database.replaceInValues(KIDAREP_DATASETS_KEY, "<type>CORPUS</type>", "");

		logger.info("*******************************************************");
		logger.info("PATCH DONE.");
		logger.info("*******************************************************");
	}

	/**
	 * 
	 * Split account fullName field in firstName and lastName.
	 * 
	 * 
	 * @throws Exception
	 */
	public void patch20161112() throws Exception
	{

		logger.info("*******************************************************");
		logger.info("PATCH STARTING 20161112…");
		logger.info("*******************************************************");

		Elements elements = this.database.getElements(ACCOUNTS_ACCOUNTS_KEY);
		logger.info("Element count={}", elements.size());

		Element first = elements.get(0);
		logger.info(first.getValue());

		if (first.getValue().contains("</email><full_name>"))
		{
			logger.info("Applying…");
			long count = 0;
			for (Element element : elements)
			{
				//
				logger.info("patching {} subkey [{}] ({}/{})", element.getKey(), element.getSubkey(), count, +elements.size());

				if (element.getSubkey() != null)
				{
					//
					count += 1;

					//
					String value = element.getValue();

					//
					int startIndex = value.indexOf("<full_name>");
					int endIndex = value.indexOf("</full_name>") + 12;
					String start = value.substring(0, startIndex);
					String end = value.substring(endIndex);
					// logger.info(start + "###" + end);

					String fullName = value.substring(startIndex + 11, endIndex - 12).trim();
					logger.info("full_name=[{}]", fullName);

					String firstNames;
					String lastName;
					int spaceIndex = fullName.indexOf(" ");
					if (spaceIndex == -1)
					{
						firstNames = null;
						lastName = fullName;
					}
					else
					{
						firstNames = fullName.substring(0, spaceIndex);
						lastName = fullName.substring(spaceIndex + 1);
					}

					logger.info("[firstNames={}][lastName={}]", firstNames, lastName);

					String firstNamesTarget;
					if (StringUtils.isBlank(firstNames))
					{
						firstNamesTarget = "<first_names/>";
					}
					else
					{
						firstNamesTarget = "<first_names>" + firstNames + "</first_names>";
					}

					String lastNameTarget;
					if (StringUtils.isBlank(lastName))
					{
						lastNameTarget = "<last_name/>";
					}
					else
					{
						lastNameTarget = "<last_name>" + lastName + "</last_name>";
					}

					String target = String.format("%s%s%s%s", start, firstNamesTarget, lastNameTarget, end);

					logger.info("target=[{}]", target);

					//
					this.database.put(element.getKey(), element.getSubkey(), target);
				}
			}
			logger.info("Done.");
		}
		else
		{
			logger.info("No applied.");
		}

		logger.info("*******************************************************");
		logger.info("PATCH DONE.");
		logger.info("*******************************************************");
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Article source) throws Exception
	{
		this.database.archive(HICO_ARTICLES_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Collection source) throws Exception
	{
		this.database.archive(KIDAREP_COLLECTIONS_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Dataset source) throws Exception
	{
		//
		this.database.archive(KIDAREP_DATASETS_KEY, String.valueOf(source.getId()));

		//
		if (source.getOriginFile() != null)
		{
			this.database.archive(KIDAREP_FILES_KEY, String.valueOf(source.getOriginFile().id()));
			for (LoadedFileHeader file : source.getOriginFile().statistics().graphics())
			{
				this.database.remove(KIDAREP_FILES_KEY, String.valueOf(file.id()));
			}
		}

		if (source.getPublicFile() != null)
		{
			this.database.archive(KIDAREP_FILES_KEY, String.valueOf(source.getPublicFile().id()));
			for (LoadedFileHeader file : source.getPublicFile().statistics().graphics())
			{
				this.database.remove(KIDAREP_FILES_KEY, String.valueOf(file.id()));
			}
		}

		//
		for (LoadedFileHeader attachment : source.attachments())
		{
			this.database.archive(KIDAREP_FILES_KEY, String.valueOf(attachment.id()));
		}
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final EventLog source) throws Exception
	{
		this.database.archive(ACTALOG_EVENTLOGS_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final FormatIssue source) throws Exception
	{
		this.database.archive(KIDAREP_FORMAT_ISSUES_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Log source) throws Exception
	{
		this.database.remove(ACTILOG_LOGS_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Logs source) throws Exception
	{
		//
		String[] subkeys = new String[source.size()];
		for (int index = 0; index < subkeys.length; index++)
		{
			//
			subkeys[index] = String.valueOf(source.getByIndex(index).getId());
		}

		//
		String[][] batchs = KiwaUtils.split(subkeys, 100);

		//
		for (String[] batch : batchs)
		{
			//
			this.database.removeMany(ACTILOG_LOGS_KEY, batch);
		}
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Message source) throws Exception
	{
		this.database.archive(AGORA_MESSAGES_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Topic source) throws Exception
	{
		this.database.archive(AGORA_TOPICS_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void remove(final Wire source) throws Exception
	{
		this.database.archive(ACTULOG_WIRES_KEY, String.valueOf(source.getId()));
	}

	/**
	 * 
	 * @param source
	 * @throws Exception
	 */
	public void removeKidarepFile(final long id) throws Exception
	{
		logger.debug("remove kidarep file [{}]", id);
		this.database.remove(KIDAREP_FILES_KEY, String.valueOf(id));
	}

	/**
	 *
	 */
	public void store(final Account source)
	{
		try
		{
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				String xml = XMLAccountManager.toXMLString(source);
				this.database.put(ACCOUNTS_ACCOUNTS_KEY, String.valueOf(source.getId()), xml);
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a wire.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final AccountManager source)
	{
		try
		{
			String xml = XMLAccountManager.toXMLString(source.roles());
			this.database.put(ACCOUNTS_ROLES_KEY, xml);
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a AccountManager.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Actilog source)
	{
		try
		{
			this.database.put(ACTILOG_LOGLEVEL_KEY, source.getLogLevel().toString());
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Actilog.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Article source)
	{
		try
		{
			String xml = XMLHico.toXMLString(source);
			this.database.put(HICO_ARTICLES_KEY, String.valueOf(source.getId()), xml);
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Article.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Collection source)
	{
		try
		{
			this.database.put(KIDAREP_COLLECTIONS_KEY, String.valueOf(source.getId()), XMLKidarep.toXMLString(source));
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Collection.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Dataset source)
	{
		try
		{
			this.database.put(KIDAREP_DATASETS_KEY, String.valueOf(source.getId()), XMLKidarep.toXMLString(source));
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Dataset.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 *
	 */
	public void store(final EventLog source)
	{
		try
		{
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				String xml = XMLActalog.toXMLString(source);
				this.database.put(ACTALOG_EVENTLOGS_KEY, String.valueOf(source.getId()), xml);
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a EventLog.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 *
	 */
	public void store(final FormatIssue source)
	{
		try
		{
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				String xml = XMLKidarep.toXMLString(source);
				this.database.put(KIDAREP_FORMAT_ISSUES_KEY, String.valueOf(source.getId()), xml);
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a FormatIssue.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Forums source)
	{
		try
		{
			this.database.put(AGORA_FORUMS_KEY, XMLAgora.toXMLString(source));
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Forums.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Log source)
	{
		//
		try
		{
			//
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				String xml = XMLActilog.toXMLString(source);
				this.database.put(ACTILOG_LOGS_KEY, String.valueOf(source.getId()), xml);
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Log.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Message source)
	{
		try
		{
			this.database.put(AGORA_MESSAGES_KEY, String.valueOf(source.getId()), XMLAgora.toXMLString(source));
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Message.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 *
	 */
	public void store(final Request source)
	{
		try
		{
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				String xml = XMLSciboard.toXMLString(source);
				this.database.put(SCIBOARD_REQUESTS_KEY, String.valueOf(source.getId()), xml);
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Request.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void store(final Topic source)
	{
		try
		{
			this.database.put(AGORA_TOPICS_KEY, String.valueOf(source.getId()), XMLAgora.toXMLString(source));
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Topic.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 *
	 */
	public void store(final Wire source)
	{
		try
		{
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				String xml = XMLActulog.toXMLString(source);
				this.database.put(ACTULOG_WIRES_KEY, String.valueOf(source.getId()), xml);
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a Wire.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void storeKidarepFile(final long id, final byte[] source)
	{

		logger.debug("storeKidarepFile [id={}]", id);

		try
		{
			if (source == null)
			{
				throw new IllegalArgumentException("source is null.");
			}
			else
			{
				this.database.put(KIDAREP_FILES_KEY, String.valueOf(id), Hex.encodeHexString(source));
			}
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing a kidarep file.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void storeKiwaRandomKey(final String value)
	{
		try
		{
			this.database.put(KIWA_RANDOMKEY_KEY, Base64.encodeBase64String(value.getBytes()));
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing the Kiwa Random Key.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void storeKiwaStatus(final Status value)
	{
		try
		{
			this.database.put(KIWA_STATUS_KEY, value.toString());
		}
		catch (Exception exception)
		{
			logger.error("Error detected storing the Kiwa Status.", exception);
			throw new InternalError("DATABASE FATAL ERROR");
		}
	}
}
