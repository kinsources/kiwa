/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actalog;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.actalog.Actalog;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.util.rss.RSSCache;

/**
 *
 */
public class Kinsources_website_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Kinsources_website_xhtml.class);
	private static final int FEED_SIZE = 10;
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");

			// Get parameters.
			// ===============
			String[] parameters = SimpleServletDispatcher.shortRewritedUrlParameters(request);

			//
			Locale locale;
			if ((parameters != null) && (parameters.length > 0) && (parameters[0] != null))
			{
				locale = new Locale(parameters[0]);
			}
			else
			{
				locale = Locale.ENGLISH;
			}
			logger.info("locale=[{}]", locale);

			if ((locale == null) || (locale.getLanguage().length() == 0))
			{
				kiwa.logPageHit("pages.actulog.kinsources_website", request);
			}
			else
			{
				kiwa.logPageHit("pages.actulog.kinsources_website-" + locale.getLanguage(), request);
			}

			// Use parameters.
			// ===============
			String rss = RSSCache.instance().get(Actalog.RSS_NAME, locale);
			logger.debug("rss[{}]={}", locale.getLanguage(), rss);
			if (rss == null)
			{
				logger.debug("Refresh cache");
				rss = kiwa.actalog().toRSS(FEED_SIZE, locale, kiwa.getWebsiteUrl());
				RSSCache.instance().put(Actalog.RSS_NAME, locale, rss);
			}

			//
			String fileName;
			if ((locale == null) || (locale.getLanguage().length() == 0))
			{
				fileName = Actalog.RSS_NAME + ".rss";
			}
			else
			{
				fileName = Actalog.RSS_NAME + "-" + locale.getLanguage() + ".rss";
			}

			//
			response.reset();
			response.setContentType("application/rss+xml; charset=UTF-8");
			// response.setContentLength(rss.length());
			response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
			response.getWriter().println(rss);
			response.flushBuffer();

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
