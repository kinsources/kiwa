/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actulog;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Actulog_admin_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Actulog_admin_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/actulog/actulog_admin.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.actulog.actulog_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters
				// ==============

				// Use parameters
				// ==============
				TagDataManager data = new TagDataManager();

				//
				data.setContent("total_wire_count", kiwa.actulog().wires().size());
				data.setContent("last_id", kiwa.actulog().lastId());

				//
				int lineIndex = 0;
				for (Wire wire : kiwa.actulog().wires().copy().sortByPublicationDate().reverse())
				{
					//
					data.setContent("wire", lineIndex, "wire_id", wire.getId());
					data.setContent("wire", lineIndex, "wire_title", StringEscapeUtils.escapeXml(wire.getTitle()));
					data.setContent("wire", lineIndex, "wire_language", wire.getLocale().getLanguage());
					if (wire.getPublicationDate() != null)
					{
						data.setContent("wire", lineIndex, "wire_date", wire.getPublicationDate().toString("dd/MM/yyyy"));
					}
					data.setAttribute("wire", lineIndex, "wire_title", "href", "wire_edition.xhtml?target_id=" + wire.getId());
					data.setContent("wire", lineIndex, "wire_status", wire.status().toString());
					data.setContent("wire", lineIndex, "wire_author", StringEscapeUtils.escapeXml(wire.getAuthor()));

					//
					lineIndex += 1;
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(userAccount.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
