/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actulog;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Wire_creation_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Wire_creation_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/actulog/wire_creation.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.actulog.wire_creation", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String sourceWireIdParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("target_id")));
				logger.info("sourceWireIdParameter=[{}]", sourceWireIdParameter);

				// Use parameters.
				// ===============
				TagDataManager data = new TagDataManager();

				//
				String sourceLanguage = null;
				if (sourceWireIdParameter != null)
				{
					long sourceWireId = Long.parseLong(sourceWireIdParameter);
					Wire source = kiwa.actulog().getWireById(sourceWireId);
					if (source != null)
					{
						data.setAttribute("title", "value", StringEscapeUtils.escapeXml(source.getTitle()));
						if (source.getAuthor() != null)
						{
							data.setAttribute("author", "value", StringEscapeUtils.escapeXml(source.getAuthor()));
						}
						if (source.getPublicationDate() != null)
						{
							data.setAttribute("publication_date", "value", source.getPublicationDate().toString("dd/MM/yyyy"));
						}
						if (source.getLeadParagraph() != null)
						{
							data.setContent("lead_paragraph", StringEscapeUtils.escapeXml(source.getLeadParagraph()));
						}
						if (source.getBody() != null)
						{
							data.setContent("body", StringEscapeUtils.escapeXml(source.getBody()));
						}
						sourceLanguage = source.getLocale().getLanguage();
					}
				}

				//
				data.setContent("label_title", "Title:");
				data.setContent("label_author", "Author:");
				data.setAttribute("author", "value", userAccount.getFullName());
				data.setContent("label_publication_date", "Publication date:");
				data.setContent("label_status", "Status:");

				data.setContent("label_language", "Language:");
				data.setContent("label_lead_paragraph", "Lead paragraph:");
				data.setContent("label_body", "Body:");

				//
				data.setContent("status", "Not created");

				//
				int lineIndex = 0;
				for (String language : kiwa.languages())
				{
					//
					data.setContent("language_option", lineIndex, language);
					data.setAttribute("language_option", lineIndex, "value", language);

					if ((sourceLanguage != null) && (language.equals(sourceLanguage)))
					{
						data.setAttribute("language_option", lineIndex, "selected", "selected");
					}

					//
					lineIndex += 1;
				}

				// Send response.
				// ==============
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer page = kiwa.getCharterView().getHtml(userAccount.getId(), locale, content);

				//
				// response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				response.getWriter().println(page);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
