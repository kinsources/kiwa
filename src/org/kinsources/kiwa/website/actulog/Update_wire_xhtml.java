/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actulog;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Update_wire_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Update_wire_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.actulog.update_wire", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String targetWireIdParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("target_id")));
				logger.info("targetWireId=[{}]", targetWireIdParameter);
				long targetWireId = Long.parseLong(targetWireIdParameter);

				System.out.println("===+>" + request.getCharacterEncoding());
				String title = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("title")));
				logger.info("title=[{}]", title);

				String author = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("author")));
				logger.info("author=[{}]", author);

				String publicationDateParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("publication_date")));
				logger.info("publicationDate=[{}]", publicationDateParameter);
				DateTime publicationDate;
				if (publicationDateParameter == null)
				{
					publicationDate = null;
				}
				else
				{
					publicationDate = DateTime.parse(publicationDateParameter, DateTimeFormat.forPattern("dd/MM/yyyy"));
				}

				String languageParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("language")));
				if (languageParameter == null)
				{
					languageParameter = "en";
				}
				logger.info("language=[{}]", languageParameter);
				Locale localeParameter = new Locale(languageParameter);

				String leadParagraph = XMLTools.toHTLM5(StringUtils.trim(request.getParameter("lead_paragraph")));
				logger.info("leadParagraph=[{}]", leadParagraph);
				logger.info("body1=[{}]", request.getParameter("body"));
				String body = XMLTools.toHTLM5(StringUtils.trim(request.getParameter("body")));
				logger.info("body=[{}]", body);

				// Use parameters.
				// ===============
				if (StringUtils.isBlank(title))
				{
					throw new IllegalArgumentException("Title undefined.");
				}
				else if (StringUtils.isBlank(leadParagraph))
				{
					throw new IllegalArgumentException("Lead paragraph undefined.");
				}
				else
				{
					Wire wire = kiwa.actulog().getWireById(targetWireId);

					if (wire == null)
					{
						throw new IllegalArgumentException("Undefined wire.");
					}
					else
					{
						logger.info("UPDATE WIRE [wireId={}]", wire.getId());
						kiwa.log(Level.INFO, "events.actulog.update_wire", "[wireId={}]", wire.getId());

						//
						kiwa.updateWire(wire, title, author, publicationDate, localeParameter, leadParagraph, body);

						//
						Redirector.redirect(response, "wire_edition.xhtml?target_id=" + wire.getId());
					}
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			//
			StringList message = new StringList();

			message.appendln("<p>Your wire edition is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");

			ErrorView.show(request, response, "Wire update issue", message, "/actulog/actulog_admin.xhtml");

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
