/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.hico;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.FileItemHelper;
import fr.devinsy.kiss4web.Redirector;

/**
 *
 */
public class Import_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 4492307319738030023L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Import_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("Hico export starting...");
			kiwa.logPageHit("pages.hico.import", request);

			//
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if ((account == null) || (!account.isRole(KiwaRoles.REDACTOR)))
			{
				//
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
				upload.setSizeMax(80000000);
				List /* FileItem */items = upload.parseRequest(request);

				//
				FileItem fileSource = FileItemHelper.getItem(items, "file_to_import");
				if (fileSource == null)
				{
					logger.info("fileSource=[null]");
				}
				else
				{
					logger.info("fileSource=[{}]", fileSource.getName());
				}

				// Use parameters.
				// ===============
				if (fileSource == null)
				{
					throw new IllegalArgumentException("Invalid upload.");
				}
				else if (!kiwa.isRedactor(account))
				{
					throw new IllegalArgumentException("Permission denied.");
				}
				else if ((StringUtils.isBlank(fileSource.getName())) || (fileSource.getSize() == 0))
				{
					throw new IllegalArgumentException("Bad file.");
				}
				else
				{
					//
					logger.info("IMPORT HICO ARTICLES [name={}][size={}]", fileSource.getName(), fileSource.getSize());
					kiwa.log(Level.INFO, "events.import_hico_articles", "[accountId={}][name={}][size={}]", account.getId(), fileSource.getName(), fileSource.getSize());

					//
					kiwa.importArticles(fileSource.get());

					//
					Redirector.redirect(response, "/hico/hico_admin.xhtml");
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Hico import issue", exception.getMessage(), "/hico/hico_admin.xhtml");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
