/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.hico;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.hico.Article;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Create_article_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 7178321421060490363L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Create_article_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.hico.create_article", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.REDACTOR)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String name = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("name")));
				logger.info("name=[{}]", name);

				String author = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("author")));
				logger.info("author=[{}]", author);

				boolean visibility = Boolean.valueOf(request.getParameter("visibility"));
				logger.info("visibility=[{}]", visibility);

				String languageParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("language")));
				if (languageParameter == null)
				{
					languageParameter = Locale.ENGLISH.getLanguage();
				}
				logger.info("language=[{}]", languageParameter);
				Locale localeParameter = new Locale(languageParameter);

				String text = XMLTools.toHTLM5(StringUtils.trim(request.getParameter("text")));
				logger.info("text=[{}]", text);

				// Use parameters.
				// ===============
				if (StringUtils.isBlank(name))
				{
					throw new IllegalArgumentException("Name undefined.");
				}
				else if (StringUtils.isBlank(text))
				{
					throw new IllegalArgumentException("Text undefined.");
				}
				else
				{
					Article article = kiwa.createArticle(name, author, visibility, localeParameter, text);

					logger.info("ARTICLE CREATION");
					kiwa.log(Level.INFO, "events.hico.article_creation", "[articleId={}]", article.getId());

					//
					Redirector.redirect(response, "article_edition.xhtml?target_id=" + article.getId());
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			//
			StringList message = new StringList();

			message.appendln("<p>Your article edition is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");

			ErrorView.show(request, response, "Article creation issue", message, "/hico/hico_admin.xhtml");

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
