/**
 * Copyright 2013-2015,2019 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.kernel.EnvironmentInformation;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.Kiwa.Status;
import org.kinsources.kiwa.website.charter.EmailCharterView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class KiwaDispatcher extends SimpleServletDispatcher
{
	public enum State
	{
		INITIALIZING,
		INITIALIZED
	}

	private static final long serialVersionUID = 3450146249729600552L;
	private static Logger logger = LoggerFactory.getLogger(KiwaDispatcher.class);

	private static URLPresenter initializing = new URLPresenter("/org/kinsources/kiwa/website/charter/initializing.html");
	private static URLPresenter initFailed = new URLPresenter("/org/kinsources/kiwa/website/charter/init_failed.html");
	private static State state = State.INITIALIZING;

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		//
		request.setCharacterEncoding("UTF-8");

		//
		if (state == State.INITIALIZING)
		{
			String html = initializing.toString();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		else if (Kiwa.instance().getStatus() == Status.INIT_FAILED)
		{
			String html = initFailed.toString();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		else
		{
			super.doGet(request, response);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		//
		request.setCharacterEncoding("UTF-8");

		//
		if (state == State.INITIALIZING)
		{
			String html = initializing.toString();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		else if (Kiwa.instance().getStatus() == Status.INIT_FAILED)
		{
			String html = initFailed.toString();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		else
		{
			super.doPost(request, response);
		}
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
		//
		super.init();
		state = State.INITIALIZING;

		Thread thread = new Thread()
		{
			@Override
			public void run()
			{
				//
				String logFilePathname;
				try
				{
					logFilePathname = new EnvironmentInformation().log4jFilePathname();
				}
				catch (Exception exception)
				{
					logger.error("Log configuration file not defined in environment information.");
					logFilePathname = null;
				}

				// Set logger.
				if (logFilePathname == null)
				{
					System.out.println("Log configuration file not available, use of the basic configurator.");
					org.apache.log4j.BasicConfigurator.configure();
				}
				else
				{
					System.out.println("Log configuration found (" + logFilePathname + "), will use it.");

					// Check if the log4j file is in the war.
					if (!logFilePathname.startsWith("/"))
					{
						logFilePathname = getServletContext().getRealPath("/") + logFilePathname;
					}

					//
					org.apache.log4j.PropertyConfigurator.configure(logFilePathname);

					//
					logger = LoggerFactory.getLogger(this.getClass());
				}
				logger.info("Log initialization done.");
				logger.info("Configuration log file: {}", logFilePathname);

				//
				logger.info("KiwaDispatcher init started.");

				//
				Kiwa kiwa = Kiwa.instance();

				//
				kiwa.setWebContentPath(getServletContext().getRealPath("/"));
				logger.info("setWebContentPath=[" + kiwa.getWebContentPath() + "]");
				System.out.println("setWebContentPath=[" + kiwa.getWebContentPath() + "]");

				//
				Kiwa.instance().setCharterView(new KiwaCharterView());
				Kiwa.instance().setEmailCharterView(new EmailCharterView());

				//
				state = KiwaDispatcher.State.INITIALIZED;

				//
				logger.info("KiwaDispatcher initialization done.");
			}
		};
		thread.start();
	}
}
