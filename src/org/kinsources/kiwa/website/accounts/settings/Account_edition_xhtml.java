/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.EmailScope;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.utils.CountryList;
import org.kinsources.kiwa.utils.GMTShortList;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.myspace.MySpaceView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Account_edition_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Account_edition_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/accounts/settings/account_edition.html");
	private static MySpaceView mySpaceView = new MySpaceView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.accounts.settings.account_edition", request);

			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Use parameters.
			// ===============
			Account account = kiwa.accountManager().getAccountById(accountId);

			if (account == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("label_first_names", "First names:");
				data.setContent("label_last_name", "Last name:");
				data.setAttribute("first_names", "value", XMLTools.escapeXmlBlank(account.getFirstNames()));
				data.setAttribute("last_name", "value", XMLTools.escapeXmlBlank(account.getLastName()));
				data.setContent("label_email", "Email:");
				data.setContent("email", XMLTools.escapeXmlBlank(account.getEmail()));
				data.setContent("label_email_scope", "Email is public:");
				if (account.getEmailScope() == EmailScope.PUBLIC)
				{
					data.setAttribute("email_scope", "checked", "checked");
				}
				data.setContent("label_password", "Password:");
				data.setAttribute("password", "pattern", Account.PASSWORD_PATTERN);
				data.setAttribute("password", "title", "At least 8 letters (character or digit)");
				data.setContent("label_password_confirmation", "Confirm password:");
				data.setAttribute("password_confirmation", "pattern", Account.PASSWORD_PATTERN);
				data.setAttribute("password_confirmation", "title", "At least 8 letters (character or digit)");
				data.setContent("label_organization", "Organization:");
				data.setAttribute("organization", "value", XMLTools.escapeXmlBlank(account.getOrganization()));
				data.setContent("label_about_me", "About me:");
				data.setContent("about_me", XMLTools.escapeXmlBlank(account.getBusinessCard()));
				data.setContent("label_website", "Personal website:");
				data.setAttribute("website", "value", XMLTools.escapeXmlBlank(account.getWebsite()));
				data.setContent("label_country", "Country:");
				data.setAttribute("country", "value", XMLTools.escapeXmlBlank(account.getCountry()));
				data.setContent("label_timezone", "Timezone:");
				data.setContent("label_email_notification", "Email notification:");
				if (account.isEmailNotification())
				{
					data.setAttribute("email_notification", "checked", "checked");
				}
				data.setAttribute("button_save", "value", "Update account");

				// TODO set default timezone from
				// request.getLocale().getCountry().

				//
				GMTShortList gmtIds = GMTShortList.instance();
				for (int index = 0; index < gmtIds.size(); index++)
				{
					data.setContent("timezone_option", index, gmtIds.getLabel(index));
					data.setAttribute("timezone_option", index, "value", gmtIds.getId(index));
				}

				data.setAttribute("timezone_option", gmtIds.indexOf(account.getTimeZone()), "selected", "selected");

				//
				int countryIndex = 0;
				for (String country : CountryList.values())
				{
					//
					data.setContent("country_option", countryIndex, country);
					data.setAttribute("country_option", countryIndex, "value", country);

					//
					countryIndex += 1;
				}

				// Send response.
				// ==============
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer page = kiwa.getCharterView().getHtml(accountId, locale, mySpaceView.getHtml(MySpaceView.Menu.MY_ACCOUNT, content, locale));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(page);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
