/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.registering;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.actalog.EventLog.Category;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;

/**
 *
 */
public class Confirm_registration_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Confirm_registration_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static final int EXPIRATION_DAYS_COUNT = 2;

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);

			//
			kiwa.logPageHit("pages.accounts.registering.confirm_registration", request);

			String idParameter = StringUtils.trim(request.getParameter("id"));
			logger.info("id=[{}]", idParameter);

			String activationTimeParameter = StringUtils.trim(request.getParameter("activation_time"));
			logger.info("activationTime=[{}]", activationTimeParameter);

			String activationKey = StringUtils.trim(request.getParameter("activation_key"));
			logger.info("emailConfirmation=[{}]", activationKey);

			// Use parameters.
			// ===============
			logger.info("CONFIRM_REGISTRATION for [id={}][activationTime={}][activationKey={}]", idParameter, activationTimeParameter, activationKey);

			//
			if ((StringUtils.isBlank(idParameter)) || (!NumberUtils.isNumber(idParameter)))
			{
				throw new IllegalArgumentException("Bad parameter (id).");
			}
			else if ((StringUtils.isBlank(activationTimeParameter)) || (!NumberUtils.isNumber(activationTimeParameter)))
			{
				throw new IllegalArgumentException("Bad parameter (at).");
			}
			else if (StringUtils.isBlank(activationKey))
			{
				throw new IllegalArgumentException("Bad parameter (ak).");
			}
			else
			{
				//
				String confirmKey = kiwa.securityAgent().computeAuth(idParameter, activationTimeParameter);

				//
				if (!StringUtils.equals(confirmKey, activationKey))
				{
					//
					throw new IllegalArgumentException("Compromised parameters (ak).");

				}
				else
				{
					//
					int accountId = Integer.parseInt(idParameter);
					Account account = kiwa.accountManager().getAccountById(accountId);

					if (account == null)
					{
						throw new IllegalArgumentException("Bad parameter (an).");
					}
					else if (account.getStatus() != Status.CREATED)
					{
						throw new IllegalArgumentException("Request previously submited. Your account is already activated.");
					}
					else
					{
						long activationTime = Long.parseLong(activationTimeParameter);

						if (activationTime < account.getEditionDate().getMillis())
						{
							throw new IllegalArgumentException("Outdated resquest.");
						}
						else if (DateTime.now().isAfter(new DateTime(activationTime).plusDays(EXPIRATION_DAYS_COUNT)))
						{
							throw new IllegalArgumentException("Expired request. Please, register again to receive a new validation email.");
						}
						else
						{
							//
							kiwa.log(Level.INFO, "events.registration_confirmed", "[id={}][fullName={}]", accountId, account.getFullName());

							//
							kiwa.activateAccount(account);

							//
							kiwa.logEvent(Category.REGISTRATION, account.getFullName());

							//
							notifyWebmasters(account, locale);

							//
							Redirector.redirect(response, "registration_confirmed.xhtml");
						}
					}
				}
			}
		}
		catch (IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Registration issue", exception.getMessage(), "/");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private void notifyWebmasters(final Account account, final Locale locale) throws Exception
	{
		//
		StringList directLink = new StringList();
		directLink.append(kiwa.getWebsiteUrl());
		directLink.append("accounts/admin/account_edition.xhtml");
		directLink.append("?target_id=").append(account.getId());

		//
		StringList html = new StringList();
		html.appendln("<p>New account creation confirmed.</p>");
		html.appendln("<p>");
		html.append("Email: ").append(StringEscapeUtils.escapeXml(account.getEmail())).appendln("<br/>");
		html.append("Full name: ").append(StringEscapeUtils.escapeXml(account.getFullName())).appendln("<br/>");
		html.append("<br/>");
		html.append("<a href=\"").append(directLink.toString()).appendln("\" class=\"button_second\">Direct link</a><br/>");
		html.appendln("</p>");

		//
		kiwa.notifyWebmasters(account, "Account event: new account confirmed", html.toString(), locale);
	}
}

// ////////////////////////////////////////////////////////////////////////
