/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.sciboard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.sciboard.Requests;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Requests_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Requests_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/sciboard/requests.html");
	private static final Kiwa kiwa = Kiwa.instance();
	private static final int WARNING_DELAY = 14;
	private static final int ALERT_DELAY = 31;

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.sciboard.requests", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			if ((!kiwa.isSciboarder(account)) && (!kiwa.isWebmaster(account)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String requestStatusFilter = request.getParameter("status_filter");
				logger.info("requestStatusFilter=[{}]", requestStatusFilter);

				String requestTypeFilter = request.getParameter("type_filter");
				logger.info("requestTypeFilter=[{}]", requestTypeFilter);

				// Use parameters.
				// ===============
				Requests requests = kiwa.sciboard().requests().copy();

				if (!StringUtils.equals(requestStatusFilter, "ALL"))
				{
					Request.Status statusFilter = EnumUtils.getEnum(Request.Status.class, requestStatusFilter);

					if (statusFilter == null)
					{
						requests = requests.findByStatus(Request.Status.OPENED);
					}
					else
					{
						requests = requests.findByStatus(statusFilter);
					}
				}

				if ((requestTypeFilter != null) && (!StringUtils.equals(requestTypeFilter, "ALL")))
				{
					Dataset.Type typeFilter = EnumUtils.getEnum(Dataset.Type.class, requestTypeFilter);

					if (typeFilter != null)
					{
						Requests target = new Requests();
						for (Request query : requests)
						{
							Dataset.Type queryType = kiwa.extractDatasetTypeFromRequest(query);
							if (queryType == typeFilter)
							{
								target.add(query);
							}
						}
						requests = target;
					}
				}

				// requests = requests.findByProperty("dataset_type", requestTypeFilter);

				// Send response.
				// ==============
				TagDataManager data = new TagDataManager();

				//
				if (StringUtils.isNotBlank(requestTypeFilter))
				{
					data.appendAttribute("status_all", "href", "/sciboard/requests.xhtml?status_filter=ALL&amp;type_filter=" + requestTypeFilter);
					data.appendAttribute("status_opened", "href", "/sciboard/requests.xhtml?status_filter=OPENED&amp;type_filter=" + requestTypeFilter);
					data.appendAttribute("status_accepted", "href", "/sciboard/requests.xhtml?status_filter=ACCEPTED&amp;type_filter=" + requestTypeFilter);
					data.appendAttribute("status_accepted_w_r", "href", "/sciboard/requests.xhtml?status_filter=ACCEPTED_WITH_REVISIONS&amp;type_filter=" + requestTypeFilter);
					data.appendAttribute("status_cancelled", "href", "/sciboard/requests.xhtml?status_filter=CANCELLED&amp;type_filter=" + requestTypeFilter);
					data.appendAttribute("status_declined", "href", "/sciboard/requests.xhtml?status_filter=DECLINED&amp;type_filter=" + requestTypeFilter);
				}

				if (StringUtils.isNotBlank(requestStatusFilter))
				{
					data.appendAttribute("type_genealogy", "href", "/sciboard/requests.xhtml?type_filter=GENEALOGY&amp;status_filter=" + requestStatusFilter);
					data.appendAttribute("type_terminology", "href", "/sciboard/requests.xhtml?type_filter=TERMINOLOGY&amp;status_filter=" + requestStatusFilter);
					data.appendAttribute("type_all", "href", "/sciboard/requests.xhtml?status_filter=" + requestStatusFilter);
				}

				//
				data.setContent("request_count", requests.size());

				if (requests.isEmpty())
				{
					data.setContent("request", "<td colspan=\"10\">No request.</td>");
				}
				else
				{
					//
					int lineIndex = 0;
					for (Request query : requests.copy().reverse())
					{
						//
						data.setContent("request", lineIndex, "request_id", query.getId());
						data.setAttribute("request", lineIndex, "request_id", "href", "request.xhtml?request_id=" + query.getId());
						data.setContent("request", lineIndex, "request_author", XMLTools.escapeXmlBlank(kiwa.accountManager().getAccountById(query.getContributorId()).getFullName()));
						data.setContent("request", lineIndex, "request_type", KiwaUtils.humanName(query.getType()));
						data.setContent("request", lineIndex, "request_description", kiwa.buildRequestDescription(query));
						data.setAttribute("request", lineIndex, "request_description", "href", "request.xhtml?request_id=" + query.getId());
						data.setContent("request", lineIndex, "request_status", KiwaUtils.humanName(query.getStatus()));
						data.setContent("request", lineIndex, "request_comments", query.countOfComments());
						data.setContent("request", lineIndex, "request_my_vote", KiwaUtils.humanName(query.voteValueOf(account.getId())));
						if (query.getDecisionDate() == null)
						{
							data.setContent("request", lineIndex, "request_date", query.getCreationDate().toString("dd/MM/yyyy"));
						}
						else
						{
							data.setContent("request", lineIndex, "request_date", query.getDecisionDate().toString("dd/MM/yyyy"));
						}
						int delay = query.delay();
						data.setContent("request", lineIndex, "request_delay", delay);
						if (query.getStatus() == Request.Status.OPENED)
						{
							if (delay > ALERT_DELAY)
							{
								data.setAttribute("request", lineIndex, "request_delay", "class", "center danger");
							}
							else if (delay > WARNING_DELAY)
							{
								data.setAttribute("request", lineIndex, "request_delay", "class", "center warning");
							}
						}

						switch (query.getStatus())
						{
							case OPENED:
								data.setAttribute("request", lineIndex, "request_status", "class", "center alert");
							break;

							case ACCEPTED:
								data.setAttribute("request", lineIndex, "request_status", "class", "center success");
							break;

							case ACCEPTED_WITH_REVISIONS:
								data.setAttribute("request", lineIndex, "request_status", "class", "center warning");
							break;

							case DECLINED:
								data.setAttribute("request", lineIndex, "request_status", "class", "center active");
							break;

							case CANCELLED:
								data.setAttribute("request", lineIndex, "request_status", "class", "center info");
							break;
						}

						//
						lineIndex += 1;
					}
				}

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, content);

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
