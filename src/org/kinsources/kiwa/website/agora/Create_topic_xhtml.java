/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.agora;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.agora.Forum;
import org.kinsources.kiwa.agora.Topic;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Create_topic_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Create_topic_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.agora.create_topic", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if (Account.isNotActivated(userAccount))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String forumIdParameter = request.getParameter("forum_id");
				logger.info("forumId=[{}]", forumIdParameter);
				long forumId = Long.parseLong(forumIdParameter);

				String title = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("title")));
				logger.info("title=[{}]", title);

				boolean sticky = Boolean.parseBoolean(request.getParameter("sticky"));
				logger.info("sticky=[{}]", sticky);

				String statusParameter = request.getParameter("status");
				logger.info("status=[{}]", statusParameter);
				Topic.Status status;
				if (statusParameter == null)
				{
					status = Topic.Status.OPENED;
				}
				else
				{
					status = Topic.Status.valueOf(statusParameter);
				}

				String message = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("message")));
				logger.info("message=[{}]", message);

				// Use parameters.
				// ===============
				Forum forum = kiwa.agora().getForumById(forumId);

				if (forum == null)
				{
					throw new IllegalArgumentException("Unknown forum [" + forumId + "].");
				}
				else
				{
					//
					if (StringUtils.isBlank(title))
					{
						throw new IllegalArgumentException("Title undefined.");
					}
					else if (StringUtils.isBlank(message))
					{
						throw new IllegalArgumentException("Message undefined.");
					}
					else
					{
						//
						Topic topic = kiwa.createTopic(forum, title, message, sticky, status, userAccount);

						logger.info("TOPIC CREATION");
						kiwa.log(Level.INFO, "events.agora.create_topic", "[forumId={}][topicId={}]", forum.getId(), topic.getId());

						//
						notifyModerators(userAccount, topic, locale);

						//
						Redirector.redirect(response, "forum.xhtml?forum_id=" + forum.getId());
					}
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your topic creation is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");

			ErrorView.show(request, response, "Topic creation issue", message, "/agora/forums.xhtml");

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private void notifyModerators(final Account author, final Topic topic, final Locale locale) throws Exception
	{
		//
		StringList directLink = new StringList();
		directLink.append(kiwa.getWebsiteUrl());
		directLink.append("agora/topic.xhtml");
		directLink.append("?topic_id=").append(topic.getId());

		//
		StringList html = new StringList();
		html.appendln("<p>New topic was created.</p>");
		html.appendln("<p>");
		html.append("Author: ").append(StringEscapeUtils.escapeXml(author.getFullName())).appendln("<br/>");
		html.append("Forum: ").append(StringEscapeUtils.escapeXml(topic.getForum().getTitle())).appendln("<br/>");
		html.append("Topic title: ").append(StringEscapeUtils.escapeXml(topic.getTitle())).appendln("<br/>");
		html.append("<br/>");
		html.append("<a href=\"").append(directLink.toString()).appendln("\" class=\"button_second\">Direct link</a><br/>");
		html.appendln("</p>");

		//
		kiwa.notifyModerators(author, "Agora event: new topic", html.toString(), locale);
	}
}

// ////////////////////////////////////////////////////////////////////////
