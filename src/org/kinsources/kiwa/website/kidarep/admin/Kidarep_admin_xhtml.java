/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.admin;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.kidarep.Kidarep;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Kidarep_admin_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Kidarep_admin_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/admin/kidarep_admin.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.admin.kidarep_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{

				// Get parameters
				// ==============

				// Use parameters
				// ==============
				Kidarep kidarep = kiwa.kidarep();

				//
				TagDataManager data = new TagDataManager();

				{
					data.setContent("contributor_count", kidarep.countOfContributors());
					data.setContent("collaborator_count", kidarep.countOfCollaborators());
					data.setContent("dataset_count", kidarep.countOfDatasets());
					data.setContent("collection_count", kidarep.countOfCollections());
					data.setContent("file_count", kidarep.countOfFiles());
					data.setContent("dataset_file_count", kidarep.countOfDatasetFiles());
					data.setContent("attachment_file_count", kidarep.countOfAttachmentFiles());
					data.setContent("format_issue_count", kidarep.countOfFormatIssues());
					data.setContent("format_issue_ready_count", kidarep.countOfReadyFormatIssues());
				}

				{
					data.setContent("type_genealogy_count", kidarep.countOfGenealogies());
					data.setContent("type_terminology_count", kidarep.countOfTerminologies());
					data.setContent("type_all_count", kidarep.countOfDatasets());
				}

				{
					Datasets datasets = kidarep.datasets();
					data.setContent("status_genealogy_created_count", datasets.findGenealogies().count(Dataset.Status.CREATED));
					data.setContent("status_terminology_created_count", datasets.findTerminologies().count(Dataset.Status.CREATED));
					data.setContent("status_created_count", datasets.count(Dataset.Status.CREATED));
					data.setContent("status_genealogy_submitted_count", datasets.findGenealogies().count(Dataset.Status.SUBMITTED));
					data.setContent("status_terminology_submitted_count", datasets.findTerminologies().count(Dataset.Status.SUBMITTED));
					data.setContent("status_submitted_count", datasets.count(Dataset.Status.SUBMITTED));
					data.setContent("status_genealogy_validated_count", datasets.findGenealogies().count(Dataset.Status.VALIDATED));
					data.setContent("status_terminology_validated_count", datasets.findTerminologies().count(Dataset.Status.VALIDATED));
					data.setContent("status_validated_count", datasets.count(Dataset.Status.VALIDATED));
					data.setContent("status_genealogy_resubmitted_count", datasets.findGenealogies().count(Dataset.Status.RESUBMITTED));
					data.setContent("status_terminology_resubmitted_count", datasets.findTerminologies().count(Dataset.Status.RESUBMITTED));
					data.setContent("status_resubmitted_count", datasets.count(Dataset.Status.RESUBMITTED));
					data.setContent("status_genealogy_removed_count", datasets.findGenealogies().count(Dataset.Status.REMOVED));
					data.setContent("status_terminology_removed_count", datasets.findTerminologies().count(Dataset.Status.REMOVED));
					data.setContent("status_removed_count", datasets.count(Dataset.Status.REMOVED));
					data.setContent("status_genealogy_all_count", datasets.findGenealogies().size());
					data.setContent("status_terminology_all_count", datasets.findTerminologies().size());
					data.setContent("status_all_count", datasets.size());
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(userAccount.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
