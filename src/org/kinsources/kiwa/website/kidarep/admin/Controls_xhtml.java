/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.admin;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.kidarep.Kidarep;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.ControlReporter.ControlType;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Controls_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Controls_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/admin/controls.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.admin.controls", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters
				// ==============

				// Use parameters
				// ==============
				Kidarep kidarep = kiwa.kidarep();

				//
				TagDataManager data = new TagDataManager();

				//
				Datasets datasets = kidarep.datasets().findGenealogies().findWithKinshipFile();
				{
					data.setContent("summary_count", datasets.size());
					int controlTypeIndex = 0;
					for (ControlType controlType : ControlReporter.ControlType.values())
					{
						int naCount = 0;
						int zeroCount = 0;
						int gtCount = 0;

						for (Dataset dataset : datasets)
						{
							String value = dataset.getOriginFile().statistics().getStringValue(controlType.name());

							if (value == null)
							{
								naCount += 1;
							}
							else if (value.equals("0"))
							{
								zeroCount += 1;
							}
							else
							{
								gtCount += 1;
							}
						}

						data.setContent("summary_row", controlTypeIndex, "summary_type", controlType.name());
						data.setContent("summary_row", controlTypeIndex, "summary_na", naCount);
						data.setContent("summary_row", controlTypeIndex, "summary_zero", zeroCount);
						data.setContent("summary_row", controlTypeIndex, "summary_gt", gtCount);

						if (naCount > 0)
						{
							data.setAttribute("summary_row", controlTypeIndex, "summary_na", "class", "td_number danger");
						}
						else
						{
							data.setAttribute("summary_row", controlTypeIndex, "summary_na", "class", "td_number success");
						}

						if ((controlType == ControlType.AUTO_MARRIAGE) || ((controlType == ControlType.CYCLIC_DESCENT_CASES)))
						{
							data.setAttribute("summary_row", controlTypeIndex, "summary_gt", "style", "border: 2px solid red;");

							if (gtCount == 0)
							{
								data.setAttribute("summary_row", controlTypeIndex, "summary_gt", "class", "td_number success");
							}
							else
							{
								data.setAttribute("summary_row", controlTypeIndex, "summary_gt", "class", "td_number danger");
							}
						}
						else
						{
							if (gtCount == 0)
							{
								data.setAttribute("summary_row", controlTypeIndex, "summary_gt", "class", "td_number success");
							}
							else
							{
								data.setAttribute("summary_row", controlTypeIndex, "summary_gt", "class", "td_number warning");
							}
						}

						controlTypeIndex += 1;
					}
				}

				//
				{
					data.setContent("detail_count", datasets.size());

					{
						int controlTypeIndex = 0;
						for (ControlType controlType : ControlReporter.ControlType.values())
						{
							data.setContent("detail_header", controlTypeIndex, controlType.name().replace("_", " "));
							controlTypeIndex += 1;
						}
					}

					int datasetIndex = 0;
					for (Dataset dataset : datasets)
					{
						StringList buffer = new StringList();
						buffer.append("<td>").append(dataset.getId()).appendln("</td>");
						buffer.append("<td><a href=\"/kidarep/dataset-").append(dataset.getId()).append(".xhtml\" >").append(dataset.getName()).appendln("</a></td>");

						if (dataset.isGenealogy())
						{
							buffer.appendln("<td class=\"center\" sorttable_customkey=\"Genealogy\"><img src=\"/charter/commons/dataset-genealogy.png\" title=\"Genealogy\" alt=\"Genealogy Type\" /></td>");
						}
						else
						{
							buffer.appendln("<td class=\"center\" sorttable_customkey=\"Terminology\"><img src=\"/charter/commons/dataset-terminology.png\" title=\"Terminology\" alt=\"Terminolgy Type\" /></td>");
						}

						switch (dataset.getStatus())
						{
							case CREATED:
								buffer.appendln("<td class=\"center\" sorttable_customkey=\"Created\"><img src=\"/charter/commons/created.png\" title=\"Created\" alt=\"Created Status\" /></td>");
							break;
							case SUBMITTED:
								buffer.appendln("<td class=\"center\" sorttable_customkey=\"Submitted\"><img src=\"/charter/commons/submitted.png\" title=\"Submitted\" alt=\"Submitted Status\" /></td>");
							break;
							case VALIDATED:
								buffer.appendln("<td class=\"center\" sorttable_customkey=\"Validated\"><img src=\"/charter/commons/validated.png\" title=\"Validated\" alt=\"Validated Status\" /></td>");
							break;
							case REMOVED:
								buffer.appendln("<td class=\"center\" sorttable_customkey=\"Removd\"><img src=\"/charter/commons/removed.png\" title=\"Removed\" alt=\"Removed Status\" /></td>");
							break;
							default:
								buffer.appendln("<td class=\"center\">sorttable_customkey=\"Unknown\" />?</td>");
						}

						for (ControlType controlType : ControlReporter.ControlType.values())
						{
							String value = dataset.getOriginFile().statistics().getStringValue(controlType.name());
							if (value == null)
							{
								buffer.append("<td class=\"td_number danger\">").appendln("n/a").appendln("</td>");
							}
							else if (value.equals("0"))
							{
								buffer.append("<td class=\"td_number success\">").appendln("0").appendln("</td>");
							}
							else
							{
								if ((controlType == ControlType.AUTO_MARRIAGE) || ((controlType == ControlType.CYCLIC_DESCENT_CASES)))
								{
									buffer.append("<td class=\"td_number danger\">").appendln(value).appendln("</td>");
								}
								else
								{
									buffer.append("<td class=\"td_number warning\">").appendln(value).appendln("</td>");
								}
							}
						}

						data.setContent("detail_row", datasetIndex, buffer.toString());

						datasetIndex += 1;
					}
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(userAccount.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
