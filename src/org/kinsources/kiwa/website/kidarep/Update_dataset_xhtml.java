/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Update_dataset_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Update_dataset_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.update_dataset", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			//
			if (account == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				// Get parameters.
				// ===============
				String datasetId = request.getParameter("dataset_id");
				logger.info("datasetId=[{}]", datasetId);

				String name = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("name")));
				logger.info("name=[{}]", name);

				String atlasCode = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("atlas_code")));
				logger.info("atlasCode=[{}]", atlasCode);

				String author = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("author")));
				logger.info("author=[{}]", author);

				String bibliography = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("bibliography")));
				logger.info("bibliography=[{}]", bibliography);

				String citation = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("citation")));
				logger.info("citation=[{}]", citation);

				String coder = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("coder")));
				logger.info("coder=[{}]", coder);

				String collectionNotes = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("collection_notes")));
				logger.info("collectionNotes=[{}]", collectionNotes);

				String contact = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("contact")));
				logger.info("contact=[{}]", contact);

				String continent = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("continent")));
				logger.info("continent=[{}]", continent);

				String country = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("country")));
				logger.info("country=[{}]", country);

				String coverage = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("coverage")));
				logger.info("covereage=[{}]", coverage);

				String description = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("description")));
				logger.info("description=[{}]", description);

				String ethnicOrCulturalGroup = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("ethnic_or_cultural_group")));
				logger.info("ethnicOrCulturalGroup=[{}]", ethnicOrCulturalGroup);

				String geographicCoordinate = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("geographic_coordinate")));
				logger.info("geographicCoordinate=[{}]", geographicCoordinate);

				String history = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("history")));
				logger.info("history=[{}]", history);

				String languageGroup = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("language_group")));
				logger.info("languageGroup=[{}]", languageGroup);

				String licenseHelper = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("license_helper")));
				logger.info("licenseHelper=[{}]", licenseHelper);

				String license = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("license")));
				logger.info("license=[{}]", license);

				String location = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("location")));
				logger.info("location=[{}]", location);

				String otherRepositories = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("other_repositories")));
				logger.info("otherRepositories=[{}]", otherRepositories);

				String period = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("period")));
				logger.info("period=[{}]", period);

				String periodNote = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("period_note")));
				logger.info("periodNote=[{}]", periodNote);

				String radiusFromCenter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("radius_from_center")));
				logger.info("radiusFromCenter=[{}]", radiusFromCenter);

				String reference = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("reference")));
				logger.info("reference=[{}]", reference);

				String region = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("region")));
				logger.info("region=[{}]", region);

				String shortDescription = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("short_description")));
				logger.info("shortDescription=[{}]", shortDescription);

				String summary = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("summary")));
				logger.info("summary=[{}]", summary);

				// Use parameters.
				// ===============
				Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

				//
				if (StringUtils.isBlank(name))
				{
					throw new IllegalArgumentException("Name is blank.");
				}
				else if ((radiusFromCenter != null) && (!NumberUtils.isNumber(radiusFromCenter)))
				{
					throw new IllegalArgumentException("Radius form center is not a number.");
				}
				else if (dataset == null)
				{
					throw new IllegalArgumentException("Unknown dataset.");
				}
				else if ((!dataset.isContributor(account)) && ((dataset.getStatus() != Dataset.Status.SUBMITTED) || (!kiwa.isSciboarder(account))) && (!kiwa.isWebmaster(account)))
				{
					throw new IllegalArgumentException("Permission denied.");
				}
				else if ((!StringUtils.equals(name, dataset.getName())) && (kiwa.kidarep().isNotAvailableDatasetName(name, dataset)))
				{
					throw new IllegalArgumentException("The technical name generated from this new name is already in use.");
				}
				else
				{
					//
					logger.info("UPDATE DATASET [{}][{}]", datasetId, name);
					kiwa.log(Level.INFO, "events.update_dataset", "[accountId={}][datasetId={}][name={}]", account.getId(), dataset.getId(), name);

					//
					dataset.setAtlasCode(atlasCode);
					dataset.setAuthor(author);
					dataset.setBibliography(bibliography);
					dataset.setCitation(citation);
					dataset.setCoder(coder);
					dataset.setCollectionNotes(collectionNotes);
					dataset.setContact(contact);
					dataset.setContinent(continent);
					dataset.setCountry(country);
					dataset.setCoverage(coverage);
					dataset.setDescription(description);
					dataset.setEthnicOrCulturalGroup(ethnicOrCulturalGroup);
					dataset.setGeographicCoordinate(geographicCoordinate);
					dataset.setHistory(history);
					dataset.setLanguageGroup(languageGroup);
					if (StringUtils.isBlank(license))
					{
						license = licenseHelper;
					}
					dataset.setLicense(license);
					dataset.setLocation(location);
					dataset.setName(name);
					dataset.setOtherRepositories(otherRepositories);
					dataset.setPeriod(period);
					dataset.setPeriodNote(periodNote);
					if (radiusFromCenter == null)
					{
						dataset.setRadiusFromCenter(null);
					}
					else
					{
						dataset.setRadiusFromCenter(Long.parseLong(radiusFromCenter));
					}
					dataset.setReference(reference);
					dataset.setRegion(region);
					if (shortDescription == null)
					{
						dataset.setShortDescription(null);
					}
					else
					{
						dataset.setShortDescription(shortDescription.replace("\n", " "));
					}

					//
					kiwa.updateDataset(dataset);

					// Add notification in request comments.
					kiwa.addCommentFromAutomaticNotification(dataset, account.getId(), "Metadata updated");

					// Send response.
					// ==============
					Redirector.redirect(response, kiwa.permanentURI(dataset));
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your dataset form is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + exception.getMessage() + "</pre>");

			ErrorView.show(request, response, "Dataset update error", message);
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
