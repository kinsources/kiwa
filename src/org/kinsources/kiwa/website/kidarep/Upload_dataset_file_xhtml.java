/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.exceptions.KiwaBadFileFormatException;
import org.kinsources.kiwa.kernel.exceptions.KiwaControlException;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.FormatIssue;
import org.kinsources.kiwa.kidarep.LoadedFile;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.tip.puck.net.workers.ControlReporter.ControlType;

import fr.devinsy.kiss4web.FileItemHelper;
import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Upload_dataset_file_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Upload_dataset_file_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.upload_dataset_file", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			//
			if (account == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				// Get parameters.
				// ===============
				ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
				upload.setSizeMax(80000000);
				List<FileItem> /* FileItem */items = upload.parseRequest(request);

				//
				String datasetId = FileItemHelper.getItemValue(items, "dataset_id");
				logger.info("datasetId=[{}]", datasetId);

				//
				String femaleFathersOrMaleMothers = FileItemHelper.getItemValue(items, "female_fathers_or_male_mothers");
				logger.info("femaleFathersOrMaleMothers=[{}]", femaleFathersOrMaleMothers);

				String multipleFathersOrMothers = FileItemHelper.getItemValue(items, "multiple_fathers_or_mothers");
				logger.info("multipleFathersOrMothers=[{}]", multipleFathersOrMothers);

				String namelessPersons = FileItemHelper.getItemValue(items, "nameless_persons");
				logger.info("namelessPersons=[{}]", namelessPersons);

				String parentChildMarriages = FileItemHelper.getItemValue(items, "parent_child_marriages");
				logger.info("parentChildMarriages=[{}]", parentChildMarriages);

				String personsOfUnknownSex = FileItemHelper.getItemValue(items, "persons_of_unknown_sex");
				logger.info("personsOfUnknownSex=[{}]", personsOfUnknownSex);

				String sameSexSpouses = FileItemHelper.getItemValue(items, "same_sex_spouses");
				logger.info("sameSexSpouses=[{}]", sameSexSpouses);

				//
				FileItem fileSource = FileItemHelper.getItem(items, "new_attachment");
				if (fileSource == null)
				{
					logger.info("fileSource=[null]");
				}
				else
				{
					logger.info("fileSource=[{}]", fileSource.getName());
				}

				// Use parameters.
				// ===============
				Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

				// Check MSIE upload name format.
				String uploadFileName;
				if (fileSource == null)
				{
					uploadFileName = null;
				}
				else if (fileSource.getName().indexOf("\\") != -1)
				{
					uploadFileName = fileSource.getName().substring(fileSource.getName().lastIndexOf("\\") + 1);
				}
				else
				{
					uploadFileName = fileSource.getName().toLowerCase();
				}

				//
				if (fileSource == null)
				{
					throw new IllegalArgumentException("Invalid upload.");
				}
				else if (dataset == null)
				{
					throw new IllegalArgumentException("Unknown dataset.");
				}
				else if (dataset.getStatus() != Dataset.Status.CREATED)
				{
					throw new IllegalArgumentException("Invalid dataset status.");
				}
				else if ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)))
				{
					throw new IllegalArgumentException("Permission denied.");
				}
				else if ((StringUtils.isBlank(uploadFileName)) || (fileSource.getSize() == 0))
				{
					throw new IllegalArgumentException("Bad file.");
				}
				else if ((dataset.isGenealogy()) && (!StringUtils.endsWithAny(uploadFileName.toLowerCase(), ".ged", ".ods", ".paj", ".pl", ".puc", ".tip", ".txt", ".xls")))
				{
					//
					LoadedFile originFile = new LoadedFile(uploadFileName, fileSource.get());

					//
					FormatIssue formatIssue = kiwa.createFormatIssue(account.getId(), dataset.getId(), originFile);

					//
					Redirector.redirect(response, "/kidarep/format_issue_dialog.xhtml?format_issue_id=" + formatIssue.getId());
				}
				else if ((dataset.isTerminology()) && (!StringUtils.endsWithAny(uploadFileName.toLowerCase(), ".term.iur.txt", ".term.iur.ods", ".term.iur.xls", ".term.reg.txt")))
				{
					//
					LoadedFile originFile = new LoadedFile(uploadFileName, fileSource.get());

					//
					FormatIssue formatIssue = kiwa.createFormatIssue(account.getId(), dataset.getId(), originFile);

					//
					Redirector.redirect(response, "/kidarep/format_issue_dialog.xhtml?format_issue_id=" + formatIssue.getId());

				}
				else
				{
					//
					logger.info("UPLOAD DATASET FILE [datasetId={}][name={}][size={}]", datasetId, uploadFileName, fileSource.getSize());
					kiwa.log(Level.INFO, "events.upload_dataset_file", "[accountId={}][datasetId={}][name={}][size={}]", account.getId(), dataset.getId(), uploadFileName, fileSource.getSize());

					//
					LoadedFile originFile = new LoadedFile(uploadFileName, fileSource.get());

					//
					try
					{
						if (dataset.isGenealogy())
						{
							//
							List<ControlType> controls = new ArrayList<ControlType>(10);
							controls.add(ControlType.AUTO_MARRIAGE);
							controls.add(ControlType.CYCLIC_DESCENT_CASES);

							if (femaleFathersOrMaleMothers != null)
							{
								controls.add(ControlType.FEMALE_FATHERS_OR_MALE_MOTHERS);
							}

							if (multipleFathersOrMothers != null)
							{
								controls.add(ControlType.MULTIPLE_FATHERS_OR_MOTHERS);
							}

							if (namelessPersons != null)
							{
								controls.add(ControlType.NAMELESS_PERSONS);
							}

							if (parentChildMarriages != null)
							{
								controls.add(ControlType.PARENT_CHILD_MARRIAGES);
							}

							if (personsOfUnknownSex != null)
							{
								controls.add(ControlType.UNKNOWN_SEX_PERSONS);
							}

							if (sameSexSpouses != null)
							{
								controls.add(ControlType.SAME_SEX_SPOUSES);
							}

							//
							kiwa.addDatasetGenealogyFile(dataset, originFile, controls, locale);
						}
						else if (dataset.isTerminology())
						{
							//
							List<ControlType> controls = new ArrayList<ControlType>(10);
							controls.add(ControlType.AUTO_MARRIAGE);
							controls.add(ControlType.CYCLIC_DESCENT_CASES);

							if (femaleFathersOrMaleMothers != null)
							{
								controls.add(ControlType.FEMALE_FATHERS_OR_MALE_MOTHERS);
							}

							if (multipleFathersOrMothers != null)
							{
								controls.add(ControlType.MULTIPLE_FATHERS_OR_MOTHERS);
							}

							if (namelessPersons != null)
							{
								controls.add(ControlType.NAMELESS_PERSONS);
							}

							if (parentChildMarriages != null)
							{
								controls.add(ControlType.PARENT_CHILD_MARRIAGES);
							}

							if (personsOfUnknownSex != null)
							{
								controls.add(ControlType.UNKNOWN_SEX_PERSONS);
							}

							if (sameSexSpouses != null)
							{
								controls.add(ControlType.SAME_SEX_SPOUSES);
							}

							//
							kiwa.addDatasetGenealogyFile(dataset, originFile, controls, locale);
						}

						//
						Redirector.redirect(response, kiwa.permanentURI(dataset) + "#dataset_file_section");

					}
					catch (KiwaBadFileFormatException exception)
					{
						//
						FormatIssue formatIssue = kiwa.createFormatIssue(account.getId(), dataset.getId(), originFile);

						//
						Redirector.redirect(response, "/kidarep/format_issue_dialog.xhtml?format_issue_id=" + formatIssue.getId());

					}
					catch (KiwaControlException exception)
					{
						//
						StringList message = new StringList();

						message.appendln("<p>The file does not pass the controls.</p>");
						message.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");

						ErrorView.show(request, response, "Dataset file upload", message);

					}
					catch (Exception exception)
					{
						//
						FormatIssue formatIssue = kiwa.createFormatIssue(account.getId(), dataset.getId(), originFile);

						//
						Redirector.redirect(response, "/kidarep/format_issue_dialog.xhtml?format_issue_id=" + formatIssue.getId());

					}

				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your Dataset file upload is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");

			ErrorView.show(request, response, "Dataset file upload", message);

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
