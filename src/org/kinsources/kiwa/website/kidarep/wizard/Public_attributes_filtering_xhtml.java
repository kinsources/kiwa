/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.wizard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.kinsources.kiwa.website.kidarep.DatasetFiltersView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Public_attributes_filtering_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Public_attributes_filtering_xhtml.class);
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/wizard/public_attributes_filtering.html");
	private static final Kiwa kiwa = Kiwa.instance();
	private static WizardView wizardView = new WizardView();
	private static DatasetFiltersView datasetFiltersView = new DatasetFiltersView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.my_space.wizard.attribute_filters_edition", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

			// Send response.
			// ==============
			if (account == null)
			{
				Redirector.redirect(response, request.getContextPath() + "/accounts/login.xhtml");
			}
			else if (dataset == null)
			{
				throw new IllegalArgumentException("Undefined dataset.");
			}
			else if (dataset.getStatus() != Dataset.Status.CREATED)
			{
				throw new IllegalArgumentException("Bad dataset status.");
			}
			else if ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setAttribute("dataset_id", "value", dataset.getId());

				//
				if (dataset.isGenealogy())
				{
					data.setContent("lead_paragraph", kiwa.hico().getText("Dataset Publication Wizard > Step 5", locale));

					if (dataset.getOriginFile() == null)
					{
						data.setContent("data_filtering", "You have not yet upload a dataset file. So nothing to filter.");
					}
					else
					{
						data.setContent("data_filtering", datasetFiltersView.getHtmlBody(dataset.getOriginFile(), dataset.publicAttributeFilters(), locale));
					}
				}
				else
				{
					data.setContent("data_filtering", "<h1>Attribute filters edition</h1><p>No filter required for terminology.</p>");
				}

				//
				data.setAttribute("previous_button", "href", "attachment_upload.xhtml?dataset_id=" + dataset.getId());
				data.setAttribute("next_button", "href", "dataset_submit.xhtml?dataset_id=" + dataset.getId());

				//
				StringBuffer content = view.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, wizardView.getHtml(WizardView.Menu.ATTRIBUTE_FILTERS, content, locale, dataset));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
