/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.wizard;

import java.util.Locale;

import org.kinsources.kiwa.kidarep.Dataset;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class WizardView
{
	public enum Menu
	{
		INTRODUCTION,
		DATASET_CREATION,
		METADATA_EDITION,
		FILE_UPLOAD,
		ATTACHMENT_UPLOAD,
		ATTRIBUTE_FILTERS,
		DATASET_SUBMIT,
		CONCLUSION
	}

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WizardView.class);

	private static TranslatorPresenter page = new TranslatorPresenter("/org/kinsources/kiwa/website/kidarep/wizard/wizard_view.html");
	private static final String STATUS_COMPLETED = "Completed";
	private static final String STATUS_COGS = "Improvable";

	/**
	 * 
	 */
	public StringBuffer getHtml(final Menu menu, final CharSequence content, final Locale locale, final Dataset dataset) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		data.setContent("tools", "Tools");

		//
		switch (menu)
		{
			case INTRODUCTION:
				data.setAttribute("step0", "class", "current_step");
			break;

			case DATASET_CREATION:
				data.setAttribute("step1", "class", "current_step");
			break;

			case METADATA_EDITION:
				data.setAttribute("step2", "class", "current_step");
			break;

			case FILE_UPLOAD:
				data.setAttribute("step3", "class", "current_step");
			break;

			case ATTACHMENT_UPLOAD:
				data.setAttribute("step4", "class", "current_step");
			break;

			case ATTRIBUTE_FILTERS:
				data.setAttribute("step5", "class", "current_step");
			break;

			case DATASET_SUBMIT:
				data.setAttribute("step6", "class", "current_step");
			break;

			case CONCLUSION:
				data.setAttribute("step7", "class", "current_step");
			break;
		}

		//
		if (dataset == null)
		{
			//
			data.setAttribute("title_details", "style", "display: none");

			//
			data.setAttribute("step0_status", "src", "/charter/commons/checked.png");
			data.setAttribute("step0_status", "title", STATUS_COMPLETED);
			data.setAttribute("step1_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step1_status", "title", STATUS_COGS);
			data.setAttribute("step2_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step2_status", "title", STATUS_COGS);
			data.setAttribute("step3_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step3_status", "title", STATUS_COGS);
			data.setAttribute("step4_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step4_status", "title", STATUS_COGS);
			data.setAttribute("step5_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step5_status", "title", STATUS_COGS);
			data.setAttribute("step6_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step6_status", "title", STATUS_COGS);
			data.setAttribute("step7_status", "src", "/charter/commons/cogs.png");
			data.setAttribute("step7_status", "title", STATUS_COGS);
		}
		else
		{
			//
			data.setContent("title_dataset_name", XMLTools.escapeXmlBlank(dataset.getName()));
			data.setContent("title_dataset_id", dataset.getId());

			if (dataset.isTerminology())
			{
				data.setContent("dataset_type_file", "Terminology");
			}

			//
			if (menu == Menu.CONCLUSION)
			{
				//
				data.setAttribute("step1_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step1_status", "title", STATUS_COMPLETED);
				data.setAttribute("step2_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step2_status", "title", STATUS_COMPLETED);
				data.setAttribute("step3_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step3_status", "title", STATUS_COMPLETED);
				data.setAttribute("step4_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step4_status", "title", STATUS_COMPLETED);
				data.setAttribute("step5_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step5_status", "title", STATUS_COMPLETED);
				data.setAttribute("step6_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step6_status", "title", STATUS_COMPLETED);
				data.setAttribute("step7_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step7_status", "title", STATUS_COMPLETED);
			}
			else
			{
				//
				data.setAttribute("step0_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step0_status", "title", STATUS_COMPLETED);
				data.setAttribute("step1_status", "src", "/charter/commons/checked.png");
				data.setAttribute("step1_status", "title", STATUS_COMPLETED);

				data.setAttribute("step2_status", "src", "/charter/commons/cogs.png");
				data.setAttribute("step2_status", "title", STATUS_COGS);

				if (dataset.getOriginFile() == null)
				{
					data.setAttribute("step3_status", "src", "/charter/commons/exclamation_triangle.png");
					data.setAttribute("step3_status", "title", "Dataset file is required");
				}
				else
				{
					data.setAttribute("step3_status", "src", "/charter/commons/checked.png");
					data.setAttribute("step3_status", "title", STATUS_COMPLETED);
				}

				if (dataset.countOfAttachmentFiles() == 0)
				{
					data.setAttribute("step4_status", "src", "/charter/commons/cogs.png");
					data.setAttribute("step4_status", "title", STATUS_COGS);
				}
				else
				{
					data.setAttribute("step4_status", "src", "/charter/commons/checked.png");
					data.setAttribute("step4_status", "title", STATUS_COMPLETED);
				}

				if (dataset.getOriginFile() == null)
				{
					data.setAttribute("step5_status", "src", "/charter/commons/ban.png");
					data.setAttribute("step5_status", "title", "Undefined dataset file");
				}
				else if (dataset.publicAttributeFilters().isEmpty())
				{
					data.setAttribute("step5_status", "src", "/charter/commons/exclamation_triangle.png");
					data.setAttribute("step5_status", "title", "To complete");

				}
				else
				{
					data.setAttribute("step5_status", "src", "/charter/commons/checked.png");
					data.setAttribute("step5_status", "title", STATUS_COMPLETED);
				}

				if ((dataset.getStatus() == Dataset.Status.CREATED))
				{
					data.setAttribute("step6_status", "src", "/charter/commons/exclamation_triangle.png");
					data.setAttribute("step6_status", "title", "Mandatory step");
				}
				else
				{
					data.setAttribute("step6_status", "src", "/charter/commons/checked.png");
					data.setAttribute("step6_status", "title", STATUS_COMPLETED);
				}

				//
				data.setAttribute("step7_status", "src", "/charter/commons/cogs.png");
				data.setAttribute("step7_status", "title", "Mandatory step");
			}
		}

		//
		data.setContent("wizard_content_zone", XidynUtils.extractBodyContent(content));

		//
		result = page.dynamize(data);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
