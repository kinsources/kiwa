/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeFilters;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Update_collaborator_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -6435123109307074502L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Update_collaborator_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			// Get parameters.
			// ===============
			kiwa.logPageHit("pages.kidarep.update_collaborator", request);
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			//
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			String collaboratorId = request.getParameter("collaborator_id");
			logger.info("collaboratorId=[{}]", collaboratorId);

			String description = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("description")));
			logger.info("description=[{}]", description);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);
			Account collaboratorAccount = kiwa.accountManager().getAccountById(collaboratorId);

			if (dataset == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else if (collaboratorAccount == null)
			{
				throw new IllegalArgumentException("Incorrect collaborator");
			}
			else if ((dataset.getStatus() != Dataset.Status.VALIDATED) || ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account))))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else if (!NumberUtils.isDigits(collaboratorId))
			{
				throw new IllegalArgumentException("Invalid collaborator id.");
			}
			else
			{
				//
				logger.info("UPDATE COLLABORATOR [accountId={}][datasetId={}][collaboratorId={}][", account.getId(), datasetId, collaboratorId);
				kiwa.log(Level.INFO, "events.update_collaborator", "[accountId={}][datasetId={}][collaboratorId={}][", account.getId(), datasetId, collaboratorId);

				//
				AttributeFilters filters;
				if (dataset.getOriginFile() != null)
				{
					AttributeDescriptors descriptors = dataset.getOriginFile().statistics().attributeDescriptors();
					filters = DatasetFiltersView.getCriteria(request, descriptors);

				}
				else
				{
					filters = null;
				}

				//
				kiwa.updateCollaborator(dataset, Long.parseLong(collaboratorId), description, filters);

				//
				Redirector.redirect(response, kiwa.permanentURI(dataset) + "#collaborators_section");
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your collaborator form is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + exception.getMessage() + "</pre>");

			ErrorView.show(request, response, "Collaborator creation error", message, "/kidarep/dataset_upload.xhtml");

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
