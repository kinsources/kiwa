/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Contributors;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Dataset_transfer_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Dataset_transfer_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/dataset_transfer.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.dataset_transfer", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============
			String datasetIdParameter = request.getParameter("dataset_id");
			logger.info("datasetIdParameter=[{}]", datasetIdParameter);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetIdParameter);

			if (dataset == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else
			{
				TagDataManager data = new TagDataManager();

				if ((dataset.getStatus() != Dataset.Status.VALIDATED) || ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account))))
				{
					throw new IllegalAccessException("You are not able to transfer this dataset.");
				}
				else
				{
					//
					data.setContent("dataset_name", XMLTools.escapeXmlBlank(dataset.getName()));
					data.setContent("dataset_id", dataset.getId());

					//
					Contributors contributors = kiwa.kidarep().contributors().findContributors(Account.Status.ACTIVATED);
					contributors.remove(dataset.getContributor());

					//
					int contributorIndex = 0;
					for (Contributor contributor : contributors.sortByFullName())
					{
						//
						data.setContent("contributor_option", contributorIndex, XMLTools.escapeXmlBlank(contributor.getFullName()));
						data.setAttribute("contributor_option", contributorIndex, "value", contributor.getId());

						//
						contributorIndex += 1;
					}

					data.setContent("button_cancel", "Cancel");
					data.setAttribute("button_cancel", "href", kiwa.permanentURI(dataset));

					data.setAttribute("transfer_dataset_id", "value", dataset.getId());
					data.setAttribute("button_transfer", "value", "Transfer");
				}

				// Send response.
				// ==============
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer page = kiwa.getCharterView().getHtml(account.getId(), locale, content);

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(page);
			}

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
