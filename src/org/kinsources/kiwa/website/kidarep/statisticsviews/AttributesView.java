/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.statisticsviews;

import java.util.Locale;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.Statistics;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;

import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class AttributesView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AttributesView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/statisticsviews/attributes_view.html");

	/**
	 * 
	 */
	public AttributesView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		// Attributes
		// ==========
		DatasetFile sourceFile = kiwa.getDatasetFile(dataset, account);

		if ((sourceFile == null) || (sourceFile.statistics().isEmpty()))
		{
			data.setContent("attributes_section", "No attributes.");
		}
		else
		{
			//
			Statistics statistics = sourceFile.statistics();

			//
			if (statistics.attributeDescriptors().isEmpty())
			{
				data.setContent("attributes_section", "No attributes.");
			}
			else
			{
				// General information.
				{
					//
					int index = 0;

					//
					AttributeDescriptors descriptors = statistics.attributeDescriptors().findByScope(AttributeDescriptor.Scope.CORPUS);

					if (descriptors.isNotEmpty())
					{
						//
						data.setContent("attributes_relation_line", index, "attributes_relation_label", "Corpus");

						//
						data.setContent("attributes_relation_line", index, "attributes_relation_count", descriptors.size());

						//
						StringList attributeList = new StringList(descriptors.size());
						attributeList.addAll(descriptors.labelsSorted());
						String attributes = attributeList.toStringWithFrenchCommas();
						data.setContent("attributes_relation_line", index, "attributes_relation_list", XMLTools.escapeXmlBlank(attributes));

						//
						index += 1;
					}

					//
					descriptors = statistics.attributeDescriptors().findByScope(AttributeDescriptor.Scope.INDIVIDUALS);

					if (descriptors.isNotEmpty())
					{
						//
						data.setContent("attributes_relation_line", index, "attributes_relation_label", "Individuals");

						//
						data.setContent("attributes_relation_line", index, "attributes_relation_count", descriptors.size());

						//
						StringList attributeList = new StringList(descriptors.size());
						attributeList.addAll(descriptors.labelsSorted());
						String attributes = attributeList.toStringWithFrenchCommas();
						data.setContent("attributes_relation_line", index, "attributes_relation_list", XMLTools.escapeXmlBlank(attributes));

						//
						index += 1;
					}

					//
					descriptors = statistics.attributeDescriptors().findByScope(AttributeDescriptor.Scope.FAMILIES);

					if (descriptors.isNotEmpty())
					{
						//
						data.setContent("attributes_relation_line", index, "attributes_relation_label", "Families");

						//
						data.setContent("attributes_relation_line", index, "attributes_relation_count", descriptors.size());

						//
						StringList attributeList = new StringList(descriptors.size());
						attributeList.addAll(descriptors.labelsSorted());
						String attributes = attributeList.toStringWithFrenchCommas();
						data.setContent("attributes_relation_line", index, "attributes_relation_list", XMLTools.escapeXmlBlank(attributes));

						//
						index += 1;
					}

					//
					for (String name : statistics.attributeDescriptors().relationModelNames())
					{
						//
						descriptors = statistics.attributeDescriptors().findByRelationModelName(name);

						//
						if (descriptors.isNotEmpty())
						{
							//
							data.setContent("attributes_relation_line", index, "attributes_relation_label", name);

							//
							data.setContent("attributes_relation_line", index, "attributes_relation_count", descriptors.size());

							//
							StringList attributeList = new StringList(descriptors.size());
							attributeList.addAll(descriptors.labelsSorted());
							String attributes = attributeList.toStringWithFrenchCommas();
							data.setContent("attributes_relation_line", index, "attributes_relation_list", XMLTools.escapeXmlBlank(attributes));

							//
							index += 1;
						}
					}

					//
					descriptors = statistics.attributeDescriptors().findByScope(AttributeDescriptor.Scope.ACTORS);

					if (descriptors.isNotEmpty())
					{
						//
						data.setContent("attributes_relation_line", index, "attributes_relation_label", "Actors");

						//
						data.setContent("attributes_relation_line", index, "attributes_relation_count", descriptors.size());

						//
						StringList attributeList = new StringList(descriptors.size());
						attributeList.addAll(descriptors.labelsSorted());
						String attributes = attributeList.toStringWithFrenchCommas();
						data.setContent("attributes_relation_line", index, "attributes_relation_list", XMLTools.escapeXmlBlank(attributes));

						//
						index += 1;
					}
				}

				// Detailed information.
				{
					//
					int index = 0;
					for (AttributeDescriptor descriptor : statistics.attributeDescriptors())
					{
						//
						data.setContent("attributes_detail_line", index, "attributes_detail_scope", XMLTools.escapeXmlBlank(descriptor.getRelationName()));
						data.setContent("attributes_detail_line", index, "attributes_detail_label", XMLTools.escapeXmlBlank(descriptor.getLabel()));
						data.setContent("attributes_detail_line", index, "attributes_detail_notset", String.valueOf(descriptor.getCountOfNotSet()));
						data.setContent("attributes_detail_line", index, "attributes_detail_notset_coverage", String.format("%.2f", descriptor.getCovegareOfNotSet()) + "&#160;%");
						data.setContent("attributes_detail_line", index, "attributes_detail_setblank", String.valueOf(descriptor.getCountOfBlank()));
						data.setContent("attributes_detail_line", index, "attributes_detail_setblank_coverage", String.format("%.2f", descriptor.getCoverageOfBlank()) + "&#160;%");
						data.setContent("attributes_detail_line", index, "attributes_detail_filled", String.valueOf(descriptor.getCountOfFilled()));
						data.setContent("attributes_detail_line", index, "attributes_detail_filled_coverage", String.format("%.2f", descriptor.getCoverageOfFilled()) + "&#160;%");
						data.setContent("attributes_detail_line", index, "attributes_detail_set", String.valueOf(descriptor.getCountOfSet()));
						data.setContent("attributes_detail_line", index, "attributes_detail_set_coverage", String.format("%.2f", descriptor.getCoverageOfSet()) + "&#160;%");
						data.setContent("attributes_detail_line", index, "attributes_detail_max", String.valueOf(descriptor.getMax()));

						//
						index += 1;
					}
				}
			}
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(dataset, account, locale));

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
