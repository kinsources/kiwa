/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.statisticsviews;

import java.util.Locale;

import org.apache.commons.io.FilenameUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.LoadedFileHeader;
import org.kinsources.kiwa.kidarep.LoadedFileHeaders;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class GraphicsView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GraphicsView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/statisticsviews/graphics_view.html");

	/**
	 * 
	 */
	public GraphicsView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		// Graphics
		// ========
		DatasetFile datasetFile = kiwa.getDatasetFile(dataset, account);

		if (datasetFile == null)
		{
			data.setContent("graphics", "No graphic.");
			data.setAttribute("graphics", "class", "");
			data.setAttribute("svg_container", "class", "xid:nodisplay");
			data.setAttribute("graphics_sentence", "class", "xid:nodisplay");
		}
		else
		{
			LoadedFileHeaders graphics = datasetFile.statistics().graphics();

			if (graphics.isEmpty())
			{
				data.setContent("graphics", "For huge dataset, statistics and graphics are computed later. Please visit the dataset page later.");
				data.setAttribute("graphics", "class", "");
				data.setAttribute("svg_container", "class", "xid:nodisplay");
			}
			else
			{
				if (dataset.isGenealogy())
				{
					data.setAttribute("svg_container", "class", "xid:nodisplay");

					int graphicIndex = 0;
					for (LoadedFileHeader graphic : graphics)
					{
						//
						String title = FilenameUtils.getBaseName(graphic.name());
						data.setContent("graphic", graphicIndex, "graphic_title", title);
						data.setAttribute("graphic", graphicIndex, "graphic_image", "src",
								SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_graphic", String.valueOf(dataset.getId()), String.valueOf(graphic.id()), graphic.name()));
						data.setAttribute("graphic", graphicIndex, "graphic_image", "alt", title);
						data.setAttribute("graphic", graphicIndex, "graphic_image", "title", title);

						//
						graphicIndex += 1;
					}

					if (graphics.size() != 12)
					{
						data.setContent("graphics_sentence", "For huge dataset, statistics and graphics are computed later. Please visit the dataset page later.");
					}
					else
					{
						data.setAttribute("graphics_sentence", "class", "xid:nodisplay");
					}
				}
				else
				{
					data.setAttribute("graphics", "class", "xid:nodisplay");

					LoadedFileHeader graphic = graphics.getByIndex(0);
					data.setContent("svg_view_title", "Kinoath diagram");
					data.setAttribute("svg_viewer_link", "href", "/kidarep/statisticsviews/SVG_viewer.xhtml?dataset_id=" + dataset.getId());
					data.setAttribute("svg_view", "src",
							SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_graphic", String.valueOf(dataset.getId()), String.valueOf(graphic.id()), graphic.name()));
				}
			}
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(dataset, account, locale));

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
