/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.LoadedFileHeader;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;

/**
 *
 */
public class Remove_dataset_attachment_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Remove_dataset_attachment_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.remove_dataset_attachment", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			String attachmentId = request.getParameter("attachment_id");
			logger.info("attachmentId=[{}]", attachmentId);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);
			LoadedFileHeader attachment;
			if (dataset == null)
			{
				attachment = null;
			}
			else
			{
				attachment = dataset.attachments().getById(attachmentId);
			}

			//
			if (dataset == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else if (attachment == null)
			{
				throw new IllegalArgumentException("Unknown attachment.");
			}
			else if ((dataset.getStatus() != Dataset.Status.CREATED) && ((dataset.getStatus() != Dataset.Status.SUBMITTED)))
			{
				throw new IllegalArgumentException("Invalid operation for this status.");
			}
			else if ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				logger.info("REMOVE DATASET ATTACHMENT [accountId={}][datasetId={}][attachmentId={}]", account.getId(), dataset.getId(), attachment.id());
				kiwa.log(Level.INFO, "events.kidarep.remove_dataset_attachment", "[accountId={}][datasetId={}][attachmentId={}]", account.getId(), dataset.getId(), attachment.id());

				// Use parameters.
				// ===============
				kiwa.removeDatasetAttachment(dataset, attachment);

				// Add notification in request comments.
				String comment = String.format("Remove attachment file \"%s\"", attachment.name());
				kiwa.addCommentFromAutomaticNotification(dataset, account.getId(), comment);

				//
				Redirector.redirect(response, "/kidarep/dataset-" + dataset.getId() + ".xhtml");
			}
		}
		catch (final IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Dataset remove issue", exception.getMessage(), "/");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
