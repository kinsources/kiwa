/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Accounts;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Collaborator;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class Collaborator_creation_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -4976999017907629511L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Collaborator_creation_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/collaborator_creation.html");
	private static DatasetFiltersView datasetFiltersView = new DatasetFiltersView();
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.collaborator_creation", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

			if (dataset == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else if (account == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else if ((dataset.getStatus() != Dataset.Status.VALIDATED) || ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account))))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else if (!dataset.isGenealogy())
			{
				throw new IllegalArgumentException("Collaborator is available only with kinship dataset.");
			}
			else
			{
				//
				logger.info("COLLABORATOR CREATION[{}]", dataset.getId());
				kiwa.log(Level.INFO, "events.kidarep.collaborator_creation", "[datasetId={}][accountId={}]", dataset.getId(), account.getId());

				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("label_title", "Collaborator creation");
				data.setAttribute("dataset_id", "value", datasetId);
				data.setContent("dataset_name_label", "Dataset:");
				data.setContent("dataset_name", XMLTools.escapeXmlBlank(dataset.getName()) + " (ID " + dataset.getId() + ")");

				//
				data.setContent("account_label", "Collaborator:");
				Accounts availableAccounts = kiwa.accountManager().findAccountByStatus(Account.Status.ACTIVATED);
				availableAccounts.remove(account);
				for (Collaborator collaborator : dataset.collaborators())
				{
					availableAccounts.remove(collaborator.getAccount());
				}

				int index = 0;
				for (Account availableAccount : availableAccounts.sortByLastName())
				{
					//
					data.setContent("account_option", index, XMLTools.escapeXmlBlank(availableAccount.getFullName()));
					data.setAttribute("account_option", index, "value", availableAccount.getId());

					//
					index += 1;
				}

				data.setContent("description_label", "Description:");
				data.setContent("data_filtering_label", "Data filtering:");
				data.setContent("data_filtering", XidynUtils.extractBodyContent(datasetFiltersView.getHtml(dataset.getOriginFile(), null, locale)));
				data.setContent("cancel_button", "Cancel");
				data.setAttribute("cancel_button", "href", kiwa.permanentURI(dataset) + "#collaborators_section");
				data.setAttribute("submit_button", "value", "Create");

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(account.getId(), locale, content);

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
