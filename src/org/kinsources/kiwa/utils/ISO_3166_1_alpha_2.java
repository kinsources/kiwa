/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

import java.util.Properties;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class ISO_3166_1_alpha_2
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ISO_3166_1_alpha_2.class);
	private static final ISO_3166_1_alpha_2 instance = new ISO_3166_1_alpha_2();
	private String[] codes;
	private Properties bundle;

	/**
	 * 
	 */
	private ISO_3166_1_alpha_2()
	{
		try
		{
			//
			String source = XidynUtils.load(ISO_3166_1_alpha_2.class.getResource("ISO_3166-1-alpha-2.txt"));
			source = source.replaceAll("#.*\n", "");
			this.codes = source.split("\n");

			//
			this.bundle = new Properties();
			this.bundle.load(ISO_3166_1_alpha_2.class.getResourceAsStream("/org/kinsources/kiwa/utils/ISO_3166-1-alpha-2.properties"));

		}
		catch (Exception exception)
		{
			logger.error(ExceptionUtils.getStackTrace(exception));
			throw new IllegalArgumentException("ISO_3166-1-alpha-2 undefined.");
		}
	}

	/**
	 * 
	 */
	public static String[] codes()
	{
		String[] result;

		result = instance.codes.clone();

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static String getCode(final int index)
	{
		String result;

		result = instance.codes[index];

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static String getLabel(final int index)
	{
		String result;

		result = instance.bundle.getProperty(instance.codes[index]);

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static String getLabel(final String code)
	{
		String result;

		result = instance.bundle.getProperty(code);

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static int indexOf(final String code)
	{
		int result;

		result = ArrayUtils.indexOf(instance.codes, code);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static int size()
	{
		int result;

		result = instance.codes.length;

		//
		return result;
	}
}
