/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.kinsources.kiwa.utils.WeightedStringComparator.Criteria;

/**
 * The <code>Collections</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class WeightedStrings implements Iterable<WeightedString>
{
	private List<WeightedString> collections;

	/**
	 * 
	 */
	public WeightedStrings()
	{
		super();

		this.collections = new ArrayList<WeightedString>();
	}

	/**
	 * 
	 */
	public WeightedStrings(final int initialCapacity)
	{
		this.collections = new ArrayList<WeightedString>(initialCapacity);
	}

	/**
	 * 
	 */
	public WeightedString add(final WeightedString source)
	{
		WeightedString result;

		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.collections.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final WeightedStrings source)
	{
		if (source != null)
		{
			for (WeightedString collection : source)
			{
				if (!this.collections.contains(collection))
				{
					add(collection);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.collections.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public WeightedStrings copy()
	{
		WeightedStrings result;

		result = new WeightedStrings(this.collections.size());

		for (WeightedString account : this.collections)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public WeightedString getByIndex(final int index)
	{
		WeightedString result;

		result = this.collections.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.collections.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<WeightedString> iterator()
	{
		Iterator<WeightedString> result;

		result = this.collections.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final WeightedString account)
	{
		this.collections.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public WeightedStrings reverse()
	{
		WeightedStrings result;

		//
		java.util.Collections.reverse(this.collections);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.collections.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public WeightedStrings sortByName()
	{
		WeightedStrings result;

		//
		java.util.Collections.sort(this.collections, new WeightedStringComparator(Criteria.NAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public WeightedStrings sortByWeight()
	{
		WeightedStrings result;

		//
		java.util.Collections.sort(this.collections, new WeightedStringComparator(Criteria.WEIGHT));

		//
		result = this;

		//
		return result;
	}
}
